package com.cellmatrix.chemcomp7;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MyScrollPane extends JPanel{

  double s[],d[]; // arrays of values to display
  int xMax=0,xMin=0; // bounds on those arrays
  double xZF=1.,yZF=1.; // X and Y zoom (shift/scroll for X)
  double maxY; // largest y value
  int xStart=0,yStart=0; // upper-left corner
  boolean flip=false; // set to pllut blue before red

  public void shiftReset()
  {
    xStart=yStart=0;
    repaint();
  }

  public void shift(int dx, int dy)
  {
    xStart+=dx;
    yStart+=dy;
    repaint();
  }

  public void zoom(int w,boolean xZoom)
  {
    if (xZoom){
      if (w > 0) xZF*=.8; if (w < 0) xZF*=1.25;
    } else { // y zoom
      if (w > 0) yZF*=.8; if (w < 0) yZF*=1.25;
    }
    //System.out.println("zoom=" + xZF + "," + yZF);
    repaint();
  }

// paint method
  public void paintComponent(Graphics g) // g points to a BufferedImage
  {
    super.paintComponent(g);

    Graphics2D g2=(Graphics2D) g;

    if (!flip){
      // plot S in RED
      g.setColor(Color.RED);
      for (int x=0;x<xMax;x++){
        g.fillRect((int)(x*xZF)-xStart,0-yStart,(int)(xZF),(int)(s[x]*yZF));
      }

      // D in BLUE
      g.setColor(Color.BLUE);
      for (int x=0;x<xMax;x++){
        g.fillRect((int)(x*xZF)-xStart,(int)(s[x]*yZF)-yStart,(int)(xZF),(int)(d[x]*yZF));
      }
    } else {
      // plot D in BLUE
      g.setColor(Color.BLUE);
      for (int x=0;x<xMax;x++){
        g.fillRect((int)(x*xZF)-xStart,0-yStart,(int)(xZF),(int)(d[x]*yZF));
      }

      // S in RED
      g.setColor(Color.RED);
      for (int x=0;x<xMax;x++){
        g.fillRect((int)(x*xZF)-xStart,(int)(d[x]*yZF)-yStart,(int)(xZF),(int)(s[x]*yZF));
      }
    }

    g.setColor(Color.GRAY);
    g2.setStroke(new BasicStroke(3));
    g.drawRect((int)(-xStart),(int)(-yStart),(int)(xMax*xZF),2000);
    g2.setStroke(new BasicStroke(1));
  }
}
