package com.cellmatrix.chemcomp7;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class DrawingPanel extends JPanel{
  private static final long serialVersionUID = -484074871829946479L;
  Display display=null; // remember our display...

  public void saveDisplay(Display d)
  {
    display=d;
    setSize(2000,2000);
  }

// just display img from display object
  public void paintComponent(Graphics g)
  {
    super.paintComponent(g);
    if (display==null) return; // make swing happy...
    BufferedImage img=display.getImg();
    if (img != null){
//      g.drawImage(display.getImg(),0,0,this);
      g.drawImage(img,0,0,this);
    }
  }
}