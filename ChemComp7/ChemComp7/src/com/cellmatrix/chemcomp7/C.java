package com.cellmatrix.chemcomp7;

// class of constants! Woo!

public class C {
  static double minX=0,maxX=40; // pure X values for the entire universe
  static double MAXVAL=40; // this is the most S or D a tube can hold...
  //static double dx=.03125; // this is our delta-X
  static double dx=.125; // this is our delta-X
  static double effect=20; // each transfer also affects nearby bins // was 20
  static int timeStep=0; // increment each step
  static boolean debug=false;
  static double rgbScale=7;
  static double dt=0.05; // Step the simulation this much. Used to scale pouring impact
  static double maxTime=50;
  static int scaleX=2,scaleY=1;
  static int numStepsOnDisplay=600; // y scale :)
  static boolean VOLUMECLIP=true; // set to clip volumes between [0,MAXVAL]
  static int clearTime=20;
  static int POPSIZE=40; // # of individuals in the population
  static int SELECT=6; // preserve the most fit individuals, then breed them
  static double MUTATIONRATE=0.05;
  static int CSIZE=16; // # of bins in a single gene
  static int S=0,D=1;
  static boolean looping=false; // set true to let the current member run forever
  static boolean doStim=false;
  static double snapshotDiffThreshold=0.01; // report changes smaller than this
  static int NumInitBins=2;
  static boolean ABSOLUTEXFER=false; // abs or relative
  static int loopCountExpire=100; // if expire checkbox is checked, only loop this many timesteps
}