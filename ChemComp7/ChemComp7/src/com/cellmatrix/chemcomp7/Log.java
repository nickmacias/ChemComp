package com.cellmatrix.chemcomp7;

import java.io.DataOutputStream;
import java.io.FileOutputStream;

public class Log {

  DataOutputStream dos; // write here
  Universe universe;

  Log(String fname) // constructor
  {
    try {
      dos=new DataOutputStream(new FileOutputStream(fname));
    } catch (Exception e) {
      System.out.println("Error opening " + fname + ":" + e);
    }
  }

  public void saveUniverse(Universe u)
  {
    universe=u;
  }

  // snapshot the current chemical balance
  public void logChems() // save s[] and d[] balances
  {
    if (!universe.control.chckbxConstantLogging.isSelected()) return;
    System.out.println("Log");
    int binMax=(int)((C.maxX-C.minX)/C.dx);
    try {
      for (int i=0;i<=binMax;i++){
	dos.writeDouble(universe.exist.mem[i][C.S]);
        dos.writeDouble(universe.exist.mem[i][C.D]);
      }
      dos.flush();
    } catch (Exception e) {
      System.out.println("Error writing logfile in logChems(): " + e);
    }
  }

}
