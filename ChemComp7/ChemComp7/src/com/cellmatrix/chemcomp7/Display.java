package com.cellmatrix.chemcomp7;

// Display displays its BufferedImage img
// supports pan/zoom/scroll/etc.
// can getImg() and setImg()
// also has text area with gettext() and setText()

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.image.BufferedImage;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.BevelBorder;
import javax.swing.JScrollPane;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Display extends JFrame {

  private static final long serialVersionUID = 1L;
  Control controlFrame;
  private JPanel contentPane;
  Universe universe; // this will run the simulation
  BufferedImage img=new BufferedImage(2000,2000,BufferedImage.TYPE_INT_ARGB);
  DrawingPanel panel; // for displaying graphics
  JTextArea msg;
  private JScrollPane scrollPane_1;

  GraphFrame gf; // for plotting info about system configuration
  public BufferedImage getImg()
  {
    return(img);
  }

  public void setMessage(String s)
  {
    msg.setText(s);
  }

  public void appendMessage(String s)
  {
    msg.setText(msg.getText()+s);
    msg.setCaretPosition(msg.getText().length());
  }

  public String getMessage()
  {
    return(msg.getText());
  }

  public Display(Universe u)
  {
    universe=u;
    gf=new GraphFrame();
    universe.gf=gf;
    gf.saveUniverse(universe);
    gf.setVisible(false);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 805, 604);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);

    JScrollPane scrollPane = new JScrollPane();

    scrollPane_1 = new JScrollPane();
    GroupLayout gl_contentPane = new GroupLayout(contentPane);
    gl_contentPane.setHorizontalGroup(
    	gl_contentPane.createParallelGroup(Alignment.LEADING)
    		.addGroup(gl_contentPane.createSequentialGroup()
    			.addGap(10)
    			.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
    				.addComponent(scrollPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE)
    				.addComponent(scrollPane_1, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE))
    			.addContainerGap())
    );
    gl_contentPane.setVerticalGroup(
    	gl_contentPane.createParallelGroup(Alignment.TRAILING)
    		.addGroup(gl_contentPane.createSequentialGroup()
    			.addContainerGap()
    			.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
    			.addPreferredGap(ComponentPlacement.UNRELATED)
    			.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
    			.addContainerGap())
    );

        panel = new DrawingPanel();
        panel.addMouseListener(new MouseAdapter() {
        	@Override
        	public void mouseClicked(MouseEvent arg0) {
        	  // show bin values at selected time and space location
        	   gf.setVisible(true);
        	   double xx=(double)arg0.getX()/(double)C.scaleX; // true x value
        	   double yy=(double)arg0.getY()/(double)C.scaleY; // and t value
        	   if (xx >= C.maxX/C.dx || xx < 0 ||
        	       yy > C.numStepsOnDisplay || yy < 0){
        	         System.out.println("Out of active window - try again");
        	         return;
        	   }
        	   // read source and dst values
        	   gf.setup(universe.exist.allBins,0,(int)(C.maxX/C.dx),(int) yy);
        	   gf.repaint();
        	   //double s=universe.exist.allBins[(int) xx][C.S][(int) yy];
        	   //double d=universe.exist.allBins[(int) xx][C.D][(int) yy];
                   //System.out.println("Bin " + (int) xx + " t=" + (int) yy + ": S,D=" +
                                      //s + "," + d);
        	}
        });
        scrollPane_1.setViewportView(panel);
        panel.saveDisplay(this);
    msg = new JTextArea("");
    msg.setLineWrap(true);
    msg.setForeground(Color.GREEN);
    msg.setBackground(Color.BLACK);
    scrollPane.setViewportView(msg);
    msg.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
    contentPane.setLayout(gl_contentPane);

  }
}