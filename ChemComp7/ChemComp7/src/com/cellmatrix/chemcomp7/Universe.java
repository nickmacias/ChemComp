package com.cellmatrix.chemcomp7;

//
// create population, simulate each individual (eexist.load(), step(), etc.)
//
// Universe is the main class for the entire system. Universe has access to
// displays (the main output windows);
// control (the main control window); and
// exist (the simulated chemical computer)
//
// assess(), sort(), select(), breed/mutate()

public class Universe extends Thread {
// access to other main objects:
  Display display;
  Control control;
  EXIST exist;

  GraphFrame gf; // for detailed display of a single point in time
  Log logger;

// local vars related to the population:
  int currentMember;
  Individual population[];

// run-control parameters
  boolean evolving=false; // set to TRUE to evolve...

// constructor
  public Universe()
  {
    display=new Display(this);display.setVisible(true);display.setSize(750,700); display.setLocation(0,50);
    control=new Control(this);control.setVisible(true);control.setLocation(display.getWidth(),100);
    control.RGBscrollBar.setValue(200);
    exist=new EXIST(this);
    logger=new Log("log.bin");
    logger.saveUniverse(this);
  }

  public Display getDisplay()
  {
    return(display);
  }

  public Control getControl()
  {
    return(control);
  }

  public EXIST getExist()
  {
    return(exist);
  }

  public void run() // all processing
  {
    int generation=0;
    double mem[][]=new double[1+exist.binMax][2]; //temporary storage for bins

    display.setMessage("Creating first population\n");
    initPop(C.POPSIZE); // initialize the population
    // We'll initialize each member (for Gen 1) below...

// step through generations
    display.appendMessage("Starting evolutionary loop\n");

/*************************************** MAIN LOOP ***************************************/

    while (true){
      display.appendMessage("GENERATION " + (++generation)+"\n");
//main simulation loop: test each individual and set their fitness
      for (int member=0;member<C.POPSIZE;member++){
// see if a pause has been requested
        while (!evolving){
          evolvePause();
        }

        currentMember=member;
        display.setTitle("Member "+member);
// load the genome (flow rates)
        exist.loadFlowRates(); // load the machine
        //exist.loadBins(population[member].genome);
        exist.clearSnapshot(); // show we're starting a new run

// load initial bins...
        if (generation==1){
          for (int i=0;i<=exist.binMax;i++){ // init all bins to 0
            population[member].genome[i][0]=population[member].genome[i][1]=0.;
          }
          for (int init=0;init<C.NumInitBins;init++){
            int rIndex=(int)(320.*Math.random());
            population[member].genome[rIndex][0]=40.*Math.random();
            population[member].genome[rIndex][1]=Math.random() *
                                                 (40.-population[member].genome[rIndex][0]); // hahahahaha......
            //System.out.println("pop[" + member  + "].s=" + population[member].genome[rIndex][0]);
          }
        } // done initializing genome...

// load the memory of the machine
        exist.loadBins(population[member].genome);

// but reload from file if requested
        if (control.loadRequest()){
          control.doInput();
        }

// log this system
        logger.logChems(); // save initial chemical balance

        if (generation==1) exist.localPaint(); // show first gen too!
        population[member].fitness=assess(false); // assess this individual, and record their (preliminary) fitness
        //display.appendMessage("Member " + member + ":fitness=" + population[member].fitness + "\n");
        if (population[member].fitness > 10000) { // something interesting happened...
          //System.out.println("Interesting member discovered...looping");
          control.loopCheckButton.setSelected(true);
          C.looping=true; //hang out here
        } else population[member].fitness=0; // zero this out
        ///%%% assess fitness over next batch of timesteps
        if (C.looping){ // unstable config detected - do real assessment here
          population[member].fitness=assess(true); // assess() will loop until C.looping is cleared
          display.appendMessage("Member " + member + ":fitness=" + population[member].fitness + "\n");
        }
      } // done with each individual
      sort(population); // order from most-fit to least-fit
      for (int i=0;i<C.POPSIZE;i++) System.out.println("SORTED: "+i+":"+population[i].fitness);
      display.appendMessage("Breeding...\n");
      breed(population); // create a new population from the most-fit members of this generation
      display.appendMessage("Mutating...\n");
      mutate(population); // introduce a small number of random mutations
    } // end of main loop!
  } // end of run()

  public void evolvePause() // sleep a bit
  {
    try {
      sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
  public void initPop(int size) // create initial random population
  {
    C.POPSIZE=size;
    population=new Individual[C.POPSIZE]; // array of individuals
    for (int ind=0;ind<C.POPSIZE;ind++){
      population[ind]=new Individual(exist);
      /***
      //population[i].randomize();  // set random genome (flow rates)
   // load a single bin...
      for (int i=0;i<=exist.binMax;i++){
        population[ind].genome[i][0]=population[ind].genome[i][1]=0.;
      }
      for (int init=0;init<C.NumInitBins;init++){
        int rIndex=(int)(320.*Math.random());
        population[ind].genome[rIndex][0]=40.*Math.random();population[ind].genome[rIndex][1]=Math.random() * (40.-population[ind].genome[rIndex][0]); // hahahahaha......
      }
      ***/
    } // initial population is set
  }

  // call this to set state to run/pause
  public void setEvolveState(boolean e)
  {
    evolving=e;
  }


// step the individual and measure its performance
// return its fitness at the end
// if finalMeasure=true, reset our fitness and tally it across all steps
// For now, let's look for low-frequency cyclic behavior
 double assess(boolean finalMeasure)
 {
   double fitness=0; // tally this at each timestep

   int target=0;
   loadEnv(target); // let's skip this for now, and add an exploratory phase later...
   boolean doMore=true;
   int loopCount=0; // bump this as we loop; use to expire if Expire checkbox is checked
   double cyclicFitness=0; // increase as we see more of the desired cyclic behavior!

   while (doMore){ // if Looping checkbox is selected, just keep going...
     for (int i=0;i<C.maxTime/C.dt;i++){
       while (!evolving){ // allow evolution to pause
         evolvePause();
         if (control.saveRequest()) control.doOutput();
       }
       if (control.saveRequest()) control.doOutput();
       if (C.doStim){
         C.doStim=false;
         double index=C.maxX*Math.random();
         double level=40.*Math.random();
         exist.writeBin(index,0,level);
         exist.writeBin(index,1,C.MAXVAL-level);
       }

       double variation=exist.step(finalMeasure); // advance the simulation
       //System.out.println("v="+variation);
       if ((variation < .001) || // no longer interesting
           (control.chckbxExpire.isSelected() && ++loopCount > C.loopCountExpire)){
         control.loopCheckButton.setSelected(false);C.looping=false;
       }
       exist.compareSnapshot(); // compare current bins to saved snapshot
       double difference=exist.getSnapshotDiff();
       if (difference < C.snapshotDiffThreshold){ // report this
         exist.createSnapshot(); // take a new snapshot
       }
       fitness+=(i+1)*variation; // record how much the population changes...
       if (i<C.maxTime/(2*C.dt)){ // first half of the window
         cyclicFitness+=variation; // we want increases, so add
       } else {
         cyclicFitness-=variation; // else subtract
       }
     }
     doMore=C.looping;
   } // end of looping loop
   return(finalMeasure?Math.abs(cyclicFitness):fitness);
 }

// loadEnv is used to force a chemical change into the system
   void loadEnv(int target)
   {
   /***
     for (int i=exist.toBin(30);i<exist.toBin(31);i++){
       if (target==0){
         exist.writeBin(i,C.S,0);
         exist.writeBin(i,C.D,0);
       } else {
         exist.writeBin(i,C.S,1);
         exist.writeBin(i,C.D,1);
       }
     }
     ***/
   }
//score(mode) assesses the current setup and returns a delta-fitness
//mode works as follows:
//    0: want both outputs clear
//    1: want output
//
  double matchScore(int target)
  {
    int i;
    double dfit=0; // change in fitness
    double desired;

    for (i=exist.toBin(35);i<exist.toBin(36);i++){
      if (target==0){ // want bin to be empty
        desired=0;
      } else { // want bin occupied
        desired=10;
      }
      dfit+=adjust(exist.readBin(i,C.S),desired);
      dfit+=adjust(exist.readBin(i,C.D),desired);
    }

    // make sure we haven't filled inputs that should be empty...

    for (i=exist.toBin(30);i<exist.toBin(31);i++){
      if (target==0){ // want bin to be empty
        desired=0;
      } else { // want bin occupied
        desired=10;
      }
      dfit+=adjust(exist.readBin(i,C.S),desired);
      dfit+=adjust(exist.readBin(i,C.D),desired);
    }
    //System.out.println("dfit=" + dfit);
    return(dfit);
  }

 //take a target value and an actual value, and return a relative measure of the match
  double adjust(double actual, double desired)
  {
    double diff=Math.abs(actual-desired); // how far off are we?

    if (desired==0){
      actual=Math.abs(actual);
      if (actual > 10) actual=10;
      return(-actual);
    }

 //else our target is something positive. Return a fitness based on how close we are...
    if (diff < desired/5) return(10);
    if (diff < desired/4) return(7);
    if (diff < desired/3) return(4);
    return(0);
  }

// find 10 most-fit individuals
  void sort(Individual p[])
  {
    int select=C.SELECT; // choose this many most-fit individuals
    for (int i=0;i<select;i++){ // choose ith most-fit individual
      int bestIndex=i;double bestFit=p[i].fitness;
      for (int j=i+1;j<C.POPSIZE;j++){ // compare [j] to best
        if (p[j].fitness > bestFit){
          bestIndex=j;bestFit=p[j].fitness;
        }
      } // bestFit/bestIndex are set...swap with element [i]
      Individual temp;
      temp=p[i];p[i]=p[bestIndex];p[bestIndex]=temp;
    }
  }

  void breed(Individual p[])
  {
    int p1,p2;
    int select=C.SELECT; // these individuals are preserved
    for (int i=select;i<C.POPSIZE;i++){
      p1=(int) (Math.random()*select);
      //while (p1 == (p2=(int) (Math.random()*select))); // force parent to be different
      p2=(int) (Math.random()*select);
      display.appendMessage("("+p1+","+p2+")->"+i+"  ");
      p[i]=combine(p[p1],p[p2]); // combine these genes
    }
    display.appendMessage("\n");
  }

  Individual combine(Individual p1, Individual p2)
  {
    boolean byChunks=false; // set to treat chunks of bins as single units
    boolean byAveraging=true; // set to average parents' genes
    boolean bySelect=false; // set to choose each bin from a random parent

    Individual ret=new Individual(exist); // this will be the child...

    if (byChunks){
      int parent=1; // choose p1 or p2
      for (int i=0;i<exist.binMax+1;i++){
        if (0==i%C.CSIZE){ // pick a new parent
          parent=(Math.random() > .5)?2:1;
        }
        ret.genome[i]=(parent==1)?p1.genome[i]:p2.genome[i];
      }
      return(ret);
    }

    if (byAveraging){
      for (int i=0;i<exist.binMax+1;i++){
        ret.genome[i][0]=(p1.genome[i][0]+p2.genome[i][0])/2;
        ret.genome[i][1]=(p1.genome[i][1]+p2.genome[i][1])/2; // average
      }
      return(ret);
    }

    if (bySelect){
      for (int i=0;i<exist.binMax+1;i++){
        if (Math.random() > .5){
          ret.genome[i][0]=p2.genome[i][0];
          ret.genome[i][1]=p2.genome[i][1];
        } else {
          ret.genome[i][0]=p1.genome[i][0];
          ret.genome[i][1]=p1.genome[i][1];
        }
      }
      return(ret);
    }

    return(null); // shouldn't do this!
  }

// mutate later...
  void mutate(Individual p[])
  {
    for (int member=0;member<C.POPSIZE;member++){
      if (Math.random() < .95) continue; // no mutation
      for (int i=0;i<=exist.binMax;i++){
        if (Math.random() < C.MUTATIONRATE){
          p[member].genome[i][0]=30.*Math.random();
          p[member].genome[i][1]=30.-p[member].genome[i][0]; // preserve total volume
        }
      }
    }
  }
}