package com.cellmatrix.chemcomp7;

//
// Main class for the entire system
// uses the run() method to create the universe, then starts its main thread
// everything else is driven from the universe...
//

import java.awt.EventQueue;

public class Main {

  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
	try {
	  Universe universe = new Universe();
	  universe.start(); // start the universe!
	} catch (Exception e) {
	  e.printStackTrace();
	}
      } // end of run()
    });
  }
}
