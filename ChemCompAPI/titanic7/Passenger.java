// Passenger class
class Passenger{
  int ID;
  boolean survived;
  int cls;
  String name;
  char sex;
  int age;
  int sibsp;
  int parch;
  int fare;
  String cabin;
  char embarked;

// construct from a line of input
  Passenger(String buffer)
  {
// remove commas inside quoted strings
    boolean inQ=false; // set when inside a quoted string
    for (int i=0;i<buffer.length();i++){
      if (buffer.charAt(i)=='"'){inQ=!inQ;}
      if (inQ && buffer.charAt(i)==','){
        buffer=buffer.substring(0,i)+" "+buffer.substring(i+1);
      }
    }

// split into fields
    buffer=buffer+" ,"; // pad for missing fields
    String[] pieces=buffer.split(",");

// load into Passenger object
    ID=Integer.parseInt(pieces[0]);
    survived=(pieces[1].equals("1"));
    cls=Integer.parseInt(pieces[2]);
    name=pieces[3];
    sex=(pieces[4]+" ").charAt(0);
    age=(int)(Double.parseDouble("0"+pieces[5]));
    sibsp=Integer.parseInt(pieces[6]);
    parch=Integer.parseInt(pieces[7]);
    fare=(int)(Double.parseDouble(pieces[9]));
    cabin=pieces[10];
    embarked=(pieces[11]+" ").charAt(0);
  }

  public String toString()
  {
    return("ID=" + ID + ", survived: " + survived +
           ", class=" + cls + ", name=" + name +
           ", sex=" + sex + ", age=" + age +
           ", sibsp=" + sibsp + ", parch=" + parch +
           ", fare=" + fare + ", embarked:" + embarked);
  }
}
