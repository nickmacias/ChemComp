//
// Main class for the entire system
// uses the run() method to create the universe, then starts its main thread
// everything else is driven from the universe...
//

import java.awt.EventQueue;

public class Main {

  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {

        if (args.length!=1){ // should specify where to save stuff
          System.out.println("Usage: r Main outputindex");
          return;
        }

        try {
          Core g=new Core();
          g.setNum(args[0]);
          Myout.init(args[0]); // create output file
          g.start();
        } catch (Exception e) {
          e.printStackTrace();
        }
      } // end of run()
    });
  }
}
