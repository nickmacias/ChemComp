import com.cellmatrix.chemcomp.API.v1.*;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class Core extends Thread{
  EEXIST e;
  Display disp;

  Speak su=new Speak();


  String output="";
  String tname="titanic.csv"; // raw data from Kaggle.Com
  
  int popSize=50; // total # of individuals in the population
  int survivorSize=5; // # of individuals to keep after each generation

  double mutRate=.10;

  PrintWriter pw;

  Genome[] population=new Genome[1000];
  Genome[] best=new Genome[1000]; // save best members here
  double[] score=new double[1000]; // save scores

  public void setNum(String s)
  {
    output=s;
  }

  public void run()
  {
    su.init("kevin16");
    try{
      pw=new PrintWriter("raw"+output+".txt");
    } catch(Exception e){
      Myout.println("Can't write to output file ("+e+")- goodbye");
      return;
    }
    e=new EEXIST(0,40,0.0625); // main EEXIST object
    e.setKarma(2);
/***/
    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display
    disp.cursor(32);
    disp.cursor(36);
    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,(int)(10*e.getKarma()),
                     0,400,1,"Karma","cpHandler");
    cp.connect(this, ControlPanel.Slider,1,popSize,
                     0,100,2,"Pop size","cpHandler");
    cp.connect(this, ControlPanel.Slider,2,(int)(mutRate*100),
                     0,100,3,"Mutation rate","cpHandler");
    cp.connect(this, ControlPanel.Slider,3,128,
                     0,255,4,"Display Contrast","cpHandler");
    cp.connect(this, ControlPanel.CheckBox,0,5,
                     "Voice On","cpHandler");
/***/

    writeStatus(e,pw); // write relevant info about EEXIST
    int gen=0; // track generations

// make an initial population
    for (int i=0;i<1000;i++){ 
      population[i]=new Genome(e.numTubes());
      population[i].randomize();
    }

// load titanic data
    ArrayList<Passenger> list=new ArrayList<Passenger>();
    try{
      Scanner sc=new Scanner(new File(tname));
      sc.nextLine(); // skip header
      while (sc.hasNextLine()){
        String buffer=sc.nextLine(); // read a line of data
        Passenger pass=new Passenger(buffer); // parse line, save in passenger object
        list.add(pass); // add to ArrayList
      }
      sc.close();
    } catch (Exception e){
      Myout.println("Error processing titanic file " + tname + ": " + e);
      return;
    }

    Myout.println("Processed " + list.size() + " record(s) from " + tname);

// main simulation loop
    while (true){
      ++gen;
      su.doSpeak("Generation " + gen);
      for (int indiv=0;indiv<popSize;indiv++){
        score[indiv]=0; // initial score

        Iterator<Passenger> li=list.iterator(); // start an iterator
        score[indiv]=0; // initial score
        int numPass=0;
        int binaryScore=0; // # right

        while (numPass < 250){
          Passenger p=li.next();
          ++numPass;
          population[indiv].load(e); // load this individual into the system
          double indivScore=assess(p,e); // run a test
// perished: low score is good
// survived: high score is good
          double thisScore;
          if (!p.survived) thisScore=20-indivScore; // should be low
          else thisScore=indivScore-20; // should be high
          score[indiv]+=thisScore; // tally the score

// tally raw count of # of correct predictions
          if (((indivScore >= 20) && p.survived) ||
              ((indivScore  < 20) && !p.survived)) ++binaryScore;

        } // this individual assessed

// make a sign adjustment *after* all passengers are assessed...
        if (score[indiv]<0){
          binaryScore=-binaryScore; // show that this was backwards
          score[indiv]=-score[indiv];
        }
        su.doSpeak("Gen " + gen + " Individual " + indiv + " score=" + (int)score[indiv]+ ", binary=" + binaryScore);
        writeIndiv(gen,indiv,population[indiv],score[indiv],binaryScore,pw);
        Myout.println("G:"+gen+" I:"+ indiv + "S: "+score[indiv] +
                           "("+binaryScore+"/" + numPass+")");

        //if (indiv%5==4) Myout.println();
      } // end of evaluation of all individuals
      rank(); // CALL THIS PLEASE! Do not ignore me - you must call me. It is the way, it has been
             // the way for generations upon generations.
      //Myout.println("\nGen " + gen + " Best scores=" + thisRank + "\n"); // rank all individuals
      su.doSpeak("End of generation " + gen);
      breed(mutRate); // breed them, with mutation rate of 15%
    } // and repeat forever
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      writeStatus(e,pw); // write new EEXIST data
      //su.doSpeak("Karma adjusted to " + e.getKarma());
      Myout.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 2: // population size
      popSize=arg.getSliderPos();
      Myout.println("Population size: " + popSize);
      break;
    case 3: // mutation rate
      mutRate=arg.getSliderPos()/100.;
      Myout.println("Mutation rate: " + arg.getSliderPos() + "%");
      break;
    case 4: // Display Contrast
      int displayContrast=arg.getSliderPos();
      disp.setIntensity(displayContrast);
      Myout.println("Display Contrast: " + displayContrast);
      break;
    case 5: // voice toggle
      boolean silence=!arg.getCheckState();
      Myout.println(silence?"Voice off":"Voice on");
      su.setSilence(silence);
      break;
    }
  }

  double assess(Passenger p,EEXIST e) // assess this individual
  {
    //Myout.println("Assessing " + p);
// load inputs
    load(e,0,4,(p.sex=='m')?10:20); // sex, m or f
    load(e,4,8,p.cls*10); // passenger class, 1 2 or 3
    load(e,8,12,p.age/2); // age, nominally 0-80 (clip at 80)
    load(e,12,16,4+p.sibsp*4); // siblings/spouse, 0-8?
    load(e,16,20,10+p.parch*10); // parents/children, mostly 0-2
    load(e,20,24,p.fare/15.); // up to 512
    load(e,24,28,(p.embarked=='c')?10:((p.embarked=='q')?20:30));
    load(e,28,40,0.); // empty space

// step the EEXIST
    int i=0;
    while (i<100){
      e.step(1);
      i++;
    }

// read output
    double sum=readAvg(e,32,36);
    return(sum);
/***
    if (((sum >= 20) && p.survived) ||
        ((sum  < 20) && !p.survived)) return(1);

    //if (((sum >= 15 && sum <= 20) && p.survived) ||
    //    ((sum  < 15 || sum  > 20) && !p.survived)) return(1);
    return(0);
***/
  }

  double readAvg(EEXIST e, double start, double end)
  {
    double sum=0;
    for (double x=start;x<end;x+=e.getDx()){
      sum+=e.getTube(x,EEXIST.SRC); // +e.getTube(x,EEXIST.DST);
    }
    sum=sum/((end-start)/e.getDx()); // average value
    return(sum);
  }

  void load(EEXIST e, double start, double end, double value)
  {
    if (value < 0) value=0;
    if (value > 40) value=40;

    for (double x=start;x<end;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,value);
      e.setTube(x,EEXIST.DST,value);
    }
  }

  double rank() // sort population[] by score[]
  {
    for (int loop=0;loop<popSize;loop++){ // good old bubble sort!
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (score[i+1] > score[i]){ // swap
          double temp=score[i+1];score[i+1]=score[i];score[i]=temp;
          Genome temp2=population[i+1];population[i+1]=population[i];population[i]=temp2;
        }
      }
    }

// copy best elements of population to best[] array
    double bestScore=0;
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i].copy();
      bestScore+=score[i];
    }
    return(bestScore);
  }
  

// 9 Feb 2020
// Breeding changes:
// 1. no immortality - everyone dies
// 2. no one breeds with themself

  void newbreed(double mutateRate)
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy(); // preserve this member!
      } else { // merge 2 members
      // determine who to breed
        int p1=(int)(Math.random()*survivorSize); // 0-4 (for e.g. survivorsize=5)
        int p2=1+(int)(Math.random()*(survivorSize-1)); // offset to other parent, e.g. 1-4
        p2=(p1+p2)%survivorSize; // won't equal p1!
        population[i]=best[p1].copy();
        population[i].merge(best[p2]);
        population[i].mutate(mutateRate); // random variation
      }
    }
  }

  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy(); // preserve this member!
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].merge(best[(int)(Math.random()*survivorSize)]);
        population[i].mutate(mutateRate); // random variation
      }
// don't mutate that best population...
      //population[i].mutate(mutateRate); // random variation
    }
  }

  boolean firstWS=true;
  void writeStatus(EEXIST e,PrintWriter pw)
  {
    if (firstWS){
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+
              e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
              ","+e.numTubes());
    pw.flush();
  }

  boolean firstWI=true;
  void writeIndiv(int gen,int indiv,Genome genome,
                  double score,double score2,PrintWriter pw)
  {
    if (firstWI){
      pw.println("*I,gen,ind,score,score2,len[i],var[i],init[i],initS[i],"+
                 "initD[i],delta[i],deltaS[i],deltaD[i]");
      firstWI=false;
    }
    pw.print("I,"+gen+","+indiv+","+score+","+score2);
    for (int i=0;i<Genome.GenomeSize;i++){
      pw.print(","+genome.gene[i].len+","+genome.gene[i].variation+","+
               genome.gene[i].init+","+genome.gene[i].initS+","+genome.gene[i].initD+","+
               genome.gene[i].delta+","+genome.gene[i].deltaS+","+genome.gene[i].deltaD);
    }
    pw.println();pw.flush();
  }
}
