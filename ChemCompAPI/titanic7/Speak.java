import java.beans.PropertyVetoException;
import java.util.Locale;

import javax.speech.AudioException;
import javax.speech.Central;
import javax.speech.EngineException;
import javax.speech.EngineStateError;
import javax.speech.synthesis.Synthesizer;
import javax.speech.synthesis.SynthesizerModeDesc;
import javax.speech.synthesis.Voice;

public class Speak {

  SynthesizerModeDesc desc;
  Synthesizer synthesizer;
  Voice voice;
  boolean silent=true;

  public void setSilence(boolean silence)
  {
    silent=silence;
  }

  public void init(String voiceName)
  {
    try{
      if (desc == null) {

        System.setProperty("freetts.voices",
          "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");

        desc = new SynthesizerModeDesc(Locale.US);
        Central.registerEngineCentral
          ("com.sun.speech.freetts.jsapi.FreeTTSEngineCentral");
        synthesizer = Central.createSynthesizer(desc);
        synthesizer.allocate();
        synthesizer.resume();
        SynthesizerModeDesc smd =
          (SynthesizerModeDesc)synthesizer.getEngineModeDesc();
        Voice[] voices = smd.getVoices();
        Voice voice = null;
        for(int i = 0; i < voices.length; i++) {
          System.out.println("Voice " +i + " <" + voices[i].getName() + ">");
          if(voices[i].getName().equals(voiceName)) {
            voice = voices[i];
          }
        }
        synthesizer.getSynthesizerProperties().setVoice(voice);
      }
    } catch (Exception e){
          e.printStackTrace();
    }
  }

  public void terminate(){
    try{
      synthesizer.deallocate();
    } catch (Exception e){
          e.printStackTrace();
    }
  }

  public void doSpeak(String speakText)
  {
    if (silent) return;
    try{
      synthesizer.speakPlainText(speakText, null);
      synthesizer.waitEngineState(Synthesizer.QUEUE_EMPTY);
    } catch (Exception e){
          e.printStackTrace();
    }
  }


/***
  public static void main (String[]args) throws Exception{
    Main su = new Main();

    su.init("kevin16");
    // high quality
    //su.doSpeak("Hello there, this is my laptop trying to talk to you hahaha");
    su.doSpeak(args[0]);
    su.terminate();
  }
***/
}
