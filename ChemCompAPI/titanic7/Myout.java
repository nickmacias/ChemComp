import java.io.PrintWriter;

class Myout{
  static PrintWriter pw=null;

  public static void init(String suffix)
  {
    try{
      pw=new PrintWriter("output"+suffix+".txt");
    } catch (Exception e){
      System.out.println("Error opening output"+suffix+".txt: " + e);
    }
  }

  public static void println(String msg)
  {
    System.out.println(msg);
    pw.println(msg);
    pw.flush();
  }
}
