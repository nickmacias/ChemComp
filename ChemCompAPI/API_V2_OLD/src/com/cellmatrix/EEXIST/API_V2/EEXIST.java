package com.cellmatrix.EEXIST.API_V2;

import java.awt.Color;
import java.awt.Graphics;

// EEXIST - Egoless EXtended effect contInuous Space Time


/**
 * @author Nicholas J. Macias, Cell Matrix Corporation
 * @version 2.0
 *
 * API for EEXIST (Egoless EXtended effect contInuous Space Time system)
 * <p>
 *
 * A series of methods and fields for simulating the behavior of an EEXIST system
 *
 *
 */
public class EEXIST{

    private SDElement [][] mem; // holds chemical levels
    private SDElement [][] biases; // holds offset biases
    private SDElement [][] memDelta; // save changes in here, then apply all at once
    private double[][] diameters;
    private double minX, maxX, dx;
    private double minY, maxY, dy; // fixed spatial discretization
    public int numXBins,numYBins; // number of bins: mostly used for sweeping matrix
    private double dt=0.05; // timestep size
    private double karma=5; // range of impact
    private double maxVal=0,minVal=0; // limits on chemical levels
    // TODO Allow setting of min and max vals
    private boolean volumeClip=false;
    private double totalVar=0; // sum of all variations since last reset
    private double variation;
    private boolean equilibriumFlow=false;
    private boolean sdFlow=true;
    // TODO Allow selectable SD or Equilibrium flow
    private boolean DEBUG=false; // set to print debug outputs
    private Display display=null; // points to display frame if available
    private int timeStep=0; // tracks time units

    /** chemical values specify absolute coordinates */
    public static final int Absolute=1;
    /** chemical values specify relative coordinates */
    public static final int Relative=2;
    private int xferMode=EEXIST.Absolute; // set to one of the above
    private boolean proportionalFlow=true; // set false for flow rate that's independent of the current chemical level

    /** indicies for specifying SDElement fields */
    static final int SRCX=1;
    static final int SRCY=2;
    static final int DSTX=3;
    static final int DSTY=4;

    /** constructor with no arguments uses minX=0, maxX=40, dx=0.0625
     * same for Y
     */
    public EEXIST() // use default values
    {
        this(0,40,.0625, 0, 40, .0625);
    }

    /** main class for EEXIST system
     * @param minX smallest bin number (x)
     * @param maxX largest bin number (x)
     * @param dx spatial x increment
     * @param minY smallest bin number (y)
     * @param maxY largest bin number (y)
     * @param dy spatial y increment
     *
     */
    public EEXIST(double minX, double maxX, double dx, double minY, double maxY, double dy)
    {
        this.minX=minX;this.maxX=maxX;this.dx=dx;
        this.minY=minY;this.maxY=maxY;this.dy=dy;
        maxVal=(maxX>maxY)?maxX:maxY; // for various clipping operations
        allocateMemory(); // allocate memory for storage of active individual...
    }

    /**
     * Control whether flow rate is based on current chemical level
     * @param state - set true to make flow rate proportional to current chemical level
     */
    public void proportionalFlow(boolean state)
    {
        proportionalFlow=state;
    }

    /** Set timestep for simulation (default=0.05)
     * @param dt specifies the timestep
     */
    public void setDt(double dt)
    {
        this.dt=dt;
    }

    /** Set karma (interlinking of cause and effect) of transfers. Transfers centered at a location of x will also occur
     * nearby. The diameter of the transfer conduit drops off linearly,
     * from 1 (max) at the original coordinate, to 0 (min) at a distance of "karma."
     * @param karma is the spatial extent of the transfer's karma
     */
    public void setKarma(double karma)
    {
        this.karma=karma;
    }

    /**
     * returns the current karma of each transfer
     * @return current karma
     */
    public double getKarma()
    {
        return(karma);
    }

    /**
     * set all tube diameters from an array of values
     * @param diameters array of tube diameters [0,binMax]
     */
    public void setAllDiameters(double[][] diameters)
    {
      for (int x=0;x<numXBins;x++){
        for (int y=0;y<numYBins;y++){
          this.diameters[x][y]=diameters[x][y];
        }
      }
    }

    /**
     * set diameter of a single tube
     * @param tubeX integer tube number [0,numXBins)
     * @param tubeY integer tube number [0,numYBins)
     * @param diameter diameter to set
     */
    public void setDiameter(int tubeX,int tubeY,double diameter)
    {
        diameters[tubeX][tubeY]=diameter;
    }

    /**
     * set diameter of a tube biasd on its spatial coordinates
     * @param xLoc X location of the tube
     * @param yLoc Y location of the tube
     * @param diameter diameter to set
     */
    public void setDiameter(double xLoc,double yLoc,double diameter)
    {
        diameters[toXBin(xLoc)][toYBin(yLoc)]=diameter;
    }

    /**
     * set bias value for a given tube (integer index)
     * @param tubeX X index of tube to set
     * @param tubeY Y index of tube to set
     * @param sd EEXIST.SRCX, SRCY, DSTX or DSTY
     * @param biasVal delta (default 0)
     */
    public void setBias(int tubeX, int tubeY, int sd, double biasVal)
    {
        switch(sd){
            case EEXIST.SRCX: biases[tubeX][tubeY].xSrc=biasVal;break;
            case EEXIST.SRCY: biases[tubeX][tubeY].ySrc=biasVal;break;
            case EEXIST.DSTX: biases[tubeX][tubeY].xDst=biasVal;break;
            case EEXIST.DSTY: biases[tubeX][tubeY].xDst=biasVal;break;
        }
    }

    /**
     * set SRC/DST deltas (added to chemical levels to choose src/dst addresses)
     * @param xLoc spatial location of tube
     * @param yLoc spatial location of tube
     * @param sd EEXIST.SRCX, SRCY, DSTX or DSTY
     * @param biasVal delta (default 0)
     */
    public void setBias(double xLoc, double yLoc, int sd, SDElement biasVal)
    {
        switch(sd){
            case EEXIST.SRCX: biases[toXBin(xLoc)][toYBin(yLoc)].xSrc=biasVal.xSrc;break;
            case EEXIST.SRCY: biases[toXBin(xLoc)][toYBin(yLoc)].ySrc=biasVal.ySrc;break;
            case EEXIST.DSTX: biases[toXBin(xLoc)][toYBin(yLoc)].xDst=biasVal.xDst;break;
            case EEXIST.DSTY: biases[toXBin(xLoc)][toYBin(yLoc)].yDst=biasVal.yDst;break;
        }
    }

    /**
     * load all bias values from 2D array
     * @param bias 2-D array of bias values (SDElements)
     */
    public void setAllBiases(SDElement[][] bias)
    {
        for (int x=0;x<numXBins;x++){
            for (int y=0;y<numYBins;y++){
                biases[x][y]=bias[x][y];
            }
        }
    }
    //TODO allow all biases to be set and read; also add to Individual

    /** Set the mode for chemical transfers.
     * @param mode specifies the type of transfers that take place. Options are:
     * <p>
     * EEXIST.Absolute - values are treated as absolute spatial coordinates
     * <p>
     * EEXIST.Relative - values are treated as displacements from the current location (+/-)
     * This should be changed sometime to allow for negative displacements.
     */
    public void setTransferMode(int mode)
    {
        xferMode=mode;
    }

    /** Reset the internal timeclock to t=0 */
    public void resetTimeStep()
    {
        timeStep=0;
    }

    /**
     * How many simulated time steps have passed since the last reset
     * @return Number of time steps
     */
    public int getTimeStep()
    {
        return(timeStep);
    }

/** read (fixed) minimum X value (tube 0)
     * @return minimum X value
     */
    public double getMinX()
    {
        return(minX);
    }

    /** read (fixed) maximum X value (tube n)
     * @return maximum X value
     */
    public double getMaxX()
    {
        return(maxX);
    }

    /** read (fixed) spatial increment (dx)
     * @return dx
     */
    public double getDx()
    {
        return(dx);
    }

    /** read (fixed) minimum Y value (tube 0)
     * @return minimum Y value
     */
    public double getMinY()
    {
        return(minY);
    }

    /** read (fixed) maximum Y value (tube n)
     * @return maximum Y value
     */
    public double getMaxY()
    {
        return(maxY);
    }

    /** read (fixed) spatial increment (dy)
     * @return dy
     */
    public double getDy()
    {
        return(dy);
    }

    /** associate a display with this EEXIST. step() will record activity for the display to paint
     * @param d Display object to link to this EEXIST
     */
    public void saveDisplay(Display display)
    {
        display=this.display; // this display is linked to us (set to null to disconnect)
    }

    /**
     * de-associate a display from an EEXIST. The Display itself is not affected by this.
     */
    public void releaseDisplay()
    {
        display=null;
    }

    // allocate memory for the bins and their delta containers
    private void allocateMemory()
    {
        numXBins=1+(int)((maxX-minX)/dx);
        numYBins=1+(int)((maxY-minY)/dy);
        mem=new SDElement[numXBins][numYBins]; // memory - holds all fluid levels
        memDelta=new SDElement[numXBins][numYBins]; // will hold changes
        diameters=new double[numXBins][numYBins];
        biases=new SDElement[numXBins][numYBins];
        for (int x=0;x<numXBins;x++){
          for (int y=0;y<numYBins;y++){
            diameters[x][y]=1.;
            biases[x][y]=new SDElement();
            biases[x][y].xSrc=biases[x][y].ySrc=biases[x][y].xDst=biases[x][y].yDst=0;
            mem[x][y]=new SDElement();
            mem[x][y].xSrc=mem[x][y].ySrc=mem[x][y].xDst=mem[x][y].yDst=0;
          }
        }
    }

    /**
     * Return number of X tubes
     *
     * @return number of X tubes
     */
    public int numXBins()
    {
        return(numXBins);
    }

    /**
     * Return number of Y tubes
     *
     * @return number of Y tubes
     */
    public int numYBins()
    {
        return(numYBins);
    }

    /**
     * load an individual into the system
     * @param indiv the individual to load. indiv's contents will completely
     * replace the contents of the EEXIST system's tubes
     */
/***
    public void loadIndividual(Individual indiv)
    {
        for (int x=0;x<numXBins;x++){
          for (int y=0;y<numYBins;y++){
            mem[x][y].xSrc=indiv.mem[x][y].xSrc;
            mem[x][y].ySrc=indiv.mem[x][y].ySrc;
            mem[x][y].xDst=indiv.mem[x][y].xDst;
            mem[x][y].yDst=indiv.mem[x][y].yDst;
          }
        }
    }
***/

    /**
     * read the contents of the EEXIST system's tubes, and create a new individual
     * matching those concentrations
     * @return a new Individual
     */
/***
    Individual readIndividual()
    {
        Individual indiv=new Individual(this);
        for (int x=0;x<numXBins;x++){
          for (int y=0;y<numYBins;y++){
            indiv.mem[x][y].xSrc=mem[x][y].xSrc;
            indiv.mem[x][y].ySrc=mem[x][y].ySrc;
            indiv.mem[x][y].xDst=mem[x][y].xDst;
            indiv.mem[x][y].yDst=mem[x][y].yDst;
          }
        }
        return(indiv);
    }
***/

    /** set a tube's contents
     *
     * @param tubeX tube's number (integer X index)
     * @param tubeY tube's number (integer Y index)
     * @param SD EEXIST.SRCX, SRCY, DSTX or DSTY
     * @param value total amount of SRC or DST chemical to store in tube
     */
    public void setTube(int tubeX, int tubeY, int SD, double value)
    {
        switch(SD){
            case EEXIST.SRCX: mem[tubeX][tubeY].xSrc=value;break;
            case EEXIST.SRCY: mem[tubeX][tubeY].ySrc=value;break;
            case EEXIST.DSTX: mem[tubeX][tubeY].xDst=value;break;
            case EEXIST.DSTY: mem[tubeX][tubeY].xDst=value;break;
        }
    }

    /** set a tube's contents
     *
     * @param x tube's x coordinate
     * @param y tube's y coordinate
     * @param SD EEXIST.SRCX SRYC DSTX or DSTY
     * @param value total amount of SRC or DST chemical to store in tube
     */
    public void setTube(double x, double y, int SD, double value)
    {
        writeBin(x,y,SD,value);
    }

    /**
     * load all SRC/DST values from 2-D array
     * @param mem array of values. [x][y] (SDElements)
     */
    public void setAllTubes(SDElement[][] mem)
    {
        for (int x=0;x<numXBins;x++){
            for (int y=0;y<numYBins;y++){
                this.mem[x][y].xSrc=mem[x][y].xSrc;
                this.mem[x][y].ySrc=mem[x][y].ySrc;
                this.mem[x][y].xDst=mem[x][y].xDst;
                this.mem[x][y].yDst=mem[x][y].yDst;
            }
        }
    }

    /** read the amount of SRC or DST chemical in a tube
     * @param x tube's x coordinate
     * @param y tube's y coordinate
     * @param SD EEXIST.SRCX SRCY DSTX or DSTY
     * @return total amount of SRC or DST chemical in tube
     */
    public double getTube(double x, double y, int SD)
    {
      SDElement sd=mem[toXBin(x)][toYBin(y)];
      switch(SD){
          case EEXIST.SRCX: return(sd.xSrc);
          case EEXIST.SRCY: return(sd.ySrc);
          case EEXIST.DSTX: return(sd.xDst);
          case EEXIST.DSTY: return(sd.yDst);
      }
      System.exit(1); // can't happen
      return(0); // silly
    }

    /**
     * read all chemical levels
     * @return 2D array of chemical values, suitable for setAllTubes
     */
    public SDElement[][] getAllTubes()
    {
      SDElement[][] mem=new SDElement[numXBins][numYBins];
      for (int x=0;x<numXBins;x++){
        for (int y=0;y<numYBins;y++){
          mem[x][y].xSrc=this.mem[x][y].xSrc;
          mem[x][y].ySrc=this.mem[x][y].ySrc;
          mem[x][y].xDst=this.mem[x][y].xDst;
          mem[x][y].yDst=this.mem[x][y].yDst;
        }
      }
      return(mem);
    }

    /**
     * read all bias levels
     * @return 2D array of bias levels, suitable for setAllBiass
     */
    public SDElement[][] getAllBiass()
    {
      SDElement[][] biases=new SDElement[numXBins][numYBins];
      for (int x=0;x<numXBins;x++){
        for (int y=0;y<numYBins;y++){
          biases[x][y].xSrc=this.biases[x][y].xSrc;
          biases[x][y].ySrc=this.biases[x][y].ySrc;
          biases[x][y].xDst=this.biases[x][y].xDst;
          biases[x][y].yDst=this.biases[x][y].yDst;
        }
      }
      return(biases);
    }


    /** zero-out all tubes. This properly belongs more to the Individual class than the EEXIST class */
    public void clearAllTubes()
    {
      for (int x=0;x<numXBins;x++){
        for (int y=0;y<numYBins;y++){
          mem[x][y].xSrc=mem[x][y].ySrc=mem[x][y].xDst=mem[x][y].yDst=0;
        }
      }
    }

    /** advance the simulation a number of steps
     *
     * @param steps number of timesteps (dt each)
     */
    public void step(int steps)
    {
        for (int i=0;i<steps;i++){
            step();
        }
        ++timeStep; // track time since last resetTimeStep()
        if (display != null) display.update(this);
    }

// advance the simulation one time step
// scale by dt, but also by karma (based on radial distance from [x][y]):
//   karma controls how widespread the change is felt...
//     if karma~0, then most change occurs near src
//     if karma~1, then the change occurs ~everywhere!
//
// delta M(src+dx) = -M(src+dx)*dt*(karma^dx)
// delta M(dst+dx) =  M(src+dx)*dt*(karma^dx)

    private void step()
    {
      double xLoc,yLoc; // corresponding to [x][y]
      double xSrc,ySrc,xDst,yDst;
      initMemDelta(); // zero-out our delta arrays (then add cumulative karmas)
      xLoc=minX;
      for (int x=0;x<numXBins;x++){
        yLoc=minY;
        for (int y=0;y<numYBins;y++){
// [x][y] is the location of this instruction...
          xSrc=mem[x][y].xSrc;
          ySrc=mem[x][y].ySrc;
          xDst=mem[x][y].xDst;
          yDst=mem[x][y].yDst;
          if (xferMode==EEXIST.Absolute){
              transfer(xSrc,ySrc,xDst,yDst); // add up the karmas of this single src->dst command
          } else { // src/dst are relative bin IDs
              //TODO add an offset etc. to transfer code to allow negative relative displacements
              double x1,x2,y1,y2;
              x1=xLoc+xSrc;x2=xLoc+xDst;
              y1=yLoc+ySrc;y2=yLoc+yDst;
              if (x1 > maxX) x1=x1-maxX; //%%%
              if (x2 > maxX) x2=x2-maxX;
              if (x1 < 0) x1+=maxX;
              if (x2 < 0) x2+=maxX;
              if (y1 > maxY) y1=y1-maxY;
              if (y2 > maxY) y2=y2-maxY;
              if (y1 < 0) y1+=maxY;
              if (y2 < 0) y2+=maxY;
              transfer(x1,x2,x2,y2);

              x1=xLoc-xSrc;x2=xLoc-xDst;
              y1=yLoc-ySrc;y2=yLoc-yDst;
              if (x1 > maxX) x1=x1-maxX;
              if (x2 > maxX) x2=x2-maxX;
              if (x1 < 0) x1+=maxX;
              if (x2 < 0) x2+=maxX;
              if (y1 > maxY) y1=y1-maxY;
              if (y2 > maxY) y2=y2-maxY;
              if (y1 < 0) y1+=maxY;
              if (y2 < 0) y2+=maxY;
              transfer(x1,x2,x2,y2);
          }
          yLoc+=dy;
        }
        xLoc+=dx;
      } // do this across the entire space

/***
 // conservative?
 double all=0;
 for (int i=0;i<=640;i++) all=all+memDelta[i][0];
 System.out.println("all=" + all);
 ***/

// all transfers have been tallied...now adjust the memory
// Make sure tubes don't overflow or underflow
      for (int x=0;x<numXBins;x++){
        for (int y=0;y<numYBins;y++){
          mem[x][y].xSrc+=memDelta[x][y].xSrc;
          mem[x][y].ySrc+=memDelta[x][y].ySrc;
          mem[x][y].xDst+=memDelta[x][y].xDst;
          mem[x][y].yDst+=memDelta[x][y].yDst;
          variation+=Math.abs(memDelta[x][y].xSrc)+
                     Math.abs(memDelta[x][y].ySrc)+
                     Math.abs(memDelta[x][y].xDst)+
                     Math.abs(memDelta[x][y].yDst); // total change (entropy?)
//variation+=Math.abs(mem[i][EEXIST.SRC]-mem[i][EEXIST.DST]); // sum the total pressure
          if (volumeClip){
              // just clip for now
              if (mem[x][y].xSrc < minVal) mem[x][y].xSrc=minVal;
              if (mem[x][y].ySrc < minVal) mem[x][y].ySrc=minVal;
              if (mem[x][y].xDst < minVal) mem[x][y].xDst=minVal;
              if (mem[x][y].yDst < minVal) mem[x][y].yDst=minVal;

              if (mem[x][y].xSrc > maxVal) mem[x][y].xSrc=maxVal;
              if (mem[x][y].ySrc > maxVal) mem[x][y].ySrc=maxVal;
              if (mem[x][y].xDst > maxVal) mem[x][y].xDst=maxVal;
              if (mem[x][y].yDst > maxVal) mem[x][y].yDst=maxVal;
          }
        }
      }

      totalVar+=variation;
      return;
  } // end of step()

    // transfer() computes the karma of a single src->dst transfer
// the karma is not only on src and dst, but also (potentially) on nearby bins
    private void transfer(double xSrc, double ySrc, double xDst, double yDst)
    {
      if (DEBUG) System.out.println("("+xSrc+","+ySrc+") --> (" + xDst+","+yDst+")");
//
// NOTE: You need to multiply by dx!
// You're integrating (fool!) so as the bins become more numerous and shrink, the contribution from
// summing each bin needs to shrink accordingly...
//
// if karma=0, only transfer src->dst
      //if (toBin(src) < 0) return;
      //if (toBin(dst) < 0) return;

    if (karma == 0){
        connect(xSrc,ySrc,xDst,yDst,dt,1); // connect these two bins with diameter=1.0
        return;
    }
// karma is non-zero here.
// calculate transfers throughout a circular region,
// with diminishing effect from the centerpoint

    for (double x=-karma;x<karma;x+=dx){ // tally these karmas
      for (double y=-karma;y<karma;y+=dy){ // this is a square surrounding a disc of radius karma
        double r=Math.sqrt(x*x+y*y); // r=0 means full effect, r=karma means minimal effect
        if (r < karma){ // inside disc: go ahead and connect!
          connect(xSrc+x,ySrc+y,xDst+x,yDst+y,dt,(karma-r)/karma);
        } // end of transfers
      } // end of Y scan
    } // end of X scan
    return;
  } // end of transfer() code

// connect() temporarily joins two bins (b1 and b2) and allows transfer of fluid through a connection
// of the given diameter, for a time of dt
// While connected, the S and D fluids move towards equilibrium (not true unless equilibrium flow is selected)
//
    private void connect(double xSrcIn, double ySrcIn, double xDstIn, double yDstIn, double dt, double diameter)
    {
      int xSrc=toXBin(xSrcIn);
      int ySrc=toYBin(ySrcIn);
      int xDst=toXBin(xDstIn);
      int yDst=toYBin(yDstIn); // coords in e.g. mem[][] matrix

// if src and dst are the same, this should be a NOP
// but the code below will calculate working deltas for SRC and DST (-X, +X)
// and then (after sanity checking) assign delta=-X and then delta=+X
// which leads to saturation potential

        if ((xSrc==xDst)&&(ySrc==yDst)) return; // NOP!

// WRAP THESE
        while (xSrc<0) xSrc+=numXBins;
        while (xDst<0) xDst+=numXBins;
        while (xSrc>=numXBins) xSrc-=numXBins;
        while (xDst>=numXBins) xDst-=numXBins;

        while (ySrc<0) ySrc+=numYBins;
        while (yDst<0) yDst+=numYBins;
        while (ySrc>=numYBins) ySrc-=numYBins;
        while (yDst>=numYBins) yDst-=numYBins;

        //if ((b1<0)||(b2<0)) return;
        //if ((b1>binMax)||(b2>binMax)) return;
        //b1=(int)(100+100*Math.random());
        //b2=(int)(100+100*Math.random()); if (b2==b1)++b2;

        if (DEBUG) System.out.println("("+xSrc+","+ySrc+")==>("+xDst+","+yDst+") dt=" + dt + " diam=" + diameter);

        if (equilibriumFlow){ // fluids flow to reach equilibrium
            // so for example, if mem[b1]=60 and mem[b2]=20, then b1 flows to b2 with a rate of 40...
            memDelta[xSrc][ySrc].xSrc-=(mem[xSrc][ySrc].xSrc-mem[xDst][yDst].xSrc)*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy; // but if b1 is smaller, this is negative flow from b1 (i.e. TO b1)
            memDelta[xSrc][ySrc].ySrc-=(mem[xSrc][ySrc].ySrc-mem[xDst][yDst].ySrc)*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy; // but if b1 is smaller, this is negative flow from b1 (i.e. TO b1)
            memDelta[xSrc][ySrc].xDst-=(mem[xSrc][ySrc].xDst-mem[xDst][yDst].xDst)*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy; // but if b1 is smaller, this is negative flow from b1 (i.e. TO b1)
            memDelta[xSrc][ySrc].yDst-=(mem[xSrc][ySrc].yDst-mem[xDst][yDst].yDst)*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy; // but if b1 is smaller, this is negative flow from b1 (i.e. TO b1)
            return;
        }
        if (sdFlow){ // fluids flow strictly from src to dst
// see what the new memory changes should be
            double srcXSrc,srcYSrc,srcXDst,srcYDst; // changes in chemicals at source
            double dstXSrc,dstYSrc,dstXDst,dstYDst; // changes in chemicals at destination

// calculate new memDelta values (but don't actually set them unless the resulting delta is legal!)
          if (proportionalFlow){ // flow rate depends on current level
// remove chemicals from source
// srcXSrc is the change in XSrc chemicals at the source location
            srcXSrc=memDelta[xSrc][ySrc].xSrc - mem[xSrc][ySrc].xSrc*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
            srcYSrc=memDelta[xSrc][ySrc].ySrc - mem[xSrc][ySrc].ySrc*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
// srcXDst is the change in XDst chemicals at the source location etc.
            srcXDst=memDelta[xSrc][ySrc].xDst - mem[xSrc][ySrc].xDst*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
            srcYDst=memDelta[xSrc][ySrc].yDst - mem[xSrc][ySrc].yDst*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
// add chemicals to destination
            dstXSrc=memDelta[xDst][yDst].xSrc + mem[xDst][yDst].xSrc*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
            dstYSrc=memDelta[xDst][yDst].ySrc + mem[xDst][yDst].ySrc*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
            dstXDst=memDelta[xDst][yDst].xDst + mem[xDst][yDst].xDst*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
            dstYDst=memDelta[xDst][yDst].yDst + mem[xDst][yDst].yDst*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
          } else { // fixed flow rate (20 - sort of random!)
            srcXSrc=memDelta[xSrc][ySrc].xSrc - 20.*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
            srcYSrc=memDelta[xSrc][ySrc].ySrc - 20.*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
            srcXDst=memDelta[xSrc][ySrc].xDst - 20.*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
            srcYDst=memDelta[xSrc][ySrc].yDst - 20.*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
            dstXSrc=memDelta[xDst][yDst].xSrc + 20.*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
            dstYSrc=memDelta[xDst][yDst].ySrc + 20.*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
            dstXDst=memDelta[xDst][yDst].xDst + 20.*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
            dstYDst=memDelta[xDst][yDst].yDst + 20.*dt*diameter*diameters[xSrc][ySrc]*diameters[xDst][yDst]*dx*dy;
          }

// and if a delta is still legal, update memDelta
          if ((mem[xSrc][ySrc].xSrc+srcXSrc >= 0) && (mem[xDst][yDst].xSrc+dstXSrc <= maxVal)){
            memDelta[xSrc][ySrc].xSrc=srcXSrc;memDelta[xDst][yDst].xSrc=dstXSrc;
          }
          if ((mem[xSrc][ySrc].ySrc+srcYSrc >= 0) && (mem[xDst][yDst].ySrc+dstYSrc <= maxVal)){
            memDelta[xSrc][ySrc].ySrc=srcYSrc;memDelta[xDst][yDst].ySrc=dstYSrc;
          }
          if ((mem[xSrc][ySrc].xDst+srcXDst >= 0) && (mem[xDst][yDst].xDst+dstXDst <= maxVal)){
            memDelta[xSrc][ySrc].xDst=srcXDst;memDelta[xDst][yDst].xDst=dstXDst;
          }
          if ((mem[xSrc][ySrc].yDst+srcYDst >= 0) && (mem[xDst][yDst].yDst+dstYDst <= maxVal)){
            memDelta[xSrc][ySrc].yDst=srcYDst;memDelta[xDst][yDst].yDst=dstYDst;
          }
        }
    }

    // bin read/write routines
    private void writeBin(double x, double y,int sd,double value)
    {
        writeBin(toXBin(x),toYBin(y),sd,value);
    }

    private void writeBin(int tubeX, int tubeY,int sd,double value)
    {
      switch(sd){
        case EEXIST.SRCX: mem[tubeX][tubeY].xSrc=value;break;
        case EEXIST.SRCY: mem[tubeX][tubeY].ySrc=value;break;
        case EEXIST.DSTX: mem[tubeX][tubeY].xDst=value;break;
        case EEXIST.DSTY: mem[tubeX][tubeY].xDst=value;break;
      }
    }

/***
    private double readBin(double x,int sd)
    {
        return(readBin(toBin(x),sd));
    }
    private double readBin(int bin,int sd)
    {
        return(mem[bin][sd]);
    }
***/

    // convert from double (x) to int (bin)
    private int toXBin(double x) // convert real # to bin #
    {
        int retVal=(int)((x-minX)/dx);
        return(retVal%numXBins); // rollover from 0 to binMax
    }

    // convert from double (y) to int (bin)
    private int toYBin(double y) // convert real # to bin #
    {
      int retVal=(int)((y-minY)/dy);
      return(retVal%numYBins); // rollover from 0 to binMax
    }

    private void initMemDelta()
    {
      for (int x=0;x<numXBins;x++){
        for (int y=0;y<numYBins;y++){
          memDelta[x][y].xSrc=memDelta[x][y].ySrc=memDelta[x][y].xDst=memDelta[x][y].yDst=0;
        }
      }
    }

}
