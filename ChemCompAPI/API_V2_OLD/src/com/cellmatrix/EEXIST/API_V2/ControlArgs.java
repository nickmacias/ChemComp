package com.cellmatrix.EEXIST.API_V2;

/**
 *
 * @author Nicholas J. Macias, Cell Matrix Corporation
 * @version 1.0
 *
 * Object for receiving information in callbacks from control panel events
 *
 */
public class ControlArgs {
  private int userID;
  private int sliderPos;
  private boolean checkState;
  private String textIn;

  /**
   *
   * @param userID - user-supplied code to identify control
   * @param sliderPos - slider psition (0-100)
   * @param checkState - checkbox state (true=Checked)
   * @param textIn - text from the textIn box
   */
  ControlArgs(int userID, int sliderPos, boolean checkState, String textIn)
  {
    this.userID=userID;
    this.sliderPos=sliderPos;
    this.checkState=checkState;
    this.textIn=textIn;
  }

  /** get userID
   * @return user ID, as specified when control is connected
   */
  public int getUserID()
  {
    return (userID);
  }

  /** get slider position
   * @return slider position, 0-100
   */
  public int getSliderPos()
  {
    return(sliderPos);
  }

  /** get state of check box
   * @return state of check box (true=checked)
   */
  public boolean getCheckState()
  {
    return(checkState);
  }

  /** get text from textIn
   * @return text from textIn
   */
  public String getTextIn()
  {
    return(textIn);
  }
}
