package com.cellmatrix.EEXIST.API_V2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;

import java.awt.event.KeyEvent;
public class DisplayPanel {
  private JPanel mainView;
  private JPanel panel;

  public DisplayPanel() {
  panel=new MyPanel();
  mainView=new JPanel();
    panel.addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        super.keyPressed(e);
        System.out.println("Key " + e);
      }
    });
  }

  public JPanel getMainView()
  {
    return(mainView);
  }

  private void createUIComponents() {
    // TODO: place custom component creation code here
  //  panel=new MyPanel();
  }
}

