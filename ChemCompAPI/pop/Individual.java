import com.cellmatrix.chemcomp.API.v1.*;

class Individual
{
  int id;
  EEXIST e;
  Genome g;
  public double score;

  Individual()
  {
  }

  Individual(int id, EEXIST e, Genome g)
  {
    this.id=id;
    this.e=e;
    this.g=g;
    score=0;
  }

  Individual copy() // make a copy
  {
    Individual temp=new Individual();
    temp.g=g.copy(); // copy the genome
    temp.e=new EEXIST();
    temp.e.setAllTubes(e.getAllTubes());
    temp.e.setAllBiass(e.getAllBiass());
    //temp.e.setAllDiameters(e.getAllDiameters());
    score=0;
    return(temp);
  }
}
