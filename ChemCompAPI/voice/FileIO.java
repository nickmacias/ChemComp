/*
 * main .WAV file read/write code
 * NJM Sept 2017
 */

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileIO {
/***/
  public static void main(String[] args)
  {
    int[] data;
    data=readData("target.wav");
    writeData("out.wav",data);
  }
/***/

  public static boolean writeData(String fname, int[] data)
  {
    DataOutputStream os;
    byte[] buf=new byte[4];
    long size;
    int numSamples=data.length; // # of samples

    try {
      os = new DataOutputStream(new FileOutputStream(fname));
    
// write the header for the wave file
      os.write((byte)'R');
      os.write((byte)'I');
      os.write((byte)'F');
      os.write((byte)'F');
  
      int chunkSize=36 + 4*numSamples;
      os.write((byte)(chunkSize&0xff));
      os.write((byte)((chunkSize>>8)&0xff));
      os.write((byte)((chunkSize>>16)&0xff));
      os.write((byte)((chunkSize>>24)&0xff));
  
      os.write((byte)'W');
      os.write((byte)'A');
      os.write((byte)'V');
      os.write((byte)'E');

      os.write((byte)'f');
      os.write((byte)'m');
      os.write((byte)'t');
      os.write((byte)' ');

      os.write(16);os.write(0);os.write(0);os.write(0); // 16
      os.write(1);os.write(0); // 1=PCM
      os.write(2);os.write(0); // 2 channels
  
      os.write((byte)(44100&0xff));
      os.write((byte)((44100>>8)&0xff));
      os.write((byte)((44100>>16)&0xff));
      os.write((byte)((44100>>24)&0xff)); // sample rate
  
      int byteRate=44100*4;
      os.write((byte)(byteRate&0xff));
      os.write((byte)((byteRate>>8)&0xff));
      os.write((byte)((byteRate>>16)&0xff));
      os.write((byte)((byteRate>>24)&0xff)); // byte rate
  
      os.write(4);os.write(0); // block align (4 bytes per sample)
      os.write(16);os.write(0); // 16 bits per sample
  
      os.write((byte)'d');
      os.write((byte)'a');
      os.write((byte)'t');
      os.write((byte)'a');
  
      int scs2=4*numSamples;
      os.write((byte)(scs2&0xff));
      os.write((byte)((scs2>>8)&0xff));
      os.write((byte)((scs2>>16)&0xff));
      os.write((byte)((scs2>>24)&0xff));
  
  // write the data
      double time=0;
      double f=440.;
      for (int count=0;count<data.length;count++){
        int val=data[count];
        os.write((byte)(val&0xff));
        os.write((byte)((val>>8)&0xff)); // left channel
        os.write((byte)(val&0xff));
        os.write((byte)((val>>8)&0xff)); // right channel
        time=time+1./44100.;
      }
      os.close();
      return(true);
    } catch (Exception e) {
      return(false);
    } 
  }

  public static int[] readData(String fname)
  {
    FileInputStream fis;
    try {
      fis=new FileInputStream(fname);
// read header
      fis.skip(4);
      byte[] lb=new byte[4]; // for longs
      byte[] sb=new byte[2]; // for shorts
      fis.read(lb);
      int chunkSize=(lb[0]&0xff) | ((lb[1]&0xff)<<8) |
                    ((lb[2]&0xff)<<16) | ((lb[3]&0xff)<<24);
      chunkSize=chunkSize-36; // # bytes
      int samples=chunkSize/4; // # of samples
      int[] data=new int[samples]; // save the data here
      fis.skip(36);
      for (int i=0;i<samples;i++){
        fis.read(sb);
        data[i]=(sb[0]&0xff) | ((sb[1]&0xff)<<8);
        fis.skip(2);
      }
      fis.close();
      return(data);
    } catch (Exception e){
      System.out.println(e);
      return(null);
    }
  }
}
