import com.cellmatrix.chemcomp.API.v1.*;
import java.io.PrintWriter;

public class Core extends Thread{
  EEXIST e;
  Display disp;
  
  int popSize=250; // total # of individuals in the population
  int survivorSize=10; // # of individuals to keep after each generation

  PrintWriter pw;

  Genome[] population=new Genome[popSize];
  Genome[] best=new Genome[survivorSize]; // save best members here
  double[] score=new double[popSize]; // save scores

  public void run()
  {
    try{
      pw=new PrintWriter("raw.txt");
    } catch(Exception e){
      System.out.println("Can't write to output file ("+e+")- goodbye");
      return;
    }

    int[] data=FileIO.readData("target.wav"); // try to match this :)

    e=new EEXIST(); // main EEXIST object
    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display
    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,50,0,400,1,"Karma","cpHandler");
    cp.connect(this, ControlPanel.Slider,1,128,0,255,2,"Intensity","cpHandler");

    writeStatus(e,pw); // write relevant info about EEXIST
    int gen=0; // track generations

// make an initial population
    for (int i=0;i<popSize;i++){ 
      population[i]=new Genome(e.numTubes());
      population[i].randomize();
    }
// main simulation loop
    while (true){
      ++gen;
      for (int indiv=0;indiv<popSize;indiv++){
        population[indiv].load(e); // load this individual into the system
        score[indiv]=0; // initial score

        population[indiv].load(e); // re-load
        score[indiv]+=assess((indiv==0),gen,data,e); // See if system produces target pattern

        double score2=0;
        writeIndiv(gen,indiv,population[indiv],score[indiv],score2,pw);
        System.out.print(gen + ":" + indiv + "(" + score[indiv] +
                         "," + score2 + ")");

        if (indiv%5==4) System.out.println();
      } // end of evaluation of all individuals

      System.out.println("\nGen " + gen + " Best score=" + rank() + "\n"); // rank all individuals
      breed(.10); // breed them, with mutation rate of 5%
    } // and repeat forever
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      writeStatus(e,pw); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 2: // intensity
      disp.setIntensity(arg.getSliderPos());
      break;
    }
  }

//
// assessment test: 
// run the system, compare outputs to target
// 1st try: sum # of points with same polarity
//

  double assess(boolean save,int gen,
                int[] data,EEXIST e) // assess this individual
  {
    int numSamples=64*(data.length/64);
    int[] newData=null; // save member 0 here

    if (save) newData=new int[numSamples];

// Kickstart the system...
    for (int step=0;step<64;step++){

// set input now
      for (double x=18;x<26;x+=e.getDx()){
        e.setTube(x,EEXIST.SRC,20);
        e.setTube(x,EEXIST.DST,20);
      }
      e.step(1);
    }

// now sample the data
    int sample=0; // sample # from data[] array
    int matches=0; // more is better; max=sample size

    while (sample < numSamples){
// read output
      for (double x=0;x<4;x+=e.getDx()){ // 64 regions
        double val=(e.getTube(x, EEXIST.SRC)+e.getTube(x, EEXIST.DST))/2;
// val runs from 0-40; let's focus on 4-36
        val=(val-20.)/2.; // -8->8
        if (val < -8) val=-8;if (val > 7) val=7;
        int iv=(int) val; // nice integer value
        iv=iv&0x0f; // just keep 4 bits
        iv=(iv<<12)&0xf000; // this is the final data value
        if (iv==data[sample]) ++matches;
        if (save) newData[sample]=iv;
        sample++;
      } // this step evaluated
    } // end of tests

    if (save) FileIO.writeData("data/out"+gen+".wav",newData); // save first member

    return(100.*matches/(double)numSamples); // 0-100
  }

  double rank() // sort population[] by score[]
  {
    for (int loop=0;loop<popSize;loop++){ // good old bubble sort!
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (score[i+1] > score[i]){ // swap
          double temp=score[i+1];score[i+1]=score[i];score[i]=temp;
          Genome temp2=population[i+1];population[i+1]=population[i];population[i]=temp2;
        }
      }
    }

// copy best elements of population to best[] array
    double bestScore=0;
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i].copy();
      bestScore+=score[i];
    }
    //return(bestScore);
    return(score[0]);
  }
  
  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy(); // preserve this member!
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].merge(best[(int)(Math.random()*survivorSize)]);
        population[i].mutate(mutateRate); // random variation
      }
// don't mutate that best population...
      //population[i].mutate(mutateRate); // random variation
    }
  }

  boolean firstWS=true;
  void writeStatus(EEXIST e,PrintWriter pw)
  {
    if (firstWS){
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+
              e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
              ","+e.numTubes());
    pw.flush();
  }

  boolean firstWI=true;
  void writeIndiv(int gen,int indiv,Genome genome,
                  double score,double score2,PrintWriter pw)
  {
    if (firstWI){
      pw.println("*I,gen,ind,score,score2,len[i],var[i],init[i],initS[i],"+
                 "initD[i],delta[i],deltaS[i],deltaD[i]");
      firstWI=false;
    }
    pw.print("I,"+gen+","+indiv+","+score+","+score2);
    for (int i=0;i<Genome.GenomeSize;i++){
      pw.print(","+genome.gene[i].len+","+genome.gene[i].variation+","+
               genome.gene[i].init+","+genome.gene[i].initS+","+genome.gene[i].initD+","+
               genome.gene[i].delta+","+genome.gene[i].deltaS+","+genome.gene[i].deltaD);
    }
    pw.println();pw.flush();
  }
}
