import com.cellmatrix.chemcomp.API.v1.*;
//
// main execution thread. started by analyzecontrol, and used
// to run the simulation

public class AnalyzeCore extends Thread{
  int stepLimit=0;
  boolean running;
  EEXIST e;Display disp;
  boolean checkOutput=false;
  int delayTime=0;

  public void setDelayTime(int delayTime)
  {
    this.delayTime=delayTime;
  }

  public AnalyzeCore(EEXIST e, Display disp)
  {
    this.e=e;this.disp=disp;
  }

  public void checkOutput(boolean checkOutput)
  {
    this.checkOutput=checkOutput;
  }

  public void halt() // allows halt to be requested
  {
    running=false;
    checkOutput=false; // no output monitor
  }
  
  public void myDelay(int t)
  {
    try{Thread.sleep(t);} catch(Exception e){}
    return;
  }

  public void run()
  {
    int steps=0;
    running=true;
// main simulation loop
    while (running){
      e.step(1); // advance sim
      myDelay(delayTime); // pause a bit
//System.out.println("run: steps=" + steps + " limit=" + stepLimit);
// display test results if requested
      if (checkOutput){
        for (double x=24;x<28;x+=e.getDx()){
          double sum=e.getTube(x, EEXIST.SRC)+e.getTube(x, EEXIST.DST);
          if (sum > 15) System.out.print("1");
          else if (sum < 10) System.out.print("0");
          else System.out.print("?");
        } // this output evaluated
        System.out.println();
      } // end of output monitor

// finished?
      if ((stepLimit!=0) && (++steps==stepLimit)) running=false;
    }
    if (checkOutput){
      System.out.println("\n----------------------------------------\n");
    }
  } // thread exits here

  public void setLimit(int stepLimit)
  {
    this.stepLimit=stepLimit;
  }

}
