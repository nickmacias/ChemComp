//
// control panel class
//

import com.cellmatrix.chemcomp.API.v1.*;
import java.awt.EventQueue;
import java.util.Scanner;
import java.io.File;

public class AnalyzeControl extends Thread{
  EEXIST e;
  Display disp;
  AnalyzeCore core=null; // create this when we want to run forward

  String fileName;
  Scanner fileScanner=null; // for reading from raw file

  int delay=0; // inter-step delay

  public int getDelay()
  {
    return(delay);
  }

  Genome thisInd=null; // loaded from file

// main method: construct EEXIST, display and control panel
// then idle until user starts thread

  public void run()
  {
    e=new EEXIST(); // main EEXIST object
    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display

    disp.cursor(0);
    disp.cursor(4);
    disp.cursor(24);
    disp.cursor(28);

    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this,ControlPanel.Slider,0,10,0,400,1,"Karma","cpHandler");
    cp.connect(this,ControlPanel.Slider,1,128,0,255,2,"Intensity","cpHandler");
    cp.connect(this,ControlPanel.CheckBox,0,10,"Run/Pause","cpHandler");
    cp.connect(this,ControlPanel.TextIn,0,11,"Command","cpHandler");
  }
// gene.load(e);e.step(1);
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 2: // intensity
      disp.setIntensity(arg.getSliderPos());
      break;
    case 10: // Run/Pause
      System.out.println(arg.getCheckState()?"Running":"Paused");
      break;
    case 11: // text input
      System.out.println("<"+arg.getTextIn()+">");
      if (!parse(arg.getTextIn())) System.exit(0); // returns FALSE after Q command
      break;
    }
  }

  boolean parse(String s)
//
// r - run forever
// r n - run n steps
// h - halt
// f filename - name input file
// l gen ind - load this individual
// l - reload last individual
// t b0 b1 - run a test!
// d [#] - set inter-step delay
// x - load a random tube (debug gadget)
// Q - quit all
//
  {
    String[] sa=s.split(" ");
    for (int i=0;i<sa.length;i++){
      System.out.println("["+i+"]:<"+sa[i]+">");
    }
    if (sa.length < 1) return(true); // just an ENTER?

// QUIT
    if (sa[0].equals("Q")){
      if (core!=null) core.halt();
      return(false);
    }

// RUN commands
    if (sa[0].equals("h")){
      if (core != null){
        core.halt(); // signal core to exit
        core=null; // and remember that it's gone
        return(true);
      }
    }
    if (sa[0].equals("r")){
      if (core==null){
        core=new AnalyzeCore(e,disp);
      }
      core.saveMe(this);
// is a length specified?
      if (sa.length==2){ // yes
        try{
          core.setLimit(Integer.parseInt(sa[1]));
        }catch(Exception e){
          System.out.println("Error parsing " + sa[1]);return(true);
        }
        core.start(); // start the thread
        core=null; // since this thread will exit shortly
      } else {
        core.setLimit(0); // no limit
        core.start(); // start the core thread; will exit after HALT of when done
      }
    }

// test :)
    if (sa[0].equals("t")){
     if (sa.length == 2){ // just name a ferquency
// get test number
        int testNum=Integer.parseInt(sa[1]);

// now tell the core system to tally the output, and run 128 steps
        if (core==null){
          core=new AnalyzeCore(e,disp);
        }
        core.saveMe(this);
        core.setLimit(1064); // total of 128 ticks (first 64:stabilize; next 64: test)
//%%%        core.setLimit(128); // total of 128 ticks (first 64:stabilize; next 64: test)
        core.testing(true,testNum); // tally and output running score
        core.start();
        core=null;
        return(true);
      } // end of "t num" command
      System.out.println("Expecting t testnum");
      return(true);
    } // end of "t"

// delay
    if (sa[0].equals("d")){
      if (sa.length != 2){
        System.out.println("Delay=" + delay);
      } else {
        delay=Integer.parseInt(sa[1]); // pass this to the core whenever we run...
      }
      return(true);
    }

// file
    if (sa[0].equals("f")){
      if (sa.length<2) return(true); // need a filename
      fileName=sa[1]; // save this
      try{
        fileScanner=new Scanner(new File(fileName));
        System.out.println(fileName + " opened succesfully for reading");
        fileScanner.close();
      }catch(Exception e){
        System.out.println("ERROR: Can't open " + sa[1]);
      }
      return(true);
    }

// random
    if (sa[0].equals("x")){
      e.setTube(40*Math.random(),(int)(2.*Math.random()),40.*Math.random());
      e.localPaint();
      return(true);
    }

// load an individual
    if (sa[0].equals("l")){ // check usage
      if (sa.length==1){ // load last individual
        e.clearAllTubes();
        thisInd.load(e);
        e.localPaint();
        return(true);
      }

      if (sa.length != 3){ // error
        System.out.println("Expecting l or l gen ind");
        return(true);
      }

// prepare to parse
      int gen,ind; // generation # and individual #
      try{
        gen=Integer.parseInt(sa[1]);
        ind=Integer.parseInt(sa[2]);
      }catch(Exception e){
        System.out.println("Parse error: l " + sa[1] + " " + sa[2]);
        return(true);
      }
// open the file and scan
      try{
        fileScanner=new Scanner(new File(fileName));
        System.out.println("Searching " + fileName);
        thisInd=load(fileScanner,gen,ind,e); // scan file and load this individual
        e.clearAllTubes();
        thisInd.load(e); // load into system
// note that the karma etc. will be setup as the file is scanned
        fileScanner.close();
        e.localPaint();
      }catch(Exception e){
        System.out.println("Can't open " + fileName);
        return(true);
      }
    }

    return(true);

  }

  Genome load(Scanner sc, int tgtGen, int tgtInd, EEXIST e)
  {
    Genome ret=new Genome(e.numTubes());
    while (sc.hasNextLine()){
      String temp=sc.nextLine();
// comment
      if (temp.charAt(0)=='*') continue; // comment
// setup
      if (temp.charAt(0)=='S'){ // setup EEXIST
        String[] toks=temp.split(",");
        double karma=Double.parseDouble(toks[1]);
        e.setKarma(karma); // adjust this as we scan the file
        System.out.println("Karma=" + karma);
        continue;
      }
// individual
      if (temp.charAt(0)=='I'){
        String[] toks=temp.split(",");
        int gen=Integer.parseInt(toks[1]);
        int ind=Integer.parseInt(toks[2]);
        double score=Double.parseDouble(toks[3]);
        double score2=Double.parseDouble(toks[4]);
        if ((gen != tgtGen)||(ind != tgtInd)) continue;
// found the individual here
        for (int i=0;i<Genome.GenomeSize;i++){
          ret.gene[i].len=Integer.parseInt(toks[5+i*8]);
          ret.gene[i].variation=Double.parseDouble(toks[6+i*8]);
          ret.gene[i].init=Double.parseDouble(toks[7+i*8]);
          ret.gene[i].initS=Double.parseDouble(toks[8+i*8]);
          ret.gene[i].initD=Double.parseDouble(toks[9+i*8]);
          ret.gene[i].delta=Double.parseDouble(toks[10+i*8]);
          ret.gene[i].deltaS=Double.parseDouble(toks[11+i*8]);
          ret.gene[i].deltaD=Double.parseDouble(toks[12+i*8]);
        }
        System.out.println("Score=" + score + "," + score2);
        sc.close(); // done with file for now
        return(ret); // return the genome
      }
// keep reading
    } // EOF
    sc.close();
    System.out.println("WARNING: Didn't find gen " + tgtGen +
                       ", Individual " + tgtInd);
    return(null);
  }

}
