import com.cellmatrix.EEXIST.API_V2.*;
import java.io.PrintWriter;

public class Core extends Thread{
  EEXIST e;
  Display disp;
  
  int popSize=25; // total # of individuals in the population
  int survivorSize=5; // # of individuals to keep after each generation

  PrintWriter pw;

// the population!
  Genome[] population=new Genome[popSize];
  Genome[] best=new Genome[survivorSize]; // save best members here
  double[] score=new double[popSize]; // save scores

// here's where we run the entire simulation...
  public void run()
  {
    try{
      pw=new PrintWriter("raw.txt");
    } catch(Exception e){
      System.out.println("Can't write to output file ("+e+")- goodbye");
      return;
    }

    e=new EEXIST(0,40,.25,0,40,.25);
    e.proportionalFlow(true);
    //e.DEBUG=true;
    e.setKarma(0.5);
    disp=new Display(e);     // create a new display
    disp.setVisible(true);  // show the display

    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,5,0,400,1,"Karma","cpHandler");

    writeStatus(pw,e); // write relevant info about EEXIST
    int gen=0; // track generations

// make an initial population
    for (int i=0;i<popSize;i++){ 
      population[i]=new Genome();
      population[i].randomize();
    }
// main simulation loop
    while (true){
      ++gen;
      for (int indiv=0;indiv<popSize;indiv++){
        population[indiv].load(e); // load this individual into the system
        score[indiv]=0; // initial score
// should we disable the pre-stabilize step? %%%
        for (int test=0;test<8;test++){
disp.setTitle("EEXIST Gen " + gen + " Indiv " + indiv + " Test " + test);
          population[indiv].load(e); // re-load
          score[indiv]+=assess(test,e); // run a test, tally the score
        } // this individual assessed

        double score2=0;
        writeIndiv(pw,gen,indiv,population[indiv],score[indiv]);
        System.out.print(gen + ":" + indiv + "(" + score[indiv] + ")");

        if (indiv%5==4) System.out.println();
      } // end of evaluation of all individuals
      System.out.println("\nGen " + gen + " Best score=" + rank() + "\n"); // rank all individuals
      breed(.20); // breed them, with mutation rate of 5%
    } // and repeat forever
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      writeStatus(pw,e); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    }
  }

  double assess(int testNum,EEXIST e) // assess this individual
  {
    boolean a=((testNum&1) != 0);
    boolean b=((testNum&2) != 0);
    boolean c=((testNum&4) != 0);
    return(singleTest(a,b,c,testNum,e));
  }

  double singleTest(boolean a, boolean b, boolean c, int testNum,EEXIST e)
  {

// %%% Test Function Here

    //boolean out=!(a&b&c); // nand
    //boolean out=!(a|b|c); // nor
    boolean out=(a^b^c); // XOR
    
    double score=0; // running score

    //randomizeChems();

    e.clearAllTubes();

    // input a is placed in [0,4]; b in [8,12]; c in [16,20]; out is read from [24,28]
    //loadChems(0,0,4,4,a?20:5); setDiams(0,0,4,4,1);
    //loadChems(8,8,12,12,b?20:5); setDiams(8,8,12,12,1);
    //loadChems(16,16,20,20,c?20:5); setDiams(16,16,20,20,1); // or set diam=0 :)
    loadChems(0,0,4,4,a); setDiams(0,0,4,4,1);
    loadChems(8,8,12,12,b); setDiams(8,8,12,12,1);
    loadChems(16,16,20,20,c); setDiams(16,16,20,20,1); // or set diam=0 :)

    loadChems(24,24,28,28,10,15,20,25); // dump some chemicals in to work with

    // step 25 ticks, then read value for 10 more
    for (int i=0;i<25;i++){
      e.step(1);  // pre-process/stabilize
    }
    for (int i=0;i<25;i++){ // 25
      e.step(1);
      score+=readChems(out,24,24,28,28); // read chem levels in region, and adjust score accordingly
    } // end of tests
    return(score);
  }

  private void randomizeChems()
  {
  // load some chemicals
    double maxX=e.getMaxX();
    double maxY=e.getMaxY();
    for (int i=0;i<50;i++){
      double xx=maxX*Math.random();
      double yy=maxY*Math.random();
      double r1=maxX*Math.random();
      double r2=maxY*Math.random();
      double r3=maxX*Math.random();
      double r4=maxY*Math.random();

      for (double x=xx;x<xx+2;x+=e.getDx()){
        for (double y=yy;y<yy+2;y+=e.getDy()){
          e.setTube(x,y,EEXIST.SRCX,r1);
          e.setTube(x,y,EEXIST.SRCY,r2);
          e.setTube(x,y,EEXIST.DSTX,r3);
          e.setTube(x,y,EEXIST.DSTY,r4);
        }
      }
    }
  }

// read chems in a region and tally correct ones (y shows correct value)
  int readChems(boolean out, double startX,double startY,double endX,double endY)
  {
    int tally=0;
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        double sum=e.getTube(x,y,EEXIST.SRCX) + e.getTube(x,y,EEXIST.SRCY) +
                   e.getTube(x,y,EEXIST.DSTX) + e.getTube(x,y,EEXIST.DSTY);
        if ((out && sum>70) || ((!out) && sum<70)) ++tally; // correct value
      }
    }
    return(tally); // total # of correct tubes
  }

// load a square region of chemicals (same value throughout!)
  void loadChems(double startX, double startY, double endX, double endY, double v1, double v2, double v3, double v4)
  {
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        e.setTube(x,y,EEXIST.SRCX,v1);
        e.setTube(x,y,EEXIST.SRCY,v2);
        e.setTube(x,y,EEXIST.DSTX,v3);
        e.setTube(x,y,EEXIST.DSTY,v4);
      }
    }
  }

// load a square region of chemicals based on a boolean input
  void loadChems(double startX, double startY, double endX, double endY, boolean value)
  {
    double xAmt,yAmt;
    if (value){xAmt=25;yAmt=10;} else {xAmt=15; yAmt=20;}
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        e.setTube(x,y,EEXIST.SRCX,xAmt);
        e.setTube(x,y,EEXIST.SRCY,yAmt);
        e.setTube(x,y,EEXIST.DSTX,xAmt);
        e.setTube(x,y,EEXIST.DSTY,yAmt);
      }
    }
  }

// set a square region of diameters
  void setDiams(double startX, double startY, double endX, double endY, double value)
  {
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        e.setDiameter(x,y,value);
      }
    }
  }

  double rank() // sort population[] by score[]
  {
    for (int loop=0;loop<popSize;loop++){ // good old bubble sort!
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (score[i+1] > score[i]){ // swap
          double temp=score[i+1];score[i+1]=score[i];score[i]=temp;
          Genome temp2=population[i+1];population[i+1]=population[i];population[i]=temp2;
        }
      }
    }

// copy best elements of population to best[] array
    double bestScore=0;
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i].copy();
      bestScore+=score[i];
    }
    return(score[0]);
  }
  
  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy();
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].merge(best[(int)(Math.random()*survivorSize)]);
        population[i].mutate(mutateRate); // random variation
      }
    }
  }

  boolean firstWS=true;
  void writeStatus(PrintWriter pw,EEXIST e)
  {
    if (firstWS){
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
                                     e.getDy()+","+e.getMaxY()+","+e.getMinY());
    pw.flush();
  }

  boolean firstWI=true;

  void writeIndiv(PrintWriter pw,int gen,int indiv,Genome genome,double score)
  {
    if (firstWI){
      pw.println("*I,gen,ind,score,initSX[x][y],initSY[x][y],initDX[x][y],initDY[x][y],delta[x][y],deltaSX[x][y],deltaSY[x][y],deltaDX[x][y],deltaDY[x][y]");
      firstWI=false;
    }
    pw.print("I,"+gen+","+indiv+","+score);
    for (int x=0;x<Genome.GenomeSize;x++){
      for (int y=0;y<Genome.GenomeSize;y++){
        pw.print(","+
               genome.genes[x][y].initSX+"," +
               genome.genes[x][y].initSY+"," +
               genome.genes[x][y].initDX+"," +
               genome.genes[x][y].initDY+"," +
               genome.genes[x][y].delta+","  +
               genome.genes[x][y].deltaSX+","+
               genome.genes[x][y].deltaSY+","+
               genome.genes[x][y].deltaDX+","+
               genome.genes[x][y].deltaDY);
      }
    }
    pw.println();pw.flush();
  }
}
