import com.cellmatrix.EEXIST.API_V2.*;
/*
 * Definition of a single gene. The genome of the organism is comprised of a
 * matrix of genes.
 *
 * Only doing bias gradients for now...
 */
public class Gene {
  public double initSX, initSY, initDX, initDY; // initial biases (min x, min y)
  public double delta; // stepsize across gene
  public double deltaSX,deltaSY,deltaDX, deltaDY; // Bias changes per step (delta)
  
  Gene copy() // make a copy
  {
	  Gene temp=new Gene();
	  temp.initSX=initSX;
	  temp.initSY=initSY;
	  temp.initDX=initDX;
	  temp.initDY=initDY;
	  temp.delta=delta;
	  temp.deltaSX=deltaSX;
	  temp.deltaSY=deltaSY;
	  temp.deltaDX=deltaDX;
	  temp.deltaDY=deltaDY;
	  return(temp);
  }

  void randomize()
  {
    initSX=20.*Math.random();
    initSY=20.*Math.random();
    initDX=20.*Math.random();
    initDY=20.*Math.random();
    delta=(Math.random())-.5; //-.5 to +.5
    deltaSX=(Math.random())-.5;
    deltaSY=(Math.random())-.5;
    deltaDX=(Math.random())-.5;
    deltaDY=(Math.random())-.5;
// misc adjustments...
    delta=delta/5.;
    deltaDX=deltaDX/5.;
    deltaDY=deltaDY/5.;
    deltaSX=deltaSX/5.;
    deltaSY=deltaSY/5.;
  }
}
