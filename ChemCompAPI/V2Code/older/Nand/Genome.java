import com.cellmatrix.EEXIST.API_V2.*;

/*
 * Genome of an organism
 * This is comprised of a matrix of genes, plus
 * a set of methods for manipulating the genome
 */
public class Genome {
  static int GenomeSize=8; // # of genes in the genome *per dimension*
  
  public Gene[][] genes=new Gene[Genome.GenomeSize][Genome.GenomeSize];
  
  // just construct each gene
  Genome()
  {
    for (int x=0;x<GenomeSize;x++){
      for (int y=0;y<GenomeSize;y++){
        genes[x][y]=new Gene(); // everything should be initialized to 0...
      }
    }
  }
  
  void load(EEXIST e) // load the genome into the machine
  {
// zero-out the system's chemicals
    e.clearAllTubes();
    int xLen=e.numXBins()/GenomeSize; // # of tubes per genome
    int yLen=e.numYBins()/GenomeSize; // # of tubes per genome

// sweep the entire matrix of genes
    for (int x=0;x<GenomeSize;x++){
      for (int y=0;y<GenomeSize;y++){
        Gene g=genes[x][y]; // next gene to load
        loadOneGene(g,x*xLen,y*yLen,xLen,yLen,e);
      }
    }
  }

  private void loadOneGene(Gene g, int xStart, int yStart, int xLen, int yLen, EEXIST e)
  {
    double sx,sy,dx,dy;

// iterate over the gene's 2D surface
    for (int ix=0;ix<xLen;ix++){
      for (int iy=0;iy<yLen;iy++){ // load into tube [ix+xStart,iy+yStart]
        int xx=ix+xStart;
        int yy=iy+yStart;

// find bias value at loc ix,iy
        sx=g.initSX + g.deltaSX*ix;
        sy=g.initSY + g.deltaSY*iy;
        dx=g.initDX + g.deltaDX*ix;
        dy=g.initDY + g.deltaDY*iy;

// make sure these are range
        sx=normalize(sx,e.getMinX(),e.getMaxX());
        sy=normalize(sy,e.getMinY(),e.getMaxY());
        dx=normalize(dx,e.getMinX(),e.getMaxX());
        dy=normalize(dy,e.getMinY(),e.getMaxY());

// and load into EEXIST
        e.setBias(xx,yy, EEXIST.SRCX, sx);
        e.setBias(xx,yy, EEXIST.SRCY, sy);
        e.setBias(xx,yy, EEXIST.DSTX, dx);
        e.setBias(xx,yy, EEXIST.DSTY, dy);
      }
    } // Entire gene loaded
  }
  
  private double normalize(double s, double min, double max) // make sure this is in range
  {
    if (s < min) s+=max; // kind of assumes min=0 :)
    if (s > max) s-=max;
    return(s);
  }

  Genome copy() // make a copy
  {
    Genome temp=new Genome();
    for (int x=0;x<GenomeSize;x++){
      for (int y=0;y<GenomeSize;y++){
        temp.genes[x][y]=genes[x][y].copy();
      }
    }
    return(temp);
  }

// tweak current genome's genes
  void mutate(double amount)
  {
    for (int x=0;x<GenomeSize;x++){
      for (int y=0;y<GenomeSize;y++){
        if (Math.random() < amount) genes[x][y].delta*=(.5+Math.random()); // .x5-1.5x
        if (Math.random() < amount) genes[x][y].deltaDX*=(.5+Math.random()); // .x5-1.5x
        if (Math.random() < amount) genes[x][y].deltaDY*=(.5+Math.random()); // .x5-1.5x
        if (Math.random() < amount) genes[x][y].deltaSX*=(.5+Math.random()); // .x5-1.5x
        if (Math.random() < amount) genes[x][y].deltaSY*=(.5+Math.random()); // .x5-1.5x
        if (Math.random() < amount) genes[x][y].initDX*=(.5+Math.random()); // .x5-1.5x
        if (Math.random() < amount) genes[x][y].initDY*=(.5+Math.random()); // .x5-1.5x
        if (Math.random() < amount) genes[x][y].initSX*=(.5+Math.random()); // .x5-1.5x
        if (Math.random() < amount) genes[x][y].initSY*=(.5+Math.random()); // .x5-1.5x
      }
    }
  }

// make with a given genome
  void merge(Genome g)
  {
    for (int x=0;x<GenomeSize;x++){
      for (int y=0;y<GenomeSize;y++){
      // TODO may want to actually merge (average) the 2 genes
        genes[x][y].delta=(genes[x][y].delta+g.genes[x][y].delta)/2.;
        genes[x][y].deltaDX=(genes[x][y].deltaDX+g.genes[x][y].deltaDX)/2.;
        genes[x][y].deltaDY=(genes[x][y].deltaDY+g.genes[x][y].deltaDY)/2.;
        genes[x][y].deltaSX=(genes[x][y].deltaSX+g.genes[x][y].deltaSX)/2.;
        genes[x][y].deltaSY=(genes[x][y].deltaSY+g.genes[x][y].deltaSY)/2.;
        genes[x][y].initDX=(genes[x][y].initDX+g.genes[x][y].initDX)/2.;
        genes[x][y].initDY=(genes[x][y].initDY+g.genes[x][y].initDY)/2.;
        genes[x][y].initSX=(genes[x][y].initSX+g.genes[x][y].initSX)/2.;
        genes[x][y].initSY=(genes[x][y].initSY+g.genes[x][y].initSY)/2.;
      /***
      if (Math.random() < .5){ // replace this gene
        gene[i]=g.gene[i].copy();
      }
      ***/

      }
    }
  }

  void randomize()
  {
    for (int x=0;x<GenomeSize;x++){
      for (int y=0;y<GenomeSize;y++){
        genes[x][y].randomize();
      }
    }
  }
}
