import com.cellmatrix.EEXIST.API_V2.*;
import java.io.PrintWriter;

public class Core extends Thread{
  EEXIST e;
  Display disp;
  
  int popSize=25; // total # of individuals in the population
  int survivorSize=5; // # of individuals to keep after each generation

  PrintWriter pw;

// the population!
  Genome[] population=new Genome[popSize];
  Genome[] best=new Genome[survivorSize]; // save best members here
  double[] score=new double[popSize]; // save scores

// here's where we run the entire simulation...
  public void run()
  {
    try{
      pw=new PrintWriter("raw.txt");
    } catch(Exception e){
      System.out.println("Can't write to output file ("+e+")- goodbye");
      return;
    }

    e=new EEXIST(0,40,.0625,0,40,.0625);
    e.proportionalFlow(false);
    e.setKarma(4);
    disp=new Display(e);     // create a new display
    disp.setVisible(true);  // show the display

    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,50,0,400,1,"Karma","cpHandler");

    writeStatus(pw,e); // write relevant info about EEXIST
    int gen=0; // track generations

// make an initial population
    for (int i=0;i<popSize;i++){ 
      population[i]=new Genome();
      population[i].randomize();
    }
// main simulation loop
    while (true){
      ++gen;
      for (int indiv=0;indiv<popSize;indiv++){
        population[indiv].load(e); // load this individual into the system
        score[indiv]=0; // initial score
// should we disable the pre-stabilize step? %%%
        for (int test=0;test<8;test++){
disp.setTitle("EEXIST Gen " + gen + " Indiv " + indiv + " Test " + test);
          population[indiv].load(e); // re-load
          score[indiv]+=assess(test,e); // run a test, tally the score
        } // this individual assessed

        double score2=0;
        writeIndiv(pw,gen,indiv,population[indiv],score[indiv]);
        System.out.print(gen + ":" + indiv + "(" + score[indiv] + ")");

        if (indiv%5==4) System.out.println();
      } // end of evaluation of all individuals
      System.out.println("\nGen " + gen + " Best score=" + rank() + "\n"); // rank all individuals
      breed(.20); // breed them, with mutation rate of 5%
    } // and repeat forever
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      writeStatus(pw,e); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    }
  }

  double assess(int testNum,EEXIST e) // assess this individual
  {
    boolean a=((testNum&1) != 0);
    boolean b=((testNum&2) != 0);
    boolean c=((testNum&4) != 0);
    return(singleTest(a,b,c,testNum,e));
  }

  double singleTest(boolean a, boolean b, boolean c, int testNum,EEXIST e)
  {

// %%% Test Function Here

    //boolean out=!(a&b&c); // nand
    //boolean out=!(a|b|c); // nor
    boolean out=(a^b^c); // XOR
    
    double score=0; // running score

    //randomizeChems();

    e.clearAllTubes();

    // input a is placed in [0,4]; b in [8,12]; c in [16,20]; out is read from [24,28]
    loadChems(0,0,4,4,a?20:5); setDiams(0,0,4,4,0);
    loadChems(8,8,12,12,b?20:5); setDiams(8,8,12,12,0);
    loadChems(16,16,20,20,c?20:5); setDiams(16,16,20,20,0);

    loadChems(24,24,32,32,30); // dump some chemicals in to work with

    // step 10 ticks, then read value for 15 more
    for (int i=0;i<10;i++){
      e.step(1);  // pre-process/stabilize
    }
    for (int i=0;i<15;i++){
      e.step(1);
// get a consensus true/false value from output region
// and see if it matches desired output ("out")
      boolean output=readChems(out,26,26,30,30);

// and if it matches desired output, bump up the score
      if (output) ++score;
    } // end of tests
    return(score);
  }

  private void randomizeChems()
  {
  // load some chemicals
    double maxX=e.getMaxX();
    double maxY=e.getMaxY();
    for (int i=0;i<50;i++){
      double xx=maxX*Math.random();
      double yy=maxY*Math.random();
      double r1=maxX*Math.random();
      double r2=maxY*Math.random();
      double r3=maxX*Math.random();
      double r4=maxY*Math.random();

      for (double x=xx;x<xx+2;x+=e.getDx()){
        for (double y=yy;y<yy+2;y+=e.getDy()){
          e.setTube(x,y,EEXIST.SRCX,r1);
          e.setTube(x,y,EEXIST.SRCY,r2);
          e.setTube(x,y,EEXIST.DSTX,r3);
          e.setTube(x,y,EEXIST.DSTY,r4);
        }
      }
    }
  }

// read chems in a region and generate a consensus true/false
  boolean readChems(boolean out,
                double startX,double startY,
                double endX,double endY)
  {
    int tally=0,all=0;
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        ++all; // total # of reads
        double sum=e.getTube(x,y,EEXIST.SRCX) + e.getTube(x,y,EEXIST.SRCY) +
                   e.getTube(x,y,EEXIST.DSTX) + e.getTube(x,y,EEXIST.DSTY);
        if (sum >= 60) ++tally; // avg >= 15
      }
    }
    if (out && (3*tally >= 2*all)) return(true); // at least 2/3 read true
    if ((!out) && (3*tally <= all)) return(true); // <= 1/3 true
    return(false); // grey zone...
  }

// load a square region of chemicals (same value throughout!)
  void loadChems(double startX, double startY, double endX, double endY, double value)
  {
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        e.setTube(x,y,EEXIST.SRCX,value);
        e.setTube(x,y,EEXIST.SRCY,value);
        e.setTube(x,y,EEXIST.DSTX,value);
        e.setTube(x,y,EEXIST.DSTY,value);
      }
    }
  }

// set a square region of diameters
  void setDiams(double startX, double startY, double endX, double endY, double value)
  {
    for (double x=startX;x<endX;x+=e.getDx()){
      for (double y=startY;y<endY;y+=e.getDy()){
        e.setDiameter(x,y,value);
      }
    }
  }

  double rank() // sort population[] by score[]
  {
    for (int loop=0;loop<popSize;loop++){ // good old bubble sort!
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (score[i+1] > score[i]){ // swap
          double temp=score[i+1];score[i+1]=score[i];score[i]=temp;
          Genome temp2=population[i+1];population[i+1]=population[i];population[i]=temp2;
        }
      }
    }

// copy best elements of population to best[] array
    double bestScore=0;
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i].copy();
      bestScore+=score[i];
    }
    return(score[0]);
  }
  
  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy();
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].merge(best[(int)(Math.random()*survivorSize)]);
        population[i].mutate(mutateRate); // random variation
      }
    }
  }

  boolean firstWS=true;
  void writeStatus(PrintWriter pw,EEXIST e)
  {
    if (firstWS){
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
                                     e.getDy()+","+e.getMaxY()+","+e.getMinY());
    pw.flush();
  }

  boolean firstWI=true;

  void writeIndiv(PrintWriter pw,int gen,int indiv,Genome genome,double score)
  {
    if (firstWI){
      pw.println("*I,gen,ind,score,initSX[x][y],initSY[x][y],initDX[x][y],initDY[x][y],delta[x][y],deltaSX[x][y],deltaSY[x][y],deltaDX[x][y],deltaDY[x][y]");
      firstWI=false;
    }
    pw.print("I,"+gen+","+indiv+","+score);
    for (int x=0;x<Genome.GenomeSize;x++){
      for (int y=0;y<Genome.GenomeSize;y++){
        pw.print(","+
               genome.genes[x][y].initSX+"," +
               genome.genes[x][y].initSY+"," +
               genome.genes[x][y].initDX+"," +
               genome.genes[x][y].initDY+"," +
               genome.genes[x][y].delta+","  +
               genome.genes[x][y].deltaSX+","+
               genome.genes[x][y].deltaSY+","+
               genome.genes[x][y].deltaDX+","+
               genome.genes[x][y].deltaDY);
      }
    }
    pw.println();pw.flush();
  }
}
