import com.cellmatrix.chemcomp.API.v1.*;
import java.io.PrintWriter;

public class Core extends Thread{
  EEXIST e;
  Display disp;
  
  int popSize=250; // total # of individuals in the population
  int survivorSize=10; // # of individuals to keep after each generation

  PrintWriter pw;

  Genome[] population=new Genome[popSize];
  Genome[] best=new Genome[survivorSize]; // save best members here
  double[] score=new double[popSize]; // save scores

  public void run()
  {
    try{
      pw=new PrintWriter("raw.txt");
    } catch(Exception e){
      System.out.println("Can't write to output file ("+e+")- goodbye");
      return;
    }
    e=new EEXIST(); // main EEXIST object
    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display
    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,50,0,400,1,"Karma","cpHandler");
    cp.connect(this, ControlPanel.Slider,1,128,0,255,2,"Intensity","cpHandler");

    writeStatus(e,pw); // write relevant info about EEXIST
    int gen=0; // track generations

// make an initial population
    for (int i=0;i<popSize;i++){ 
      population[i]=new Genome(e.numTubes());
      population[i].randomize();
    }
// main simulation loop
    while (true){
      ++gen;
      for (int indiv=0;indiv<popSize;indiv++){
        population[indiv].load(e); // load this individual into the system
        score[indiv]=assess(e); // run a test, tally the score
// now try out-of-band data
        double score2=0;
        writeIndiv(gen,indiv,population[indiv],score[indiv],score2,pw);
        System.out.print(gen + ":" + indiv + "(" + score[indiv] +
                         "," + score2 + ")");

        //if (indiv%5==4) System.out.println();
      } // end of evaluation of all individuals
      System.out.println("\nGen " + gen + " Best score=" + rank() + "\n"); // rank all individuals
      breed(.05); // breed them, with mutation rate of 5%
    } // and repeat forever
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      writeStatus(e,pw); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 2: // intensity
      disp.setIntensity(arg.getSliderPos());
      break;
    }
  }

  double assess(EEXIST e) // assess this individual
  {
    double score=0; // running score

// input numbers in [0,4] [8,12] [16,20] [24,28] [32,36] and [36,40]
// step 50x, compare inputs to next picks.
// score is product of 1+abs(diff) of each pick
/***
(most recent)
17,68,19,43,39,13
64,43,60,9,15,4
35,20,49,26,24,19
12,30,47,62,36,9
33,21,45,11,28,11
(oldest)
***/

    load(e,11,21,28,33,45,11);
    e.step(50);score+=compare(e,12,30,36,47,62,9);
    e.step(50);score+=compare(e,20,24,26,35,49,19);
    e.step(50);score+=compare(e,9,15,43,60,64,4);
    e.step(50);score+=compare(e,17,19,39,43,68,13);
    e.step(50);readout(e,score);
    return(score);
  }

  public void readout(EEXIST e,double score)
  {
    System.out.print("> Score: " + score);
    System.out.print(" " + getBall(e,0));
    System.out.print(" " + getBall(e,8));
    System.out.print(" " + getBall(e,16));
    System.out.print(" " + getBall(e,24));
    System.out.print(" " + getBall(e,32));
    System.out.println(" " + getBall(e,36));
  }

// load (initial) pick into system
  public void load(EEXIST e, double b1, double b2, double b3,
                             double b4, double b5, double pb)
  {
    for (double x=0;x<4;x+=e.getDx()){
      e.setTube(x+0,EEXIST.SRC,b1/2);e.setTube(x+0,EEXIST.DST,b1/2);
      e.setTube(x+8,EEXIST.SRC,b2/2);e.setTube(x+8,EEXIST.DST,b2/2);
      e.setTube(x+16,EEXIST.SRC,b3/2);e.setTube(x+16,EEXIST.DST,b3/2);
      e.setTube(x+24,EEXIST.SRC,b4/2);e.setTube(x+24,EEXIST.DST,b4/2);
      e.setTube(x+32,EEXIST.SRC,b5/2);e.setTube(x+32,EEXIST.DST,b5/2);
      e.setTube(x+36,EEXIST.SRC,pb/2);e.setTube(x+36,EEXIST.DST,pb/2);
    }
  }

// compare EEXIST's outputs to target picks
  public double compare(EEXIST e, double b1, double b2, double b3,
                                  double b4, double b5, double pb)
  {
    double score=1,real;

    score*=(1+(Math.abs(getBall(e,0)-b1)));
    score*=(1+(Math.abs(getBall(e,8)-b2)));
    score*=(1+(Math.abs(getBall(e,16)-b3)));
    score*=(1+(Math.abs(getBall(e,24)-b4)));
    score*=(1+(Math.abs(getBall(e,32)-b5)));
    score*=(1+(Math.abs(getBall(e,36)-pb)));
    return(score); // best score=1; no match>=64
  }

  public double getBall(EEXIST e, double xStart)
  {
    double sum=0;
    double count=0;
    for (double x=xStart;x<4+xStart;x+=e.getDx()){
      sum+=e.getTube(x, EEXIST.SRC)+e.getTube(x, EEXIST.DST);
      ++count;
    }
// scale by # of tubes
    sum=sum/count;
    return(Math.round(sum));
  }

  double rank() // sort population[] by score[]
  {
    for (int loop=0;loop<popSize;loop++){ // good old bubble sort!
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (score[i+1] < score[i]){ // swap
          double temp=score[i+1];score[i+1]=score[i];score[i]=temp;
          Genome temp2=population[i+1];population[i+1]=population[i];population[i]=temp2;
        }
      }
    }

// copy best elements of population to best[] array
    double bestScore=0;
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i].copy();
      bestScore+=score[i];
    }
    return(score[0]);
  }
  
  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy();
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].merge(best[(int)(Math.random()*survivorSize)]);
        population[i].mutate(mutateRate); // random variation
      }
    }
  }

  boolean firstWS=true;
  void writeStatus(EEXIST e,PrintWriter pw)
  {
    if (firstWS){
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+
              e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
              ","+e.numTubes());
    pw.flush();
  }

  boolean firstWI=true;
  void writeIndiv(int gen,int indiv,Genome genome,
                  double score,double score2,PrintWriter pw)
  {
    if (firstWI){
      pw.println("*I,gen,ind,score,score2,len[i],var[i],init[i],initS[i],"+
                 "initD[i],delta[i],deltaS[i],deltaD[i]");
      firstWI=false;
    }
    pw.print("I,"+gen+","+indiv+","+score+","+score2);
    for (int i=0;i<Genome.GenomeSize;i++){
      pw.print(","+genome.gene[i].len+","+genome.gene[i].variation+","+
               genome.gene[i].init+","+genome.gene[i].initS+","+genome.gene[i].initD+","+
               genome.gene[i].delta+","+genome.gene[i].deltaS+","+genome.gene[i].deltaD);
    }
    pw.println();pw.flush();
  }
}
