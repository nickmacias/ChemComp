import com.cellmatrix.chemcomp.API.v1.*;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Pattern;

public class Core extends Thread{
  EEXIST e;
  Display disp;

  String tname="titanic.csv"; // raw data from Kaggle.Com
  
  int popSize=15; // total # of individuals in the population
  int survivorSize=5; // # of individuals to keep after each generation

  PrintWriter pw;
  ControlPanel cp=null;

  Genome[] population=new Genome[popSize];
  Genome[] best=new Genome[survivorSize]; // save best members here
  double[] score=new double[popSize]; // save scores

  public void run()
  {
    e=new EEXIST(0,60,0.0625); // main EEXIST object
    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display
    disp.cursor(36);
    disp.cursor(40);
    cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,50,0,400,1,"Karma","cpHandler");
    cp.connect(this,ControlPanel.TextIn,0,11,"cmd","cpHandler");
    cp.connect(this,ControlPanel.Button,0,20,"Exit","cpHandler");
    cp.connect(this,ControlPanel.Button,1,21,"Step","cpHandler");
    cp.connect(this,ControlPanel.Button,2,22,"Reset","cpHandler");
    cp.connect(this,ControlPanel.Button,3,23,"Compile","cpHandler");
    cp.setText(true); // make this visible; we'll read it to get source code :)

// main simulation loop
    while (true){
      //e.step(1);
    } // and repeat forever
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 20: // exit
      System.exit(0);
      break;
    case 21: // single-step
      e.step(1);
      break;
    case 22: // reset
      System.out.println("Resetting all memory");
      break;
    case 23: // compile
      compile(e,cp.getText());
      break;
    }
  }

/*
 * Each line of the program has one of the following formats:
 *
 * <blank line> - ignored
 * all white space if ignored
 * address:src->dst
 *
 * where address is a number or [num,num] or (num,num) or (num,num] or [num,num)
 * and src,dst are either address or address+bias
 *
 */

  void compile(EEXIST e,String text)
  {
    System.out.println("Compiling...");
    String[] code=text.split("\n"); // break into lines
    for (int i=0;i<code.length;i++){
      compile1(e,code[i]); // single-line compile
    }
  }

  void compile1(EEXIST e,String text) // compile one line
  {
// remove comments
    int cindex=text.indexOf(";");
    if (cindex >= 0){
      text=text.substring(0,cindex);
    }

// remove spaces
    text=text.replace(" ","");
    text=text.replace("\t","");

// if line is empty, take no action
    if (text.length()==0){
      System.out.println("EMPTY");
      return;
    }

// parse address:src->dst
    String parts[]=text.split("\\:");
    String address=parts[0];
    parts=parts[1].split("->");
    String src=parts[0];
    String dst=parts[1];

// first parse the address piece
    if (address.length() == 0){
      System.out.println("Syntax error: " + text);
      return;
    }

    double addr1=0, addr2=0;
    boolean range=false; // is a range specified
    boolean inc1=false,inc2=false; // is range boundary inclusive?

    if (address.indexOf(",") == -1){
      addr1=Double.parseDouble(address);
      addr2=addr1;
      inc1=inc2=true;
      range=false;
    } else {
      String[] par=address.split("\\,");
      addr1=Double.parseDouble(par[0].substring(1)); // skip [ or (
      addr2=Double.parseDouble(par[1].substring(0,par[1].length()-1)); // skip ] or )
      range=true;
      inc1=(address.charAt(0)=='[');
      inc2=(address.charAt(address.length()-1)==']');
    }

// process src and dst
    double srcval=0,srcbias=0,dstval=0,dstbias=0;
    if (src.indexOf("+") >= 0){
      String[] par=src.split("\\+");
      srcval=Double.parseDouble(par[0]);
      srcbias=Double.parseDouble(par[1]);
    } else {
      srcval=Double.parseDouble(src);
      srcbias=0;
    }

    if (dst.indexOf("+") >= 0){
      String[] par=dst.split("\\+");
      dstval=Double.parseDouble(par[0]);
      dstbias=Double.parseDouble(par[1]);
    } else {
      dstval=Double.parseDouble(dst);
      dstbias=0;
    }

// use range, addr1, addr2, inc1 and inc2 for addressing
// use srcval, srcbias, dstval and dstbias

    for (double x=addr1;x<=addr2;x+=e.getDx()){
      if ((!inc1) && x==addr1) continue;
      if ((!inc2) && x==addr2) continue; // skip if not inclusive
      e.setTube(x,EEXIST.SRC,srcval);
      e.setBias(x,EEXIST.SRC,srcbias);
      e.setTube(x,EEXIST.DST,dstval);
      e.setBias(x,EEXIST.DST,dstbias);
    }
    //System.out.println("range=" + range + " addr1=" + addr1 + " addr2=" + addr2 + " inc1=" + inc1 + " inc2=" + inc2);
    //System.out.println("srcval=" + srcval + " srcbias=" + srcbias + " dstval=" + dstval + " dstbias=" + dstbias);
    
    return;
  }

}
