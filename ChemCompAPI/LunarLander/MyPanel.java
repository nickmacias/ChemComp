/*
 * Simple lunar lander simulator
 */
import java.awt.Color;
import java.awt.Graphics;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

import javax.swing.JPanel;

public class MyPanel extends JPanel implements Runnable{
	
	double initAlt=200,initFuel=200,initSpeed=0;
	double fuel=initFuel,speed=initSpeed,altitude=initAlt;
	public boolean thrust=false;
	boolean done=true; // finished with this run

	double initXLoc=300;
	double xLoc=initXLoc;

	double gravAccel=-9.8; // gravity, m/s^2
	double thrustAccel=15.0; // upward thrust
	double dt=0.03125; // 1/32nd second
	
	Main parent;
	
	MyPanel(Main parent)
	{
	  this.parent=parent;
	}

	// main loop
	public void run()
	{
		Integer port;
		ServerSocket ss;
		try {
		    port=Integer.parseInt(parent.portField.getText());
			ss = new ServerSocket(port);
		} catch (Exception e1) {
			System.out.println("Error: Can't listen on port " + parent.portField.getText() +"\n"+e1);
			parent.btnStart.setEnabled(true);
			return;
		}
		
		Scanner sc=null;
		PrintWriter pw=null;
		Socket s=null;
		
// listen for connection request
		while (true){
	      try{
		    s=ss.accept();
		    reset();
		    sc=new Scanner(s.getInputStream());
		    pw=new PrintWriter(s.getOutputStream());
		    while (sc.hasNextLine()){
			    String buffer=sc.nextLine();
			    switch (buffer.charAt(0)){
			      case 'd':pw.println(status());pw.flush();break;
			      case 'r':reset();break;
			      case 't':thrust=false;repaint();break;
			      case 'T':if (fuel > 0) thrust=true;repaint();break;
			      case 's':step();break;
			      case 'Q':sc.close();break;
			      case 'I': // initial values: fuel speed alt gravAccel thrustAccel dt
			    	  String[] fields=buffer.split(" ");
			    	  initFuel=Double.parseDouble(fields[1]);
			    	  initSpeed=Double.parseDouble(fields[2]);
			    	  initAlt=Double.parseDouble(fields[3]);
			    	  gravAccel=Double.parseDouble(fields[4]);
			    	  thrustAccel=Double.parseDouble(fields[5]);
			    	  dt=Double.parseDouble(fields[6]);
			    	  break;
			      default:System.out.println("Unknown command: " + buffer);
			    }
		    }
		    System.out.println("Scanner closing");
		    pw.close();
		    sc.close();
		    s.close();
	      } catch(Exception e){
		    System.out.println("I/O error: " + e);
	      }
	      try {
			s.close();pw.close();sc.close();
		  } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  }
	   }
	}
	
	public MyPanel()
	{
		reset(); // ready for a new run
	}
	
	public String status() // return status string
	{
		return("done,"+done+",fuel,"+fuel+",speed,"+speed+",alt,"+altitude);
	}
	
	public void reset() // return to initial state
	{
		done=false;
		altitude=initAlt;
		fuel=initFuel;
		speed=initSpeed;
		thrust=false;
		xLoc=initXLoc;
		repaint();
	}
	
	public void step() // advance one timestep
	{
		if (done) return;
		
		if (altitude < .3) thrust=false; // cut engines automatically at .3m from surface
		double accel=gravAccel+((thrust)?thrustAccel:0); // current acceleration
		speed+=accel*dt;
		altitude+=speed*dt;
		if (altitude<=0) done=true;
		if (thrust) fuel-=dt; // so fuel is possible burn time in seconds
		if (fuel <= 0) thrust=false;
		repaint();
	}

	public void paint(Graphics g)
	{
		super.paint(g);
// ground
		g.fillRect(0, (int)initAlt+50+4, getWidth(), 4);
// show data
		g.setColor(Color.BLUE);
		g.drawString("Fuel:" + fuel,20,20);
		g.drawString("Speed:" + speed,20,40);
		g.drawString("Altitude:" + altitude,20,60);
// show ship
		g.setColor(Color.BLACK);
		int y=(int)(initAlt-altitude);
		g.fillOval((int)xLoc, y, 20, 50);
// show thrust
		g.setColor(Color.RED);
		if (thrust) g.fillOval((int)xLoc+5,y+25+20,10,20);
	}

}
