/*
 * Main drawing panel and main universe
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.JPanel;
import java.util.Random;

public class MyPanel extends JPanel{
  int numX=25, numY=15; // # of cells
  //int numX=10, numY=10; // # of cells
  int breedThreshold=4; // need this much energy to breed
  int maxPop=5; // pop limit for breeding
  int sizeX,sizeY; // cell size
  int nextID=1; // ID of next organism
  int nextEID=-1; // Special IDs for energy organisms
  boolean showGrid=true;
  boolean showIDs=false;
  boolean showEnergyIDs=false;
  VEco parent; // main class
  int intensity=25; // sets background intensity
  int flashPeriod=10; // how often tagged creatures change color
                      // timeStep is phase-shifted by ID, so we don't have
                      // all creatures changing at the same time
  int timeStep=0;

  public boolean breedingAllowed=true; // set with "B 0" or "B 1" command

  ConcurrentHashMap<Integer,Organism> orgHash;
  Organism[][] grid;

  Random rand; // our (seedable) random number generator

  void setIntensity(int intensity)
  {
    this.intensity=intensity;
  }

  public void saveParent(VEco parent)
  {
	this.parent=parent; // mainly to get port number, etc.
  }

  public MyPanel()
  {
	setPanelSize();
	
// build data structures for saving organisms
	orgHash=new ConcurrentHashMap<Integer,Organism>();
	grid=new Organism[numX][numY]; // these are mostly null...
        rand=new Random(1376191);
  }
  
  // find port number for our service
  int getPort()
  {
	  return(parent.getPort());
  }
  
  // set panel size based on # of cells and cell size
  void setPanelSize()
  {
      //sizeX=50;sizeY=50; // hardwired!
	  //setSize(numX*sizeX+1,numY*sizeY+1); // turn on for hardwired sizes

      sizeX=getWidth()/numX;
      sizeY=getHeight()/numY; // auto-set cell size for current panel dimensions
      repaint();
  }

  // uber paint method
  public void paint(Graphics g)
  {
          ++timeStep; // not really timesteps lol
	  super.paint(g);
	  
	      if (parent.showDailyCycle.isSelected()){
            g.setColor(new Color(10*intensity,10*intensity,10*intensity));
	      } else {
	        g.setColor(new Color(180,180,180));
	      }
          g.fillRect(0,0,getWidth(),getHeight());
// show cells
	  if (showGrid && sizeX*sizeY != 0){
	    g.setColor(Color.GRAY);
	    for (int i=0;i<=numX;i++){
	      g.drawLine(i*sizeX,0,i*sizeX,numY*sizeY);
	    }
	    for (int i=0;i<=numY;i++){
		  g.drawLine(0,i*sizeY,sizeX*numX,i*sizeY);
	    }
	  }
	  
// draw organisms
	  for (int id:orgHash.keySet()){
		Organism org=orgHash.get(id);
                if (org==null) continue; // creature died?
// set font
                g.setFont(new Font("TimesRoman", Font.PLAIN, 24)); 
// draw data
		if (id > 0){
                        int red=60+org.energy;
                        if (red > 255) red=255;
                        if (!org.tag){
                          g.setColor(new Color(red,0,0));
                        } else { // set color and possibly flash
                          if (org.tagBlink &&
                              (((org.ID+timeStep)/flashPeriod)%3 == 0)){
                            g.setColor(new Color(255-org.tagRed,
                                                 255-org.tagGreen,
                                                 255-org.tagBlue));
                          } else {
                            g.setColor(new Color(org.tagRed,
                                                 org.tagGreen,
                                                 org.tagBlue));
                          }
                        } // end of color/flash code

			drawOrg(g,org.xLoc*sizeX,org.yLoc*sizeY,sizeX,sizeY,
                                org.direction,org.old); // draw this creature :)
			// optional ID
			if (showIDs){
				g.setColor(Color.BLUE);
                                g.setFont(new Font("TimesRoman", Font.PLAIN, 24)); 
				g.drawString(id+"", org.xLoc*sizeX,org.yLoc*sizeY+20);
                                g.setFont(new Font("TimesRoman", Font.PLAIN, 14)); 
				g.drawString(org.energy+"", org.xLoc*sizeX,org.yLoc*sizeY+35);
				//g.drawString("Fluffy the Destroyer", org.xLoc*sizeX,org.yLoc*sizeY+50);
			}
		} else { // energy
                        int green=60+org.energy;
                        if (green > 255) green=255;
                        g.setColor(new Color(0,green,0));
			drawEnergy(g,org.xLoc*sizeX,org.yLoc*sizeY,sizeX,sizeY,org.direction); // draw this creature :)
			if (showIDs&&showEnergyIDs){
				g.setColor(Color.BLUE);
                                g.setFont(new Font("TimesRoman", Font.PLAIN, 14)); 
				g.drawString("("+org.energy+")", org.xLoc*sizeX,org.yLoc*sizeY+10);
			}
		}
	  }
  }

/*
 * Query an organism.
 *
 * return is as follows:
 *     energy (0=dead) surroundings (in order show below)
 * surroundings are one character per cell, shows as follows:
 *  -=empty cell; W=wall; F=energy (food);
 *  N,S,W,E=creature facing given direction
 *
 *      12 13 14 15 16
 *      11  2  3  4 17
 *      10  1  *  5 18
 *       9  8  7  6 19
 *      24 23 22 21 20
*/
  String query(int id)
  {
    Organism org=orgHash.get(id);
    if (org==null){
      System.out.println("Organism " + id + " does not exist");
      return("OK");
    }
// make a status string based on neighbors
    String response=org.energy+" ";
    int x=org.xLoc;
    int y=org.yLoc;
    response=response+gridChar(x-1,y);
    response=response+gridChar(x-1,y-1);
    response=response+gridChar(x,y-1);
    response=response+gridChar(x+1,y-1);
    response=response+gridChar(x+1,y);
    response=response+gridChar(x+1,y+1);
    response=response+gridChar(x,y+1);
    response=response+gridChar(x-1,y+1);
    response=response+gridChar(x-2,y+1);
    response=response+gridChar(x-2,y);
    response=response+gridChar(x-2,y-1);
    response=response+gridChar(x-2,y-2);
    response=response+gridChar(x-1,y-2);
    response=response+gridChar(x,y-2);
    response=response+gridChar(x+1,y-2);
    response=response+gridChar(x+2,y-2);
    response=response+gridChar(x+2,y-1);
    response=response+gridChar(x+2,y);
    response=response+gridChar(x+2,y+1);
    response=response+gridChar(x+2,y+2);
    response=response+gridChar(x+1,y+2);
    response=response+gridChar(x,y+2);
    response=response+gridChar(x-1,y+2);
    response=response+gridChar(x-2,y+2);
    return(response);
  }

// return a single character indicating what's in a given cell
  char gridChar(int x, int y)
  {
    if ((x<0)||(x>=numX)||(y<0)||(y>=numY)) return('*'); // wall
    Organism org=grid[x][y];
    if (org==null) return('-'); // empty cell
    if (org.ID < 0) return('F'); // energy
    return(org.direction); // another creature
  }

  // turn an organism
  void turnLeft(int id)
  {
	  Organism org=orgHash.get(id);
	  if (org==null){
		  System.out.println("Organism " + id + " does not exist");
		  return;
	  }
	  switch(org.direction){
	    case 'W':org.direction='S';return;
	    case 'S':org.direction='E';return;
	    case 'E':org.direction='N';return;
	    case 'N':org.direction='W';return;
	  }
  }
  
  void turnRight(int id)
  {
	  Organism org=orgHash.get(id);
	  if (org==null){
		  System.out.println("Organism " + id + " does not exist");
		  return;
	  }
	  switch(org.direction){
	    case 'W':org.direction='N';return;
	    case 'S':org.direction='W';return;
	    case 'E':org.direction='S';return;
	    case 'N':org.direction='E';return;
	  }
  }
  
// move forward if you can
// find current (x,y) and next (x,y) based on direction
// if cell is out of bounds, return "OK" (no action)
// if cell is empty, adjust (x,y) and energy and return "OK"
// if cell has energy, adjust (x,y), adjust energy, remove energy organism, and return "OK"
// if cell has an organism that's not facing you, adjust (x,y), adjust energy, remove organism and return "K id"
// EDIT: Instead of killing, wound by taking some emergy. If target' energy<=0,
//       then that creature dies.
// if cell has an organism facing you and energy>=breedThreshold and pop < maxPop and square behind you exists and is free,
//         adjust energy, create new organism and return "M idparent idnew"
// else return("OK")
    String forward(int id)
    {
      Organism org=orgHash.get(id);
	  if (org==null){ // really shouldn't happen
		  System.out.println("Organism " + id + " does not exist");
		  return("OK");
	  }
      int x=org.xLoc;
      int y=org.yLoc;

      int x2=x;int y2=y; // next location
      int x0=x;int y0=y; // location behind you (for offspring)
      switch(org.direction){
        case 'N':--y2;++y0;break;
    	case 'S':++y2;--y0;break;
    	case 'W':--x2;++x0;break;
    	case 'E':++x2;--x0;break;
      }
      if ((x2<0)||(x2>=numX)||(y2<0)||(y2>=numY)) return("OK"); // out of bounds: non-event
    	
      if (grid[x2][y2]==null){ // cell is empty
        grid[x][y]=null; // remove from old spot
        org.energy--; // takes one energy unit to move
        if (org.energy > 0){ // done
          org.xLoc=x2;org.yLoc=y2;
          grid[x2][y2]=org;
          return("OK");
        }
// else we ran out of energy
    	orgHash.remove(org.ID); // remove from hash
    	return("K " + org.ID); // indicate organism has died
      }
      
// cell is not empty: is it an energy blob?
      Organism org2=grid[x2][y2];
      if (org2.ID < 0){ // energy blob
        grid[x][y]=null; // remove from old square
        grid[x2][y2]=org; // and place in new square
        org.xLoc=x2;org.yLoc=y2; // update location in organism
        org.energy+=org2.energy; // absorb the energy
        orgHash.remove(org2.ID); // remove energy blob
        return ("OK");
      }
      
// cell is occupied by another organism
      char d2=' ';
      switch(org.direction){
        case 'N':d2='S';break;
        case 'S':d2='N';break;
        case 'W':d2='E';break;
        case 'E':d2='W';break;
      }
      if (org2.direction!=d2){ // we can attack this organism
        int energyDelta=(int) (11*rand.nextDouble()); // depleat from 0-10 energy units
        if (energyDelta < org2.energy){ // just wounded
          org2.energy-=energyDelta;
          org.energy+=energyDelta;
          return ("OK");
        } else { // creature died
          grid[x][y]=null;
          grid[x2][y2]=org;
          org.xLoc=x2;org.yLoc=y2;
          org.energy+=org2.energy; // absorb org2's energy
          orgHash.remove(org2.ID); // remove org2
          return("K " + org2.ID);
        }
      }
      
// organism is facing us; shall we breed?
      if (!okToBreed(org,org2,x0,y0)) return("OK"); // no operation
      if (!breedingAllowed) return("OK"); // disallowed
      Organism org0=new Organism(x0,y0,(org.energy+org2.energy)/4); // get 1/4 energy from each parent
      org0.xLoc=x0;org0.yLoc=y0;
      org0.direction=org2.direction; // face away from closest parent
      org0.ID=nextID;
      grid[x0][y0]=org0;
      orgHash.put(nextID, org0); // save this organism in the hash
      org.energy=org.energy*3/4;
      org2.energy=org2.energy*3/4;
      String response="M " + org2.ID + " " + org0.ID;
      nextID++;
      return(response);
    }
    
    boolean okToBreed(Organism org, Organism org2, int x0, int y0)
    {
    	if ((x0 < 0) || (x0 >= numX) || (y0 < 0) || (y0 >= numY)) return(false); // no room for offspring
    	if (grid[x0][y0]!=null) return(false); // offspring's loc already occupied
    	if (org.energy < breedThreshold) return(false); // not enough energy
    	if (org2.energy < breedThreshold) return(false);
    	if (popSize() > maxPop) return(false); // too many individuals
    	return(true);
    }
    
    int popSize() // calculate # of organisms in the population (expensive but infrequent)
    {
      int count=0;
	  for (int id:orgHash.keySet()){
		  if (id > 0) ++count;
	  }
	  return(count);
    }
  
    void setOld(int id)
    {
      Organism org=orgHash.get(id);
      if (org==null) return; // really shouldn't happen
      org.old=true;
    }

    void setTag(int id, int red, int green, int blue, int blink)
    {
      Organism org=orgHash.get(id);
      if (org==null) return; // really shouldn't happen
      org.tag=true;
      org.tagRed=red;
      org.tagGreen=green;
      org.tagBlue=blue;
      org.tagBlink=(blink!=0);
    }

  // draw a single organism
  void drawOrg(Graphics g, int x, int y, int w, int h, char dir, boolean old)
  {
	  //g.setColor(Color.RED); // body color (set based on energy?)
	  switch (dir){
	    case 'N':g.fillOval(x+w/3,y+1,w/3,h-1);
	             g.setColor(old?Color.YELLOW:Color.BLACK); // eyes
	             g.fillOval(x+w/2-w/10,y+h/5,w/10,h/10);
	             g.fillOval(x+w/2,y+h/5,w/10,h/10);
	             break;
	    case 'S':g.fillOval(x+w/3,y+1,w/3,h-1);
	             g.setColor(old?Color.YELLOW:Color.BLACK);
	             g.fillOval(x+w/2-w/10,y+4*h/5,w/10,h/10);
	             g.fillOval(x+w/2,y+4*h/5,w/10,h/10);
	             break;
	    case 'W':g.fillOval(x+1,y+h/3,w-1,h/3);
	             g.setColor(old?Color.YELLOW:Color.BLACK);
	             g.fillOval(x+w/5,y+h/2-h/10,w/10,h/10);
	             g.fillOval(x+w/5,y+h/2,w/10,h/10);
	             break;
	    case 'E':g.fillOval(x+1,y+h/3,w-1,h/3);
	             g.setColor(old?Color.YELLOW:Color.BLACK);
	             g.fillOval(x+4*w/5,y+h/2-h/10,w/10,h/10);
	             g.fillOval(x+4*w/5,y+h/2,w/10,h/10);
	             break;
	  }
  }
  
  // draw an energy blob
  void drawEnergy(Graphics g, int x, int y, int w, int h, char dir)
  {
	 //g.setColor(Color.GREEN);
	 g.fillRect(x+w/3,y+w/3,w/3,h/3);
  }

  // make a new organism somewhere in the universe
  int createOrg()
  {
	  for (int attempt=0;attempt<100;attempt++){
		  int x=(int)(numX*rand.nextDouble());
		  int y=(int)(numY*rand.nextDouble());
		  if (grid[x][y]==null){ // add organism here
			  int dir=(int)(4*rand.nextDouble());
			  char d="NSWE".charAt(dir);
			  int e=100+(int)(100*rand.nextDouble());
			  Organism org=new Organism(x,y,d,e,nextID); // this constructs the organism
			  grid[x][y]=org; // save on grid
			  orgHash.put(nextID, org); // save in hash
			  return(nextID++);
		  }
	  }
// giving up...
	  return(-1);
  }
  
  void addEnergy()
  {
// add some energy to the universe
	  for (int attempt=0;attempt<100;attempt++){
		  int x=(int)(numX*rand.nextDouble());
		  int y=(int)(numY*rand.nextDouble());
		  if (grid[x][y]==null){ // add organism here
			  Organism org=new Organism(x,y,(int)(20+20*rand.nextDouble())); // special "energy" organism
			  org.ID=nextEID;
			  grid[x][y]=org;
			  orgHash.put(nextEID--, org); // save in hash
			  return;
		  }
	  }
  }
  
  void reset() // clear entire universe
  {
	  for (int x=0;x<numX;x++){
		  for (int y=0;y<numY;y++){
			  grid[x][y]=null;
		  }
	  }
	  orgHash.clear(); // remove all hash entries
          nextID=1;nextEID=-1;
	  repaint();
  }

}
