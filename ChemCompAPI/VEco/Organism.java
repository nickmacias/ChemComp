/**
 * Single organism. Properties include location, direction and energy.
 * @author nmacias
 *
 */
public class Organism {
  int xLoc,yLoc; // initial location
  char direction; // which way it's facing: n, s, e or w
  int energy; // 0=death
  int ID; // organism's unique (forever) ID
  boolean tag=false; // set true to mark this individual
  int tagRed=255,tagGreen=255,tagBlue=0; // color to tag
  boolean tagBlink=true;
  boolean old=false; // set when age exceeds some threshold
  
  Organism(int x, int y, char d, int e, int id)
  {
	  xLoc=x;
	  yLoc=y; // location in grid [0.numXY)
	  direction=d; // 'N' 'S' 'W' or 'E'
	  energy=e; // [10,20), nominally 15
	  ID=id; // unique integer ID, beginning at 1
  }
  
  // an organism with ID=0 is an energy blob...
  Organism(int x, int y, int e)
  {
	  xLoc=x;
	  yLoc=y;
	  direction=' ';
	  energy=e;
	  ID=0;
  }

  public void tag(boolean tag)
  {
    this.tag=tag;
  }

}
