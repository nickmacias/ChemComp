//
// Main class for the entire system
// uses the run() method to create the universe, then starts its main thread
// everything else is driven from the universe...
//

import java.awt.EventQueue;

public class Main {

  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        int port=1217; // default port
        try {
          if (args.length == 1){ // assume a port number
            port=Integer.parseInt(args[0]);
          }
          Core g=new Core(port);
          g.start();
        } catch (Exception e) {
          System.out.println("Unexpected error: " + e);
          System.out.println("Usage: Main [port#]");
        }
      } // end of run()
    });
  }
}
