// main class for a single creature in the VEco
// A creature consists of an ID, an EEXIST and a Genome

import com.cellmatrix.chemcomp.API.v1.*;

class Creature
{
  int ID=0; // always unique, across all time
  EEXIST e;
  Genome genome;
  int gen=1; // or 1+max gen of parents
  int creationCycle=0;
  boolean old=false; // set when creature is old
  double R1,R2,R3,R4,R5;

  boolean zeroDiam=true; // true for forcing input diameters to 0

  Creature(int id, int creationCycle,
           double R1, double R2, double R3, double R4, double R5)
  {
    this.R1=R1;this.R2=R2;this.R3=R3;this.R4=R4;this.R5=R5;
    ID=id;
    this.creationCycle=creationCycle;
    e=new EEXIST();
    e.setKarma(10);
    genome=new Genome(e.numTubes());
    genome.randomize();
    genome.load(e);

// set diam and inject GO signal
    adjustDiamAndInject(e);
  }

// method for fixing diameter and injecting GO
  public void adjustDiamAndInject(EEXIST e)
  {
    if (zeroDiam){
// set diameter of inputs to 0
      for (double x=0;x<4;x+=e.getDx()){
        e.setDiameter(R1+x,0);
        e.setDiameter(R2+x,0);
        e.setDiameter(R3+x,0);
        e.setDiameter(R4+x,0);
        e.setDiameter(R5+x,0);
      }
    }

// inject some initial chemicals
      inject(e,32,36,20);
  }

// input injection routine
  public void inject(EEXIST e,double start, double end, double level) // inject chemicals
  {
    for (double x=start;x<end;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,level);
      e.setTube(x,EEXIST.DST,level);
    }
  }

// read a region
  public double detect(double start, double end)
  {
    double sum=0;
    for (double x=start;x<end;x+=e.getDx()){
      sum=sum+e.getTube(x,EEXIST.SRC)+e.getTube(x,EEXIST.DST);
    }
    return(sum/((end-start)/e.getDx())); // average value for a single tube
  }

// step the creature's EEXIST
  public double step(int numSteps, double start, double end)
  {
    double sum=0;
    for (int i=0;i<numSteps;i++){
      try{
        e.step(1);
        sum+=detect(start,end); // add current output to running sum
      } catch (Exception e2){
        System.out.println("Odd error in creature.step: " + e2);
        e2.printStackTrace();
      }
    }
    return(sum/numSteps);
  }

}
