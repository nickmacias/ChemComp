import com.cellmatrix.chemcomp.API.v1.*;

/*
 * Genome of an organism
 * This is comprised of an array of genes, plus
 * a set of methods for manipulating the genome
 */
public class Genome {
  static int GenomeSize=10; // # of genes in the genome
  int numTubes;
  
  // TODO Use start/slope for initial generation of e.g. bias levels, but then save tube-by-tube
  
  Gene[] gene=new Gene[Genome.GenomeSize];
  
  // just construct each gene
  Genome(int numTubes)
  {
	this.numTubes=numTubes;

    for (int i=0;i<GenomeSize;i++){
      gene[i]=new Gene();
      gene[i].len=numTubes/GenomeSize;
    }
  }
  
  void loadBias(EEXIST e) // load the bias gradients only from genome
  {
	  int nextTube=0; // running index
	  
	  // zero-out the system
	  e.clearAllTubes();

	  for (int i=0;i<GenomeSize;i++){
		  Gene g=gene[i]; // next gene to load
		  double s=gene[i].initS;
		  double d=gene[i].initD;
		  double l=gene[i].init; // initial levels
		  
		  for (int j=0;j<g.len;j++){
		      e.setBias(nextTube, EEXIST.SRC, s);
		      e.setBias(nextTube, EEXIST.DST, d);

		      s+=gene[i].deltaS+gene[i].variation*Math.random();
		      d+=gene[i].deltaD+gene[i].variation*Math.random();
		      l+=gene[i].delta+gene[i].variation*Math.random();
		      if (s<0) s=0;
		      if (d<0) d=0;
		      if (s>40) s-=40;
		      if (s<0) s+=40;
		      if (d>40) d-=40;
		      if (d<0) d+=40;
		      if (l>5) l=5;
		      if (l<0) l=0;

		      ++nextTube;
		  } // end of gene
	  } // end of genome
  }
  
  void load(EEXIST e) // load the genome into the machine
  {
	  int nextTube=0; // running index
	  
	  // zero-out the system
	  e.clearAllTubes();

	  for (int i=0;i<GenomeSize;i++){
		  Gene g=gene[i]; // next gene to load
		  double s=gene[i].initS;
		  double d=gene[i].initD;
		  double l=gene[i].init; // initial levels
		  
		  for (int j=0;j<g.len;j++){
	              e.setBias(nextTube, EEXIST.SRC , 0);
		      e.setBias(nextTube, EEXIST.DST , 0);
		      e.setTube((int)nextTube, EEXIST.SRC, 0);
		      e.setTube((int)nextTube, EEXIST.DST, 0);
		      e.setDiameter(nextTube,1.);

/***
		      if (gene[i].type==Gene.CHEM){ // set SRC/DST chemical levels
		        e.setTube(nextTube, EEXIST.SRC, s);
		        e.setTube(nextTube, EEXIST.DST, d);
		        //System.out.println("Setting tube " + nextTube + " s=" + s + " d=" + d);
		      }
		      if (gene[i].type==Gene.DIAM){ // set diameter
		        e.setDiameter(nextTube,l/20.);
		        //System.out.println("Diam[" + nextTube + "]=" + l);
		        //TODO diameters are way too big...
		      }
***/
		      //if (gene[i].type==Gene.BIAS){ // set bias level
		        e.setBias(nextTube, EEXIST.SRC, s);
		        e.setBias(nextTube, EEXIST.DST, d);
		        //System.out.println("Setting Bias " + nextTube + " s=" + s + " d=" + d);
		      //}
		      // end of loading this tube

		      s+=gene[i].deltaS+gene[i].variation*Math.random();
		      d+=gene[i].deltaD+gene[i].variation*Math.random();
		      l+=gene[i].delta+gene[i].variation*Math.random();
		      if (s<0) s=0;
		      if (d<0) d=0;
		      /***/
		      if (s>40) s-=40;
		      if (s<0) s+=40;
		      if (d>40) d-=40;
		      if (d<0) d+=40;
		      if (l>5) l=5;
		      if (l<0) l=0;
		      /***/
		      ++nextTube;
		  } // end of gene
	  } // end of genome
  }
  
  Genome copy() // make a copy
  {
	  Genome temp=new Genome(numTubes);
	  for (int i=0;i<GenomeSize;i++){
		  temp.gene[i]=gene[i].copy();
	  }
	  return(temp);
  }
  
  void mutate(double amount)
  {
	  for (int i=0;i<GenomeSize;i++){
		  if (Math.random() < amount) gene[i].delta*=(.5+Math.random()); // .x5-1.5x
		  if (Math.random() < amount) gene[i].deltaD*=(.5+Math.random()); // .x5-1.5x
		  if (Math.random() < amount) gene[i].deltaS*=(.5+Math.random()); // .x5-1.5x
		  if (Math.random() < amount) gene[i].init*=(.5+Math.random()); // .x5-1.5x
		  if (Math.random() < amount) gene[i].initD*=(.5+Math.random()); // .x5-1.5x
		  if (Math.random() < amount) gene[i].initS*=(.5+Math.random()); // .x5-1.5x
		  if (Math.random() < amount) gene[i].variation*=(.5+Math.random());
		  if (Math.random() < amount/5) gene[i].type=(int)(3.*Math.random()); // less likely!
	  }
  }
   
  void merge(Genome g)
  {
	  for (int i=0;i<GenomeSize;i++){ // select one parent
		  // TODO may want to actually merge (average) the 2 genes
		  gene[i].delta=(gene[i].delta+g.gene[i].delta)/2.;
		  gene[i].deltaD=(gene[i].deltaD+g.gene[i].deltaD)/2.;
		  gene[i].deltaS=(gene[i].deltaS+g.gene[i].deltaS)/2.;
		  gene[i].init=(gene[i].init+g.gene[i].init)/2.;
		  gene[i].initD=(gene[i].initD+g.gene[i].initD)/2.;
		  gene[i].initS=(gene[i].initS+g.gene[i].initS)/2.;
		  gene[i].variation=(gene[i].variation+g.gene[i].variation)/2.;
		  /***
		  if (Math.random() < .5){ // replace this gene
			  gene[i]=g.gene[i].copy();
		  }
		  ***/
		  
	  }
  }

  void randomize()
  {
    for (int i=0;i<GenomeSize;i++){
      gene[i].randomize();
    }
  }
}
