import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.border.BevelBorder;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class VEco extends JFrame {

	private JPanel contentPane;
	MyPanel panel;
	private JLabel lblPort;
	private JTextField portTextField;
	private JButton btnStart;
	Server server; // main communications server
	private JCheckBox chckbxShowIds;
	private JScrollPane scrollPane;
	JCheckBox showDailyCycle;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VEco frame = new VEco();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VEco() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 746, 713);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[][][grow]"));
		
		scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		contentPane.add(scrollPane, "cell 0 2,grow");
		
		panel = new MyPanel();
		scrollPane.setViewportView(panel);
		panel.saveParent(this);
		server = new Server(this);
		
		panel.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				panel.setPanelSize();
			}
		});
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			  try{
			    server.start();
                            btnStart.setEnabled(false);
			  } catch(IllegalThreadStateException e1){
				System.out.println("Server is already running");
			  }
			}
		});
		contentPane.add(btnStart, "flowx,cell 0 0");
		
		JCheckBox chckbxGrid = new JCheckBox("Grid");
		chckbxGrid.setSelected(true);
		chckbxGrid.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
			  panel.showGrid=chckbxGrid.isSelected();
			  panel.repaint();
			}
		});
		contentPane.add(chckbxGrid, "cell 0 0");
		
		chckbxShowIds = new JCheckBox("Show Data");
		chckbxShowIds.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				panel.showIDs=chckbxShowIds.isSelected();
				panel.repaint();
			}
		});
		contentPane.add(chckbxShowIds, "cell 0 0");
		
		lblPort = new JLabel("        Port:");
		contentPane.add(lblPort, "cell 0 0");
		
		portTextField = new JTextField();
		portTextField.setText("1217");
		contentPane.add(portTextField, "cell 0 0");
		portTextField.setColumns(10);
		
		showDailyCycle = new JCheckBox("Show Daily Cycle");
		contentPane.add(showDailyCycle, "cell 0 1");
	}

	public int getPort()
	{
	  try{
	    return(Integer.parseInt(portTextField.getText()));
	  } catch(Exception e){
		System.out.println("Error parsing input: using port 1217");
		portTextField.setText("1217");
		return(1217);
	  }
	}
}
