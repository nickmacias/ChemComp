import com.cellmatrix.chemcomp.API.v1.*;
import java.io.PrintWriter;
import java.util.Scanner;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.swing.JFileChooser;
import java.io.File; 

public class Core extends Thread{

  Socket server;
  PrintWriter toServer;
  Scanner fromServer;

  Object qLock=new Object(); // for locking queue :)

  boolean HL=false; // true for headless, false for normal display
  boolean DEBUG=false;
  int delayTime=0; // mSec per update

  boolean paused=false; // set to pause the run

  double mutateRate=.10; // chance of mutation during breeding
  boolean breedingAllowed=true; // can be disabled with "breed" command
                              // only affects periodic breeding;
                             // breeding when 2 creatures meet is still allowed

  int cycleCount=0;

  int initialPopSize=50;
  int initialEnergySize=20;
  int popBreedLimit=100;

  int stepSize=10; // how many steps per creature update

  String workingFilename=null; // file from which we can read genomes
  Scanner fileScanner; // for reading genomic data

  int defaultIntensity=100; // for all newly-created displays
  //int oldThreshold=1000000; // # of cycles before creature is considered "old"
  int oldThreshold=400000; // # of cycles before creature is considered "old"

// locations of inputs and outputs
  double R1=0;   // cell to the West
  double R3=8;   // cell to North
  double R5=16;  // cell to East
  double R2=4;   // 5x5 area sweep (unspecialized)
  double R4=12;  // cycling daylight
// inputs 1, 3 and 5:
//    empty=5
//    creature facing you=25
//    anything else=15

  double lrf=24; // EEXIST action request
//  5-10=LEFT
// 10-15=RIGHT
//   >15=FORWARD

  PrintWriter pw; // for writing raw output

  LinkedList<Creature> queue; // for saving and retrieving creatures
  LinkedList<Integer> oldQueue; // IDs of older creatures
  ControlPanel cp;

  String filenameMiddle; // output to e.g. raw.$filenameMiddle.txt

  int portNum=0;

// adjust karma of all individuals
  void adjustKarma(double k)
  {
    synchronized(qLock){
      for (int i=0;i<queue.size();i++){
        queue.get(i).e.setKarma(k);
      }
// log this change
      writeStatus(queue.get(0).e,pw);
      msg("Karma set to " + queue.get(0).e.getKarma());
    }
  }
      

  public Core(int portNum)
  {
    this.portNum=portNum;
  }

  public void run()
  {
// make datestamp
    filenameMiddle=new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    filenameMiddle=findNextFilename();

// prepare for raw output
    try{
      pw=new PrintWriter("raw."+filenameMiddle+".txt");
    } catch(Exception e){
      msg("Can't write to output file ("+e+") - goodbye");
      return;
    }

// make a control panel
    try{
      cp=new ControlPanel("Control Panel");
      cp.connect(this,ControlPanel.Slider,0,100,0,400,1,"Karma","cpHandler");
      //cp.connect(this,ControlPanel.Slider,1,128,0,255,2,"(Intensity)","cpHandler");
      cp.connect(this,ControlPanel.Slider,2,0,0,1000,3,"Delay","cpHandler");
      cp.connect(this,ControlPanel.Slider,3,10,1,25,4,"Step Size","cpHandler");
      cp.connect(this,ControlPanel.Slider,1,50,1,200,5,"Initial Pop Size","cpHandler");
      cp.connect(this,ControlPanel.CheckBox,0,10,"Pause","cpHandler");
      cp.connect(this,ControlPanel.TextIn,0,11,"Command:","cpHandler");
      cp.connect(this,ControlPanel.Button,0,211,"QUIT","cpHandler");
      cp.connect(this,ControlPanel.Button,1,21,"Reset","cpHandler");
      cp.setText(true); // output to here as well as stdout
    } catch (Exception e){
// assume this is simply running without an X server, and isn't a real error
//System.out.println(e);e.printStackTrace();
      System.out.println("Running headless");
    }

// connect to server
// try 5 ports

    for (int tryNum=0;tryNum<5;tryNum++){
      try{
        server=new Socket("localhost",portNum+tryNum);
        toServer=new PrintWriter(server.getOutputStream());
        fromServer=new Scanner(server.getInputStream());
        msg("Connected to server on port " + (portNum+tryNum));
        break; // success!
      } catch (Exception ee){
        msg("Can't connect to port " + (portNum+tryNum) +
                           " on server: " + ee);
        if (tryNum==4){
          msg("No server available: giving up");
          System.exit(0);
          //return;
        }
      }
    }

    EEXIST tempE=new EEXIST();tempE.setKarma(10);writeStatus(tempE,pw); // just for reference
    tempE=null; // garbage collector will recycle old EEXIST

// reset server
    command("R"); // send command, read response

// create initial population
    queue=new LinkedList<Creature>();
    oldQueue=new LinkedList<Integer>();

    for (int i=0;i<initialPopSize;i++){
      Creature c=addCreature();
// this also creates the EEXIST and Genome, etc.
      if (c!=null){
        writeIndiv(cycleCount,c.ID,c.genome,c.gen,0,pw); // save genome
        queue.add(c);
      }
//Display disp=new Display(c.e);
//disp.setVisible(true);
    }

// add some food
    for (int i=0;i<initialEnergySize;i++){
      command("E"); // add some food to the universe
    }

// begin main loop here...
    while (true){
      if (paused){
        myDelay(100);
        continue;
      }
// only do all this if the Pause checkbox is clear
      myDelay(delayTime);
      Creature cr;
      synchronized(qLock){
        cr=queue.removeFirst();
        if (cr==null) continue;
        queue.add(cr); // re-insert at the tail
        debug("Processing creature " + cr.ID);
      }

// set age if old enough
      if (!cr.old){
        if (cycleCount-cr.creationCycle >= oldThreshold){
          cr.old=true;
          oldQueue.add(cr.ID);
          command(cr.ID + " O"); // tell panel this creature is old
          debug("Creature " + cr.ID + " is now old. cycleCount=" +
                 cycleCount + " Creature created at cycle " +
                 cr.creationCycle);
        }
      }

// see what's in the neighborhood
      String s=command(cr.ID + " Q");
      if (s.equals("OK")){ // creature does not exist
        continue;
      }
      String[] sp=s.split(" "); // sp[1] is the string of neighbors
      debug("Neighborhood=" + sp[1]);
      message(cycleCount+",I,"+cr.ID+","+Integer.parseInt(sp[0])); // record the event

      double v1,v2,v3,v4,v5; // values to load into EEXIST

      char c=sp[1].charAt(0); // neighbor to West
      if (c=='-') v1=5; // empty
      else if (c=='E') v1=25; // creature facing you
      else v1=15; // other

      cr.inject(cr.e,R1,R1+4,v1);

      //c=sp[1].charAt(1); // neighbor to NW
      //if (c=='-') v2=5;
      //else v2=15;

      c=sp[1].charAt(2); // neighbor to North
      if (c=='-') v3=5;
      else if (c=='S') v3=25;
      else v3=15;

      cr.inject(cr.e,R3,R3+4,v3);

      //c=sp[1].charAt(3); // neighbor to NE
      //if (c=='-') v4=5;
      //else v4=15;

      c=sp[1].charAt(4); // neighbor to East
      if (c=='-') v5=5;
      else if (c=='W') v5=25;
      else v5=15;

      cr.inject(cr.e,R5,R5+4,v5);

// Region 2 is set if there is anything in the vicinity
      v2=0;
      for (int i=0;i<24;i++){
        if (sp[1].charAt(i) != '-') ++v2;
      }
      cr.inject(cr.e,R2,R2+4,v2);
      
// Let's add a cyclic component to the universe (daylight?)
// base this on cycleCount, run it from 25->0->25
      v4=Math.abs(((cycleCount/50)%51)-25);
      cr.inject(cr.e,R4,R4+4,v4);
      debug("R4=" + v4);

// set background on display
       if (cycleCount%50==0){ // dylight value has changed
         command("D " + (int) v4);
       }

// step the system 10 times
// and check for a command from the creature
      double response=cr.step(stepSize,lrf,lrf+4);

      //double response=cr.detect(lrf,lrf+4); // read average tube value
      debug("Response from " + cr.ID + " =" + response);
      if ((response >= 5) && (response < 10)){ // left
        command(cr.ID + " L");
      } else if ((response >= 10) && (response < 15)){ // right
        command(cr.ID + " R");
      }
// L an R are followed by F
// but if response>=15 just go forward
      if (response >= 5){ // move forward
        s=command(cr.ID + " F");
        sp=s.split(" ");
        if (sp[0].equals("K")){ // a creature died
          kill(Integer.parseInt(sp[1])); // remove this creature from the queue
        } else if (sp[0].equals("M")){ // mating
          int parentID=Integer.parseInt(sp[1]);
          int newID=Integer.parseInt(sp[2]);
// create a new creature with the given id
          Creature newC=new Creature(newID,cycleCount,R1,R2,R3,R4,R5);
          debug("New creature: " + newC.ID);

// this creature has a random genome. Give it a mix of its parents' genomes
// cr is one parent; the other has id "parentID" Let's find that creature
          Creature parent=findCreature(parentID);
          if (parent != null){
            breed(cr,parent,newC,mutateRate);

// save this creature's genome
            writeIndiv(cycleCount,newC.ID,newC.genome,newC.gen,0,pw);

            synchronized(qLock){
              queue.addFirst(newC); // add to queue (at beginning, so it gets processed next)
            }
          } // else parent vanished...
        } // else nothing special happened, creature just moved
      } // end of F command
      ++cycleCount;
      if (cycleCount%1000==0) msg("<cycleCount>");
      if (cycleCount%100000==1) writePop(pw); // save population info
      if ((cycleCount)%100==0) command("E"); // add food periodically
      if (breedingAllowed && (cycleCount)%250==0){ // add a creature
        if (queue.size() > popBreedLimit) continue; // pop limit for periodic breeding
        Creature newCreature=addCreature();
        if (newCreature != null){
// let's set its genome from two random parents
// but require these parents to be old...
  
          synchronized(qLock){
            if ((oldQueue.size() < 2) || (Math.random() < .1)){ // pick from entire population
              int p1=(int)(Math.random()*queue.size());
              int p2=(int)(Math.random()*queue.size());
              breed(queue.get(p1),queue.get(p2),newCreature,mutateRate);
              debug("New breeding: creatures " + queue.get(p1).ID + " and "
                     + queue.get(p2).ID);
            } else { // pick old parents
              int p1=(int)(Math.random()*oldQueue.size());
              int p2=(int)(Math.random()*oldQueue.size());
  // these are indexes in the oldQueue
              p1=oldQueue.get(p1).intValue();
              p2=oldQueue.get(p2).intValue();
  // these are creature IDs
              breed(findCreature(p1),findCreature(p2),newCreature,mutateRate);
              debug("Old breeding: creatures " + findCreature(p1).ID + " and "
                     + findCreature(p2).ID);
            }
          }

// set diameter to 0 and inject GO signal
          newCreature.adjustDiamAndInject(newCreature.e);
// save this creature's genome
          writeIndiv(cycleCount,newCreature.ID,
                     newCreature.genome,newCreature.gen,0,pw);
// and add to queue
          synchronized(qLock){
            queue.add(newCreature);
          }
        }
      }
    } // main loop ends here
  }

// create a creature and add to the universe
  Creature addCreature()
  {
    String s=command("C"); // create a new organism
    if (s.equals("OK")) return(null); // no space left; do nothing.
    int ID=Integer.parseInt(s); // new creature's ID
    Creature c=new Creature(ID,cycleCount,R1,R2,R3,R4,R5); // create the creature
    return(c);
  }

  Creature findCreature(int ID)
  {
    synchronized(qLock){
      for (int i=0;i<queue.size();i++){
        Creature c=queue.get(i);
        if (c.ID==ID) return(c);
      }
    }
// ???
    return(null);
  }

  void kill(int id) // remove this creature from queue
  {
    debug("Creature " + id + " got killed");
    writeKill(id,pw); // record in raw.txt
    msg("Creature " + id + " is dead");
    synchronized(qLock){
      for (int i=0;i<queue.size();i++){
        if (queue.get(i).ID==id){
          queue.remove(i);

// remove from oldQueue if found
          for (int j=0;j<oldQueue.size();j++){
            if (oldQueue.get(j).intValue()==id){ // remove from oldQueue
              oldQueue.remove(j);
              break;
            }
          }
          return;
        }
      }
    } // should never get here
    msg("But (weird) creature " + id + " not in queue ???");
  }

// server communication
// synchronized!
  synchronized String command(String cmd)
  {
    debug("Sending command <" + cmd + ">");
    toServer.println(cmd);toServer.flush();
    return(fromServer.nextLine());
  }

  void breed(Creature par1, Creature par2, Creature child, double mutateRate)
  {
    child.genome=par1.genome.copy(); // clone first parent
    child.genome.merge(par2.genome); // breed with second parent
    child.genome.mutate(mutateRate); // random spike
    child.gen=1+((par1.gen>par2.gen)?par1.gen:par2.gen);
    debug("Gen " + child.gen + " creature created, ID=" + child.ID);
    //Display disp=new Display(child.e);
    //disp.setTitle("Gen " + child.gen + " ID=" + child.ID);
    //disp.setVisible(true);

// the genome itself is set now...but we need to load this genome into
// the creature's EEXIST
    child.genome.loadBias(child.e);

    message(cycleCount + ",B," + par1.ID + "," + par2.ID + "," + child.ID); // record event
  }

  void writeKill(int id, PrintWriter pw)
  {
    pw.println("K,"+cycleCount+","+id);
    pw.flush();
  }

  void writeStepSize(PrintWriter pw)
  {
    pw.println("T," + cycleCount + "," + stepSize);
    pw.flush();
  }

  void writePop(PrintWriter pw)
  {
    pw.print("P,"+cycleCount);
    for (int i=0;i<queue.size();i++){
      synchronized(qLock){
        Creature c=queue.get(i);
        pw.print(","+c.ID+","+(cycleCount-c.creationCycle));
      }
    }
    pw.println();
    pw.flush();
  }

  boolean firstWS=true;
  void writeStatus(EEXIST e,PrintWriter pw)
  {
    if (firstWS){
// show style of all status messages
      pw.println("**,cycleCount,informational message");
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      pw.println("*K,cycleCount,id");
      pw.println("*P,cycleCount,id1,id2,...,id_n");
      pw.println("*T,cycleCount,stepSize");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+
              e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
              ","+e.numTubes());
    pw.flush();
  }

// generic info msg
  void writeMisc(String text, PrintWriter pw)
  {
    pw.println("** " + cycleCount + "," + text);
    pw.flush();
  }

  boolean firstWI=true;
  void writeIndiv(int cycle,int indiv,Genome genome,
                  int gen,double score2,PrintWriter pw)
  {
    debug("Creature " + indiv + " sBias(0)=" + genome.gene[0].initS);
    if (firstWI){
      pw.println("*I,cycle,ID,gen,rsvd,len[i],var[i],init[i],initS[i],"+
                 "initD[i],delta[i],deltaS[i],deltaD[i]");
      firstWI=false;
    }
    pw.print("I,"+cycle+","+indiv+","+gen+","+score2);
    for (int i=0;i<Genome.GenomeSize;i++){
/***
      pw.print(","+genome.gene[i].len+","+genome.gene[i].variation+","+
               genome.gene[i].init+","+genome.gene[i].initS+","+genome.gene[i].initD+","+
               genome.gene[i].delta+","+genome.gene[i].deltaS+","+genome.gene[i].deltaD);
***/
      pw.printf(",%d,%8f,%8f,%8f,%8f,%8f,%8f,%8f",
                 genome.gene[i].len,
                 genome.gene[i].variation,
                 genome.gene[i].init,
                 genome.gene[i].initS,
                 genome.gene[i].initD,
                 genome.gene[i].delta,
                 genome.gene[i].deltaS,
                 genome.gene[i].deltaD);
    }
    pw.println();pw.flush();
  }

// delay (milliseconds)
  public void myDelay(int t)
  {
    try{Thread.sleep(t);} catch(Exception e){}
    return;
  }

  void debug(String str)
  {
    if (DEBUG) msg(str);
  }

// write to out.txt file
  PrintWriter messagePW=null;
  public void message(String s)
  {
    if (messagePW==null){
      try{
        messagePW=new PrintWriter("out."+filenameMiddle+".txt");
      } catch(Exception e){
       message("Error opening log file out."+filenameMiddle+".txt");
       System.exit(0);
      }
    }
    messagePW.println(s);
    //System.out.println(s);
  }

// write to stdout and to textOut JTextArea
  public void msg(String s)
  {
    cp.addText(cycleCount+": "+s+"\n");
    System.out.println(cycleCount+": "+s);
  }

// handle user requests...
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      adjustKarma(((double)arg.getSliderPos())/10);
      break;
    case 2: // intensity
      defaultIntensity=arg.getSliderPos();
      break;
    case 3: // runtime delay
      delayTime=arg.getSliderPos();
      break;
    case 4: // step size
      stepSize=arg.getSliderPos();
      writeStepSize(pw);
      break;
    case 5: // initial population size
      initialPopSize=arg.getSliderPos();
      writeMisc("User-specified population size="+initialPopSize,pw);
      break;
    case 10: // Run/Pause
      paused=arg.getCheckState();
      break;
    case 11: // text input
      //System.out.println("<"+arg.getTextIn()+">");
      parse(arg.getTextIn());
      break;
    case 211: // quit
      System.exit(0);
    case 21: // system reset
      doFullReset();
      break;
    }
  }

// system reset
  public void doFullReset()
  {
    writeMisc("User reset requested",pw);
// reset server
    command("R"); // send command, read response

// create (new) population
    synchronized(qLock){
      queue.clear();
      oldQueue.clear();

      for (int i=0;i<initialPopSize;i++){
        Creature c=addCreature();
// this also creates the EEXIST and Genome, etc.
        if (c!=null){
          writeIndiv(cycleCount,c.ID,c.genome,c.gen,0,pw); // save genome
          queue.add(c);
        }
      }
    }
  }

/*
 * Process user commands
 *
 * t id - toggle individual's tag
 * t id red green blue blink
 * e id - open EEXIST window for individual
 * e2 id - open GraphFrame display for an individual
 * d - inject a drone: control with f l r, exit with q
 * f filename - specify a filename from which to read genomes for cloning
 *  c src dst - clone from ID=src (in filename) to ID=dst (in population)
 * pause - pause the sim. Doesn't change Control Panel checkbox.
 * breed on/off
 * run - unpause the sim. Doesn't change Control Panel checkbox.
 * @filename - run a set of commands from filename
 * reset - reset the simulation and the VEco
 * # - begin a comment
 * ? help
 * Q - quit immediately
 *
 */

  boolean drone=false;
  int droneID=0;

  void parse(String string)
  {
    msg("> "+string); // echo

    if (string.length()==0) return; // just an empty line
    String[] s=string.split(" ");

// check for a commandfile request
    if (string.charAt(0)=='@'){ // rest of line is a filename
      Scanner cmdFile;
      try{
        cmdFile=new Scanner(new File(string.substring(1)));
      } catch (Exception e){
        msg("Error executing commandfile " + string.substring(1));
        return;
      }
// read each line and parse it...
      while (cmdFile.hasNextLine()){
        parse(cmdFile.nextLine());
      }
      return;
    }

    if (drone){
      if (s[0].equals("q")){ // exit from drone control
        drone=false;
        cp.textInInstant(false); // go back to normal input mode
        return;
      }
      String response;
// process movement commnds
      if (s[0].equals("f")){response=command(droneID + " F");}
      else if (s[0].equals("l")){response=command(droneID + " L");}
      else if (s[0].equals("r")){response=command(droneID + " R");}
      else {
        msg("Use f l r to move, q to return to normal command mode");
        return;
      }

// mating with the drone makes a non-viable creature, since we have no genome
// but killing an individual really kills them, so handle that case
      String[] resp=response.split(" ");
      if (resp[0].equals("K")){ // someone died
        kill(Integer.parseInt(resp[1]));
      }
      return;
    } // end of drone mode

    if (string.charAt(0)=='#'){
      //msg(string); // log this
      return; // comment
    }

    if (s[0].equals("d")){ // inject a drone...
      String dID=command("C"); // create a new creature
      if (dID.equals("OK")){ // no space
        msg("Universe is too full");
        return;
      }
      drone=true;
      droneID=Integer.parseInt(dID);
      msg("Drone created, ID=" + dID);
      command(dID + " T 255 0 0 1"); // tag the drone
      cp.textInInstant(true); // single-keystroke mode
      return;
    }

// clone an individual
    if (s[0].equals("c")){ // check usage
      if (s.length != 3){ // error
        msg("Expecting c src dst");
        return;
      }
      if (workingFilename==null){
        msg("Please enter a filename first with the \"f\" command");
        return;
      }

// prepare to parse
      int src,dst; // clone from src->dst
      try{
        src=Integer.parseInt(s[1]);
        dst=Integer.parseInt(s[2]);
      }catch(Exception e){
        msg("Parse error: c " + s[1] + " " + s[2]);
        return;
      }
// open the file and scan
      try{
        fileScanner=new Scanner(new File(workingFilename));
        debug("Searching " + workingFilename);
        Creature cr=findCreature(dst);
        if (cr==null){
          msg("ID " + dst + " not in queue...");
          return;
        }
// ready to clone...
        writeMisc("User-requested clone from " + s[1] + "==>" + s[2],pw);

        EEXIST e=cr.e; // target EEXIST
        Genome thisInd=load(fileScanner,src,e); // scan file and setup this individual
        e.clearAllTubes();
        thisInd.loadBias(e); // load into target's EEXIST :)
// note that the karma etc. will be setup as the file is scanned
// This also resets diameter to 1...so we need to re-adjust this
        cr.adjustDiamAndInject(e);

        fileScanner.close();
        //e.localPaint();
      }catch(Exception e){
        msg("Error with load:" + e);
        e.printStackTrace();
        return;
      }
      return;
    }

// specify file for future cloning
    if (s[0].equals("f")){
      if (s.length>2){
        msg("expecting f or f filename");
        return;
      }
      if (s.length==2){
        workingFilename=s[1];
      } else {
// use file select dialog
        JFileChooser fc=new JFileChooser();
        fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
        int result=fc.showOpenDialog(cp);
        if (result==JFileChooser.APPROVE_OPTION){
            workingFilename=fc.getSelectedFile().getAbsolutePath();
        }
      }
// see if this file is readable
      try{
        fileScanner=new Scanner(new File(workingFilename));
        debug(workingFilename + " opened succesfully for reading");
        fileScanner.close();
      }catch(Exception e){
        msg("ERROR: Can't open " + workingFilename);
        workingFilename=null;
      }
      return;
    }

    if (s[0].equals("Q")) System.exit(0);

    if (s[0].equals("t")){ // tag an individual
      int id;
      if (s.length==2){ // just tag red/flashing
        id=myIParse(s[1]); // returns -999 if illegal :-P
        if (id != -999) command(id + " T 255 0 0 1");
        return;
      } else if (s.length==6){
        command(s[1] +" T " + s[2]+" "+s[3]+" "+s[4]+" "+s[5]);
      } else msg("Error in tag command");
      return;
    }

// run/pause
    if (s[0].equals("run")){
      paused=false;return;
    }

    if (s[0].equals("pause")){
      paused=true;return;
    }

// display EEXIST window on an individual
    if (s[0].equals("e")){ // open display
      int id=myIParse(s[1]);
      if (id==-999) return;
      Creature c=findCreature(id);
      if (c==null) return; // weird
// make a display
      Display disp=new Display(c.e);
      disp.exitOnClose(false);
      disp.setVisible(true); // don't show main display
      disp.setTitle("ID=" + c.ID); // which really should be id!
      disp.setIntensity(defaultIntensity);
      return;
    }

// breeding on/off
    if (s[0].equals("breed")){
      if (s.length!=2){
        msg("Usage: breed on/off");
      } else if (s[1].equals("on"))breedingAllowed=true;
      else if (s[1].equals("off")) breedingAllowed=false;
      else msg("Usage: breed on/off");
      command("B " + ((breedingAllowed)?"1":"0")); // tell server
      return;
    }

// reset
    if (s[0].equals("reset")){
      doFullReset();
      return;
    }

// display graph frame window only
    if (s[0].equals("e2")){
      int id=myIParse(s[1]);
      if (id==-999) return;
      Creature c=findCreature(id);
      if (c==null) return; // weird
// make a display
      Display disp=new Display(c.e);
      disp.exitOnClose(false);
      disp.showGF(true);
      disp.setGFTitle("ID="+c.ID);
      return;
    }

// help
    if (s[0].equals("?")){
      msg("t id - set individual's (default) tag");
      msg("t id ref green blue blink - toggle individual's tag");
      msg("e id - open EEXIST window for individual");
      msg("e2 id - open GraphFrame display for an individual");
      msg("d - inject a drone: control with f l r, exit with q");
      msg("f filename - specify a filename from which to read genomes for cloning");
      msg("c src dst - clone from ID=src (in filename) to ID=dst (in population)");
      msg("breed on/off - enable/disable breeding");
      msg("pause - pause the sim. Doesn't change Control Panel checkbox.");
      msg("run - unpause the sim. Doesn't change Control Panel checkbox.");
      msg("@filename - run a set of commands from filename");
      msg("reset - reset the simulation and the VEco");
      msg("# - begin a comment");
      msg("? this help message");
      msg("Q - quit immediately");

      return;
    }

// unknown command
    msg("Unknown command: " + string);
    msg("Enter ? for help");
  }

  int myIParse(String s)
  {
    try {
      return(Integer.parseInt(s));
    } catch(Exception e){
      return(-999);
    }
  }

// return a genome from a file
  Genome load(Scanner sc, int tgtId, EEXIST e)
  {
    Genome ret=new Genome(e.numTubes());
    while (sc.hasNextLine()){
      String temp=sc.nextLine();
// comment
      if (temp.charAt(0)=='*') continue; // comment
// setup
      if (temp.charAt(0)=='S'){ // setup EEXIST
        String[] toks=temp.split(",");
        double karma=Double.parseDouble(toks[1]);
        e.setKarma(karma); // adjust this as we scan the file
        continue;
      }
// individual
      if (temp.charAt(0)=='I'){
        String[] toks=temp.split(",");
        int cycle=Integer.parseInt(toks[1]);
        int id=Integer.parseInt(toks[2]);
        double gen=Double.parseDouble(toks[3]);
        double rsvd=Double.parseDouble(toks[4]);
        if (id != tgtId) continue;
// found the individual here
        for (int i=0;i<Genome.GenomeSize;i++){
          ret.gene[i].len=Integer.parseInt(toks[5+i*8]);
          ret.gene[i].variation=Double.parseDouble(toks[6+i*8]);
          ret.gene[i].init=Double.parseDouble(toks[7+i*8]);
          ret.gene[i].initS=Double.parseDouble(toks[8+i*8]);
          ret.gene[i].initD=Double.parseDouble(toks[9+i*8]);
          ret.gene[i].delta=Double.parseDouble(toks[10+i*8]);
          ret.gene[i].deltaS=Double.parseDouble(toks[11+i*8]);
          ret.gene[i].deltaD=Double.parseDouble(toks[12+i*8]);
        }
        msg("id=" + id + " cycle=" + cycle + " gen=" + gen);
        sc.close(); // done with file for now
        return(ret); // return the genome
      }
// keep reading
    } // EOF
    sc.close();
    msg("WARNING: Didn't find id " + tgtId);
    return(null);
  }

// search current directory for files named "raw.(num).txt"
// find largest such num and return next consecutive integer
  String findNextFilename()
  {
    int maxInd=0; // largest file number found so far
    File f=new File("."); // search current directory
    String[] s=f.list(); // list of files
    for (int i=0;i<s.length;i++){
      String[] pieces=s[i].split("\\."); // raw.number.txt
      if (pieces.length != 3) continue; // doesn't match this pattern
      if (!pieces[0].equals("raw")) continue;
      if (!pieces[2].equals("txt")) continue;
      try{ // got first and last patterns; is the middle an integer?
        int num=Integer.parseInt(pieces[1]); // yes!
        if (num > maxInd) maxInd=num; // compare to running max
      } catch(Exception e){
        continue;
      }
    }
    return(""+(1+maxInd)); // use this next
  }

}
