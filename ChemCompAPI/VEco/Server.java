import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server extends Thread{
	  VEco parent;
	  MyPanel panel; // main universe

/* Main Server Interface
 * 
 * B 0 or 1 - disallow or allow breeding
 * R - reset entire universe
 * Q - Quit (shutdown server and exit)
 * E - Deposit energy somewhere in the universe
 *
 * C - create a new organism
 *     return: organism's ID (sequential integers)
 *
 * D num - tell VEco current daylight setting (0-25 for now).
 *         Can be used to set the background intensity
 *
 * id T RED GREEN BLUE BLINK - set this individual's tag
 *      RGB are integers
 *      BLINK is 1 (blink) or 0 (steady)
 *
 * id O - set this individual's OLD tag
 *     
 * id L or R - turn
 * 
 * id F - move forward if possible
 *        returns:
 *            "OK" if nothing special happens
 *            K id of killed organism if you eat someone
 *            M idparent idnew if mating occurs
 *              If facing another organism that is also facing you, and
 *              energy is at least breedThreshold, and you
 *              try to move onto the other organism,
 *              and the space behind you is clear,
 *              then a new organism emerges (and no movement occurs).
 *              The return is the id of the other parent, followed by the id of the offspring
 * 
 * id Q - query individual
 *     return: energy (0=dead) surroundings (in order show below):
 *        -=empty cell; W=wall; F=energy (food); N,S,W,E=creature facing given direction
 *     direction matrix order:
 *      12 13 14 15 16
 *      11  2  3  4 17
 *      10  1  *  5 18
 *       9  8  7  6 19
 *      24 23 22 21 20
 *      
 */

  String process(String buffer)
  {
	  int i;

	  if (buffer.length()==0) return("OK"); // NOP
	  
	  String[] pieces=buffer.split(" ");

	  switch(buffer.charAt(0)){
	    case 'R': // universe reset
	    	panel.reset();
	    	return("OK");

            case 'B': // control breeding
              panel.breedingAllowed=(buffer.equals("B 1"));
              return("OK");

	    case 'E': // create an energy blob
	      panel.addEnergy();
	      panel.repaint();
	      return("OK");

            case 'D': // daylight value
              int intensity=Integer.parseInt(pieces[1]);
              panel.setIntensity(intensity);
              panel.repaint();
              return("OK");

	    case 'C': // create an organism
		  if (-1 == (i=panel.createOrg())){
			  System.out.println("Couldn't create new organism: too many?");
			  return("OK");
		  };
		  panel.repaint();
		  return(""+i); // send new ID to user
		  
	    case 'Q': // quit
		  System.exit(0);
		  
	    default:
// assume command starts with an ID number
	      int id; // id number
	      //if (pieces.length==0) return("OK");

	      try{
	        id=Integer.parseInt(pieces[0]);
	      }catch(Exception e){
	    	  System.out.println("Error parsing <" + pieces[0] + ">:" + e);
	    	  return("OK");
	      }
	      
            if ((pieces.length == 6) && (pieces[1].equals("T"))){ // toggle this individual's tag
// full syntax is id T RED GREEN BLUE BLINK
              int red=Integer.parseInt(pieces[2]);
              int green=Integer.parseInt(pieces[3]);
              int blue=Integer.parseInt(pieces[4]);
              int blink=Integer.parseInt(pieces[5]);
              panel.setTag(id,red,green,blue,blink);
              panel.repaint();
              return("OK");
            }

	      if (pieces.length==2){ // id DIR or id F or id Q or id T or b f
            if (pieces[1].equals("O")){ // mark individual as old
              panel.setOld(id);
              panel.repaint();
              return("OK");
            }

            if (pieces[1].equals("L")){
            	panel.turnLeft(id);
            	panel.repaint();
            	return("OK");
            }

            if (pieces[1].equals("R")){
            	panel.turnRight(id);
            	panel.repaint();
            	return("OK");
            }

            if (pieces[1].equals("F")){
            	String response=panel.forward(id);
            	panel.repaint();
            	return(response);
            }

            if (pieces[1].equals("Q")){
            	return(panel.query(id));
            }
	      }

	      System.out.println("Unknown command: <" + buffer + ">");
	      return("OK");
	  }
  }

  
  Server(VEco parent)
  {
	this.parent=parent;
    panel=parent.panel;
  }

  public void run()
  {
	ServerSocket ss;
/***
	try {
	} catch (Exception e) {
		System.out.println("Server error: " + e);
		return;
	}
***/
	while (true){
	  Socket sock;
	  try {
                ss=new ServerSocket(parent.getPort());
		sock = ss.accept();
                ss.close();
	  } catch (IOException e) {
		System.out.println("Error accepting connection: " + e);
		return;
	  }
	  processClient(sock);
	}
  }
  
  void processClient(Socket sock) //throws Exception
  {
	Scanner s;
	PrintWriter pw;
    parent.setTitle("Connected to " + sock.getRemoteSocketAddress());
    try{
      s=new Scanner(sock.getInputStream());
      pw=new PrintWriter(sock.getOutputStream());
    } catch(Exception e){
    	System.out.println("Server connect error: " + e);
    	return;
    }
    while (s.hasNextLine()){
    	String buffer=s.nextLine();
    	String response=process(buffer);
    	if (response.length()!=0){
    		pw.println(response);
    		pw.flush();
    	}
    }
    s.close();
  }
}
