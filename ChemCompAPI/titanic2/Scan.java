import java.util.Scanner;
import java.io.File;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.Iterator;

class Scan{

  public static void main(String[] args) throws Exception
  {
    ArrayList<Passenger> list=new ArrayList<Passenger>();
    Scanner sc=new Scanner(new File ("titanic.csv"));
    sc.nextLine(); // skip header
    while (sc.hasNextLine()){
      String buffer=sc.nextLine(); // read a line of data
      Passenger pass=new Passenger(buffer); // parse line, save in passenger object
      list.add(pass); // add to ArrayList
    }
    sc.close();

// now look at data
    Iterator<Passenger> li=list.iterator();
    while (li.hasNext()){
      Passenger p=li.next();
      System.out.println("Index " + p.ID + ": " + p);
    }
  } // end of main
}
