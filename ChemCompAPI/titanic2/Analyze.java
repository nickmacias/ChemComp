//
// Main class for the entire system
// uses the run() method to create the universe, then starts its main thread
// everything else is driven from the universe...
//

import java.awt.EventQueue;

public class Analyze {

  public static void main(String[] args) {
    EventQueue.invokeLater(new Runnable() {
      public void run() {
        try {
          AnalyzeControl ac=new AnalyzeControl();
          ac.start();
        } catch (Exception e) {
          e.printStackTrace();
        }
      } // end of run()
    });
  }
}
