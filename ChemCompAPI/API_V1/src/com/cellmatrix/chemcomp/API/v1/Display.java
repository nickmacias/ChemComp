package com.cellmatrix.chemcomp.API.v1;

// Display displays its BufferedImage img
// supports pan/zoom/scroll/etc.
// can getImg() and setImg()
// also has text area with getText() and setText()

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.image.BufferedImage;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.BevelBorder;
import javax.swing.JScrollPane;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Nicholas J. Macias, Cell Matrix Corporation
 * @version 1.0
 * 
 * Display module for EEXIST (Egoless EXtened effect contInuous Space Time system)
 * <p>
 * A series of methods supporting live display of EEXIST activity.
 * This mostly displays contents of tubes, in a color-coded representation of SRC and DST amounts.
 * Displays a rolling time window (time on Y axis).
 * 
 * Also supports a live activity display, showing the current chemical concentrations across space.
 * 
 * 
 */
public class Display extends JFrame {

  private JPanel contentPane;
  private BufferedImage img=new BufferedImage(2000,2000,BufferedImage.TYPE_INT_ARGB);
  protected DrawingPanel panel; // for displaying graphics
  private JTextArea msg;
  private JScrollPane scrollPane_1;
  protected EEXIST eexist; // this is the EEXIST to which we're connected
  protected int scaleX=2,scaleY=1; // zoom
  protected int numStepsOnDisplay=900; // y scale
  private double minX,maxX,dx; // from EEXIST
  private double RGBScale=70; // how much to scale display by
  private boolean exitOnClose=true;

  GraphFrame gf; // for plotting info about system configuration
  
  /**
   * Display or hide detailed GraphFrame. The GraphFrame shows the SRC/DST concentrations across the spatial dimension.
   * The titlebar shows the SRC/DST, Bias and Diamater settings of wherever the mouse is positioned in the GraphFrame.
   * @param state visibility (true to show, false to hide)
   */
  public void showGF(boolean state)
  {
    gf.setVisible(state);
  }
  
  /**
   * Specify prefix to appear in the GraphFrame's titlebar
   * @param s String to display
   */
  public void setGFTitle(String s)
  {
    gf.setUserTitle(s); // this will prefix the mouse data
  }
  
  protected BufferedImage getImg()
  {
    return(img);
  }
  
  /**
   * Specify whether or not the entire app should exit when the display is closed.
   * @param exitOnClose true (default) causes entire app to exit when display is closed; false just closes the Display window.
   */
  public void exitOnClose(boolean exitOnClose)
  {
    this.exitOnClose=exitOnClose;
    if (exitOnClose) setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    else setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
  }
  
  /**
   * set the intensity of the display
   * @param RGBScale scale the SRC/DST concentrations by this amount to get RGB values (0-255)
   */
  public void setIntensity(int RGBScale)
  {
    this.RGBScale=RGBScale;
  }

/** display a message on top of the main window
 * 
 * @param s string to display
 */
  public void setMessage(String s)
  {
    msg.setText(s);
  }

/**
 * Append text to the message displayed above the main window
 * @param s string to append
 */
  public void appendMessage(String s)
  {
    msg.setText(msg.getText()+s);
    msg.setCaretPosition(msg.getText().length());
  }

/**
 * read message displayed above the main window
 * @return the displayed message
 */
  public String getMessage()
  {
    return(msg.getText());
  }
  
  /**
   * read the current intensity setting
   * @return intensity, 0-255
   */
  public double getIntensity()
  {
    return(RGBScale);
  }
  
  /**
   * display live graph of current system state
   * @param state true to display, false to remove
   */
  public void showLiveUpdates(boolean state)
  {
    gf.setVisible(state);
  }

  // TODO clear, scale XY, origin X (or range)

/**
 * Create a Display object for visual output of system state after each step()
 * @param e an EXXIST system whose state will be displayed in this window.
 */
  public Display(EEXIST e)
  {
	eexist=e; // remember our connection
	minX=e.getMinX();
	maxX=e.getMaxX();
	dx=e.getDx();
	
// tell EEXIST about us
	e.saveDisplay(this);
// probably should do this AFTER making gf etc.
// otherwise, if a separate thread is calling e.step, e will see that its
// display is not null, and will call localpaint, which expects gf
// and panel to exist

    gf=new GraphFrame();
    gf.saveE(e);
    gf.setVisible(false);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 805, 604);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);

    JScrollPane scrollPane = new JScrollPane();

    scrollPane_1 = new JScrollPane();
    GroupLayout gl_contentPane = new GroupLayout(contentPane);
    gl_contentPane.setHorizontalGroup(
    	gl_contentPane.createParallelGroup(Alignment.LEADING)
    		.addGroup(gl_contentPane.createSequentialGroup()
    			.addGap(10)
    			.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
    				.addComponent(scrollPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE)
    				.addComponent(scrollPane_1, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE))
    			.addContainerGap())
    );
    gl_contentPane.setVerticalGroup(
    	gl_contentPane.createParallelGroup(Alignment.TRAILING)
    		.addGroup(gl_contentPane.createSequentialGroup()
    			.addContainerGap()
    			.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
    			.addPreferredGap(ComponentPlacement.UNRELATED)
    			.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
    			.addContainerGap())
    );

        panel = new DrawingPanel();
        gf.savePanel(panel); // for access to cursor array
        panel.addMouseListener(new MouseAdapter() {
        	public void mouseClicked(MouseEvent arg0) {
        	  // show bin values at selected time and space location
        	   gf.setVisible(true);
        	   double xx=(double)arg0.getX()/(double)scaleX; // true x value
        	   double yy=(double)arg0.getY()/(double)scaleY; // and t value
        	   if (xx >= maxX/dx || xx < 0 ||
        	       yy > numStepsOnDisplay || yy < 0){
        	         System.out.println("Out of active window - try again");
        	         return;
        	   }
        	   // read source and dst values
        	   // TODO call live update on mouse click
        	   //////gf.setup(universe.eexist.allBins,0,(int)(C.maxX/C.dx),(int) yy);
        	   //////gf.repaint();
        	   //double s=universe.exist.allBins[(int) xx][C.S][(int) yy];
        	   //double d=universe.exist.allBins[(int) xx][C.D][(int) yy];
                   //System.out.println("Bin " + (int) xx + " t=" + (int) yy + ": S,D=" +
                                      //s + "," + d);
        	}
        });
        scrollPane_1.setViewportView(panel);
        panel.saveDisplay(this);
    msg = new JTextArea("");
    msg.setLineWrap(true);
    msg.setForeground(Color.GREEN);
    msg.setBackground(Color.BLACK);
    scrollPane.setViewportView(msg);
    msg.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
    contentPane.setLayout(gl_contentPane);
  }
  
  /***
   * disconnect a display from an EEXIST. The display is not affected by this.
   */
  public void disconnect() // remove from EEXIST
  {
    eexist.releaseDisplay();
  }
  
  private void paintComponent(Graphics g)
  {
	panel.repaint();
  }
  
  /**
   * force the detail window to updated
   */
  public void updateDisplay()
  {
    if (gf != null) gf.repaint();
  }
  
/**
 * set a cursor on the display. Causes a vertical line to be drawn at the given location
 * 
 * @param loc x location of the cursor
 */
  public void cursor(double loc)
  {
    panel.cursor(loc);
  }
  
/**
 * clear all display cursors
 */
  public void cursor()
  {
    panel.cursor();
  }
}
