package com.cellmatrix.chemcomp.API.v1;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

class MyScrollPane extends JPanel{

  GraphFrame gf;
  DrawingPanel mainPanel=null;
    
  public MyScrollPane()
  {
  }

  public void savePanel(DrawingPanel mainPanel)
  {
    this.mainPanel=mainPanel;
  }
  
  public void saveGF(GraphFrame gf)
  {
    this.gf=gf;
  }

  double s[],d[]; // arrays of values to display
  double diam[],biasS[],biasD[]; // more values to display

  int xMax=0,xMin=0; // bounds on those arrays
  double xZF=1.,yZF=1.; // X and Y zoom (shift/scroll for X)
  double maxY; // largest y value
  int xStart=0,yStart=0; // upper-left corner
  boolean flip=false; // set to put blue before red

  public void shiftReset()
  {
    xStart=yStart=0;
    repaint();
  }

  public void shift(int dx, int dy)
  {
    xStart+=dx;
    yStart+=dy;
    repaint();
  }

  public void zoom(int w,boolean xZoom)
  {
    if (xZoom){
      if (w > 0) xZF*=.8; if (w < 0) xZF*=1.25;
      if (xZF < 1) xZF=1;
    } else { // y zoom
      if (w > 0) yZF*=.8; if (w < 0) yZF*=1.25;
    }
    //System.out.println("zoom=" + xZF + "," + yZF);
    repaint();
  }

// paint method
  public void paintComponent(Graphics g) // g points to a BufferedImage
  {
    super.paintComponent(g);

    Graphics2D g2=(Graphics2D) g;
    double sy=0,dy=0; // y values

    if (!flip){
      // plot S in RED
      g.setColor(Color.RED);
      for (int x=0;x<xMax;x++){
        sy=0;
        if (gf.showChem()) sy+=s[x];
        if (gf.showBias()) sy+=biasS[x];
        dy=0;
        if (gf.showChem()) dy+=d[x];
        if (gf.showBias()) dy+=biasD[x];
        g.fillRect((int)(x*xZF)-xStart,0-yStart,(int)(xZF),(int)(sy*yZF));
      }

      // D in BLUE
      g.setColor(Color.BLUE);
      for (int x=0;x<xMax;x++){
        sy=0;
        if (gf.showChem()) sy+=s[x];
        if (gf.showBias()) sy+=biasS[x];
        dy=0;
        if (gf.showChem()) dy+=d[x];
        if (gf.showBias()) dy+=biasD[x];
        g.fillRect((int)(x*xZF)-xStart,(int)(sy*yZF)-yStart,(int)(xZF),(int)(dy*yZF));
      }
    } else {
      // plot D in BLUE
      g.setColor(Color.BLUE);
      for (int x=0;x<xMax;x++){
        sy=0;
        if (gf.showChem()) sy+=s[x];
        if (gf.showBias()) sy+=biasS[x];
        dy=0;
        if (gf.showChem()) dy+=d[x];
        if (gf.showBias()) dy+=biasD[x];
        g.fillRect((int)(x*xZF)-xStart,0-yStart,(int)(xZF),(int)(dy*yZF));
      }

      // S in RED
      g.setColor(Color.RED);
      for (int x=0;x<xMax;x++){
        sy=0;
        if (gf.showChem()) sy+=s[x];
        if (gf.showBias()) sy+=biasS[x];
        dy=0;
        if (gf.showChem()) dy+=d[x];
        if (gf.showBias()) dy+=biasD[x];
        g.fillRect((int)(x*xZF)-xStart,(int)(dy*yZF)-yStart,(int)(xZF),(int)(sy*yZF));
      }
    }

    g.setColor(Color.GRAY);
    g2.setStroke(new BasicStroke(3));
    g.drawRect((int)(-xStart),(int)(-yStart),(int)(xMax*xZF),2000);
    g2.setStroke(new BasicStroke(1));
    
    if (mainPanel==null) return;
    // draw cursor(s)
    g.setColor(Color.YELLOW);
    if (mainPanel.cursors != null){
      for (int i=0;i<mainPanel.cursors.size();i++){
        int cursorX=mainPanel.cursors.get(i).intValue();
        g.drawLine((int)((cursorX*xZF)-xStart),0,(int)((cursorX*xZF)-xStart),getHeight()); //display.numStepsOnDisplay);
      } // all cursors drawn
    }
  }
}
