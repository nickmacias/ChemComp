package com.cellmatrix.chemcomp.API.v1;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;

// This is the panel on which we actually draw the image of the system
// Display is the calling-class, which maintains a BufferedImage
// and calls this class' repaint method
// This class' paintComponent() reads the image from Display, and then draws it

class DrawingPanel extends JPanel{

  ArrayList<Double> cursors=new ArrayList<Double>(); // holds list of cursors
  
  public DrawingPanel() {
  }

  Display display=null; // remember our display...
  
  protected void cursor(double loc)
  {
    cursors.add(loc/display.eexist.getDx());
  }
  
  protected void cursor()
  {
    cursors.clear();
  }

// remember from where to read the image
  public void saveDisplay(Display d)
  {
    display=d;
    setSize(2000,2000);
  }

// just display img from display object
  public void paintComponent(Graphics g)
  {
    super.paintComponent(g);
    if (display==null) return; // make swing happy...
    BufferedImage img=display.getImg();
    if (img != null){
      g.drawImage(img,0,0,this);
      g.setColor(Color.GREEN);
      g.drawLine(0,
                 ((display.eexist.getTimeStep()+1)*display.scaleY)%display.numStepsOnDisplay,
                 display.eexist.binMax*display.scaleX,
                 ((display.eexist.getTimeStep()+1)*display.scaleY)%display.numStepsOnDisplay);
// draw cursor(s)
      g.setColor(Color.YELLOW);
      for (int i=0;i<cursors.size();i++){
        int cursorX=display.scaleX*cursors.get(i).intValue();
        g.drawLine(cursorX,0,cursorX,display.numStepsOnDisplay);
      } // all cursors drawn
    }
  }
}