package com.cellmatrix.chemcomp.API.v1;

/**
 * 
 * @author Nicholas J. Macias, Cell Matrix Corporation
 * @version 1.0
 * 
 * Class for manipulating individuals, which are the collection of
 * chemical concentrations throughout the spatial extent of
 * the EEXIST system. This is nothing more than a 2-D array of
 * doubles, representing SRC and DST amounts in each tube from 0 to binMax.
 *
 */
 
public class Individual {
  private double minX,maxX,dx;
  private int binMax;
  protected double[][] mem;
  protected double diameters[];
  protected double bias[][];
  protected double karma;
/**
 * 
 * @param e the EEXIST system with which this individual is associated. This is only
 * relevant for the minX, maxX and dx values of the individual (so that their chemical
 * composition can be loaded into the EEXIST).
 */
  public Individual(EEXIST e)
  {
    minX=e.getMinX();
    maxX=e.getMaxX();
    dx=e.getDx();
    binMax=e.binMax;
    
// create storage for chemicals
    mem=new double[binMax+1][2];
    
// set diameter
    diameters=new double[binMax+1];
    for (int i=0;i<binMax+1;i++) diameters[i]=1.;
    
// set bias
    bias=new double[binMax+1][2];
    for (int i=0;i<binMax+1;i++) bias[i][0]=bias[i][1]=0.;
    
// default karma
    karma=5.;
  }
  
  /** read the amount of SRC or DST chemical in a tube
   * @param x tube's x coordinate
   * @param SD EEXIST.SRC or EEXIST.DST
   * @return total amount of SRC or DST chemical in tube
   */
  public double getTube(double x, int SD)
  {
    return(mem[toBin(x)][SD]);
  }
  
  /**
   * set SRC or DST level in a single tube
   * @param x spatial coordinate
   * @param SD EEXIST.SRC or EEXIST.DST
   * @param value value to save
   */
  public void setTube(double x, int SD, double value)
  {
    mem[toBin(x)][SD]=value;
  }
  
  /**
   * read the saved karma value for this individual
   * 
   * @return karma parameter for EEXIST karma
   */
  public double getkarma()
  {
    return(karma);
  }
  
  /**
   * Read array of diameters for each tube
   * @return array of diameters (default=1.0)
   */
  public double[] getAllDiameters()
  {
    double[] diameters=new double[1+binMax];
    for (int i=0;i<1+binMax;i++){
      diameters[i]=this.diameters[i];
    }
    return(diameters);
  }
  
  /**
   * Read array of diameters for each tube
   * @param diameters array of diameters
   */
  public void setAllDiameters(double[] diameters)
  {
    for (int i=0;i<1+binMax;i++){
      this.diameters[i]=diameters[i];
    }
  }
  
  /**
   * Read diameter of a tube
   * @param tube index of tube [0,binMax]
   * @return diameter
   */
  public double getDiameter(int tube)
  {
    return(diameters[tube]);
  }
  
  /**
   * Read diameter of a tube
    * @param xLoc location of tube to read
    * @return diameter
    */
  public double getDiameter(double xLoc)
  {
    return(diameters[toBin(xLoc)]);
  }
  
  /**
   * set bias levels for a single tube
   * @param tube tube number
   * @param SD EEXIST.SRC or EXIST.DST
   * @param bias bias level (0=no bias)
   */
  public void setBias(int tube, int SD, double bias)
  {
    this.bias[tube][SD]=bias;
  }
  
  /**
   * set bias levels for a single tube
   * @param xLoc spatial coordinate of tube
   * @param SD EEXIST.SRC or EXIST.DST
   * @param bias bias level (0=no bias)
   */
  public void setBias(double xLoc, int SD, double bias)
  {
    this.bias[toBin(xLoc)][SD]=bias;
  }
  
  /**
   * Load bias values for entire collection of tubes
   * @param bias 2D array [x][SRC/DST] of bias values
   */
  public void setAllBiass(double[][] bias)
  {
    for (int i=0;i<1+binMax;i++){
      this.bias[i][0]=bias[i][0];
      this.bias[i][1]=bias[i][1];
    }
  }
  
  /**
   * read bias level of a single tube
   * @param tube index of tube
   * @param SD EEXIST.SRC or EEXIST.DST
   * @return requested bias level
   */
  public double getBias(int tube, int SD)
  {
    return(bias[tube][SD]);
  }
  
  /**
   * read bias level of a single tube
   * @param xLoc spatial coordinate of tube
   * @param SD EEXIST.SRC or EEXIST.DST
   * @return requested bias level
   */
  public double getBias(double xLoc, int SD)
  {
    return(bias[toBin(xLoc)][SD]);
  }
  
  /**
   * read bias level of all tubes
   * @return 2d array [x][SRC/DST] of bias values, suitable as an argument to setAllBiass()
   */
  public double[][] getAllBiass()
  {
    double[][]bias=new double[1+binMax][2];
    for (int i=0;i<1+binMax;i++){
      bias[i][0]=this.bias[i][0];
      bias[i][1]=this.bias[i][1];
    }
    return(bias);
  }
  
  
/**
 * Set karma for an individual's karma
 * @param karma karma to set
 */
  public void setkarma(double karma)
  
  {
    this.karma=karma;
  }
  
  /**
   * set all tube diameters from an array of values
   * @param diameters array of tube diameters [0,binMax]
   */
  public void setDiameters(double[] diameters)
  {
    for (int i=0;i<binMax+1;i++){
      this.diameters[i]=diameters[i];
    }
  }
  
  /**
   * set diameter of a single tube
   * @param tube integer tube number [0,binMax]
   * @param diameter diameter to set
   */
  public void setDiameter(int tube,double diameter)
  {
    diameters[tube]=diameter;
  }
  
  /**
   * set diameter of a tube biasd on its spatial coordinates
   * @param xLoc X location of the tube
   * @param diameter diameter to set
   */
  public void setDiameter(double xLoc,double diameter)
  {
    diameters[toBin(xLoc)]=diameter;
  }
  
  /** zero-out all tubes. This properly belongs more to the Individual class than the EEXIST class */
  public void clearAllTubes()
  {
    for (int i=0;i<binMax+1;i++){
      mem[i][0]=mem[i][1]=0;
    }
  }
  
  /**
   * Load memory into an individual
   * @param mem 2D array to copy into the individual
   */
  public void setAllTubes(double[][] mem)
  {
    for (int i=0;i<1+binMax;i++){
      this.mem[i][0]=mem[i][0];
      this.mem[i][1]=mem[i][1];
    }
  }
  
  /**
   * read individual's chemical composition
   * @return 2D array of chemical values, suitable for setAllTubes
   */
  public double[][] getAllTubes()
  {
    double[][] mem=new double[1+binMax][2];
    for (int i=0;i<1+binMax;i++){
      mem[i][0]=this.mem[i][0];
      mem[i][1]=this.mem[i][1];
    }
    return(mem);
  }
  
  // convert from double (x) to int (bin)
  private int toBin(double x) // convert real # to bin #
  {
    int retVal=(int)((x-minX)/dx);
    if (retVal>binMax) retVal=(-1);
    if (retVal < 0) retVal=(-1);
    return(retVal%(binMax+1)); // rollover from 0 to binMax
  }
 
 /* TODO
  * Separate class to breed, mutate, evaluate, copy mem[][], disk IO, ...?
  */
}