package com.cellmatrix.chemcomp.API.v1;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import java.awt.event.InputEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import java.awt.Color;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeListener;

import javax.swing.event.ChangeEvent;
import javax.swing.JTextField;

class GraphFrame extends JFrame {
  private JPanel contentPane;
  MyScrollPane scrollPane;
  int startingX,startingY; // set when the mouse is clicked
  JCheckBox chckbxUpdateLive;
  JCheckBox chemBox; // select to show chemical levels
  JCheckBox biasBox; // select to show bias level
  JToggleButton tglbtnFlipRb;
  boolean showDetails=false; // set true to respond to mouse movements with info
  private final JTextField detailField = new JTextField();
  DrawingPanel mainPanel=null;
  EEXIST e;
  String userTitle=""; // prefix for titlebar
  
  void setUserTitle(String title)
  {
    userTitle=title;
  }
  
  public void saveE(EEXIST e)
  {
    this.e=e;
  }
  
  public void savePanel(DrawingPanel mainPanel)
  {
    this.mainPanel=mainPanel;
    scrollPane.savePanel(mainPanel);
  }

  public boolean liveUpdates()
  {
    return(chckbxUpdateLive.isSelected());
  }
  
  public boolean showChem()
  {
    return(chemBox.isSelected());
  }
  
  public boolean showBias()
  {
    return(biasBox.isSelected());
  }

  public GraphFrame() {
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 836, 387);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);

    scrollPane = new MyScrollPane();
    scrollPane.saveGF(this); // so it can find our checkbox values
    scrollPane.setBackground(Color.DARK_GRAY);
    scrollPane.addMouseListener(new MouseAdapter() {
    	public void mousePressed(MouseEvent arg0) {
    	  //showDetails=!showDetails;
    	  //System.out.println(showDetails);
    	  detailField.setVisible(showDetails);
    	  detailField.setLocation(arg0.getX(),arg0.getY());
    	}
    });
    scrollPane.addMouseMotionListener(new MouseMotionAdapter() {
    	public void mouseMoved(MouseEvent arg0) {
    	  double bin=(arg0.getX()+scrollPane.xStart)/scrollPane.xZF;
    	  /***
    	  bin=bin*C.dx;
    	  if (bin < 0) bin=0;
    	  if (bin >= C.maxX) bin=C.maxX-C.dx;
    	  ***/
    	  int x=(int)bin;
    	  if (scrollPane.s == null) return; // haven't set up these arrays yet...
    	  if (x >= scrollPane.s.length) return; // out of bounds
    	  double xloc=bin*e.getDx();
    	  String out=String.format("x=%.2f Src=%.2f Dst=%.2f S_Bias=%.2f D_Bias=%.2f Diam=%.2f",
    	           xloc,
    	           scrollPane.s[x],
    	           scrollPane.d[x],
    	           scrollPane.biasS[x],
    	           scrollPane.biasD[x],
    	           scrollPane.diam[x]);
    	  setTitle(userTitle+"<"+out+">");
    	}
    });

    scrollPane.addMouseWheelListener(new MouseWheelListener() {
    	public void mouseWheelMoved(MouseWheelEvent arg0) {
    	  scrollPane.zoom(arg0.getWheelRotation(),(0!=(arg0.getModifiers()&InputEvent.SHIFT_MASK))); // -1 means zoom in +1 means zoom out
    	  //System.out.println("mod=" + arg0.getModifiers());
    	  repaint();
    	}
    });

    JButton btnResetScroll = new JButton("Reset Scroll");
    btnResetScroll.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent arg0) {
    	  shiftReset();
    	}
    });

    JButton btnResetZoom = new JButton("Reset Zoom");
    btnResetZoom.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent arg0) {
    	  scrollPane.xZF=scrollPane.yZF=1.;
    	  repaint();
    	}
    });

    chckbxUpdateLive = new JCheckBox("Update Live");
    chckbxUpdateLive.setSelected(true);

    tglbtnFlipRb = new JToggleButton("Flip R/B");
    tglbtnFlipRb.addChangeListener(new ChangeListener() {
    	public void stateChanged(ChangeEvent arg0) {
    	  scrollPane.flip=tglbtnFlipRb.isSelected();
    	  repaint();
    	}
    });
    
    chemBox = new JCheckBox("chem");
    chemBox.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent arg0) {
          repaint();
    	}
    });

    chemBox.setSelected(true);
    
    biasBox = new JCheckBox("bias");
    biasBox.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent e) {
    	  repaint();
    	}
    });
    GroupLayout gl_contentPane = new GroupLayout(contentPane);
    gl_contentPane.setHorizontalGroup(
    	gl_contentPane.createParallelGroup(Alignment.LEADING)
    		.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 804, Short.MAX_VALUE)
    		.addGroup(gl_contentPane.createSequentialGroup()
    			.addComponent(btnResetScroll)
    			.addGap(18)
    			.addComponent(btnResetZoom)
    			.addGap(36)
    			.addComponent(chckbxUpdateLive)
    			.addPreferredGap(ComponentPlacement.RELATED)
    			.addComponent(tglbtnFlipRb)
    			.addGap(18)
    			.addComponent(chemBox)
    			.addPreferredGap(ComponentPlacement.RELATED)
    			.addComponent(biasBox)
    			.addGap(26))
    );
    gl_contentPane.setVerticalGroup(
    	gl_contentPane.createParallelGroup(Alignment.LEADING)
    		.addGroup(gl_contentPane.createSequentialGroup()
    			.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
    				.addComponent(btnResetScroll)
    				.addComponent(btnResetZoom)
    				.addComponent(chckbxUpdateLive)
    				.addComponent(tglbtnFlipRb)
    				.addComponent(chemBox)
    				.addComponent(biasBox))
    			.addPreferredGap(ComponentPlacement.RELATED)
    			.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 283, Short.MAX_VALUE)
    			.addGap(0))
    );
    detailField.setText("HAHAHA!");
    scrollPane.add(detailField);
    detailField.setVisible(false);
    detailField.setEditable(false);
    detailField.setColumns(50);
    contentPane.setLayout(gl_contentPane);
  }

  public void shiftReset()
  {
    scrollPane.shiftReset();
  }

// prepare to paint
  public void setup(double[][] mem, double[] diam, double[][] bias,int xMi,int xMa,int time)
  {
    scrollPane.xMax=xMa;
    scrollPane.xMin=xMi;

    scrollPane.s=new double[scrollPane.xMax];
    scrollPane.d=new double[scrollPane.xMax];
    scrollPane.diam=new double[scrollPane.xMax];
    scrollPane.biasS=new double[scrollPane.xMax];
    scrollPane.biasD=new double[scrollPane.xMax];

    double thisY,maxY=0;

    for (int x=scrollPane.xMin;x<scrollPane.xMax;x++){
      scrollPane.s[x]=mem[x][EEXIST.SRC];
      scrollPane.d[x]=mem[x][EEXIST.DST];
      scrollPane.biasS[x]=bias[x][EEXIST.SRC];
      scrollPane.biasD[x]=bias[x][EEXIST.DST];
      scrollPane.diam[x]=diam[x];
      thisY=scrollPane.s[x]+scrollPane.d[x];
      if (thisY > maxY) maxY=thisY;
    }
    scrollPane.maxY=maxY;
    //scrollPane.setSize(scrollPane.xMax-scrollPane.xMin,(int)(maxY));
  }
}
