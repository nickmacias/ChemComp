import com.cellmatrix.chemcomp.API.v1.*;

public class Core extends Thread{
  EEXIST e;
  Display disp;
  
  int popSize=250; // total # of individuals in the population
  int survivorSize=10; // # of individuals to keep after each generation

  Genome[] population=new Genome[popSize];
  Genome[] best=new Genome[survivorSize]; // save best members here
  double[] score=new double[popSize]; // save scores

  public void run()
  {
    e=new EEXIST(); // main EEXIST object
    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display
    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,10,0,400,1,"Effect","cpHandler");
    cp.connect(this, ControlPanel.Slider,1,128,0,255,2,"Intensity","cpHandler");

    int gen=0; // track generations

// make an initial population
    for (int i=0;i<popSize;i++){ 
      population[i]=new Genome(e.numTubes());
      population[i].randomize();
    }
// main simulation loop
    while (true){
      ++gen;
      for (int indiv=0;indiv<popSize;indiv++){
        population[indiv].load(e); // load this individual into the system
        score[indiv]=0; // initial score
        //for (int i=0;i<200;i++) e.step(1); // let the system stabilize for a while
        for (int test=0;test<8;test++){
          score[indiv]+=assess(test,e); // run a test, tally the score
        } // this individual assessed
        System.out.print(gen + ":" + indiv + "(" + score[indiv] + ")");
        if (indiv%10==0) System.out.println();
      } // end of evaluation of all individuals
      System.out.println("\nGen " + gen + " Best scores=" + rank() + "\n"); // rank all individuals
      breed(.01); // breed them, with mutation rate of 1%
    } // and repeat forever
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // effect
      e.setEffect(((double)arg.getSliderPos())/10);
      break;
    case 2: // intensity
      disp.setIntensity(arg.getSliderPos());
      break;
    }
  }

  double assess(int testNum,EEXIST e) // assess this individual
  {
  // randomize a test
  /***
    boolean a=(Math.random()<.5);
    boolean b=(Math.random()<.5);
  ***/
    boolean a=((testNum&1) != 0);
    boolean b=((testNum&2) != 0);
    boolean y=(a^b);
    
    double score=0; // running score

    // input a is placed in [0,2]; b in [4,6]; y is read from [8,10]
    for (double x=0;x<4;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,a?0:20);
      e.setTube(x,EEXIST.DST,a?0:20); // chemical balance (input)
      //e.setBase(x,EEXIST.SRC,0);
      //e.setBase(x,EEXIST.DST,0); // these are true values
      //e.setDiameter(x,0); // these values don't change...
    }

    for (double x=0;x<2;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,b?0:20);
      e.setTube(x,EEXIST.DST,b?0:20);
      //e.setDiameter(x,0);
    }
    
    for (double x=4;x<6;x+=e.getDx()){
      ////e.setDiameter(x,1); // allow this region to modulate easily
    }
    // step 50 ticks, then read value for 50 more
    for (int i=0;i<25;i++) e.step(1);
    for (int i=0;i<25;i++){
      e.step(1);
      for (double x=8;x<10;x+=e.getDx()){
        double sum=e.getTube(x, EEXIST.SRC)+e.getTube(x, EEXIST.DST);
        if ((y && sum>15) || ((!y) && sum<5)) ++score;
      } // this output evaluated
    } // end of tests
    return(score);
  }

  double rank() // sort population[] by score[]
  {
    for (int loop=0;loop<popSize;loop++){ // good old bubble sort!
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (score[i+1] > score[i]){ // swap
          double temp=score[i+1];score[i+1]=score[i];score[i]=temp;
          Genome temp2=population[i+1];population[i+1]=population[i];population[i]=temp2;
        }
      }
    }

// copy best elements of population to best[] array
    double bestScore=0;
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i].copy();
      bestScore+=score[i];
    }
    return(bestScore);
  }
  
  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy();
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].merge(best[(int)(Math.random()*survivorSize)]);
      }
      population[i].mutate(mutateRate); // random variation
    }
  }
}