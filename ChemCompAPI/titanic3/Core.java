import com.cellmatrix.chemcomp.API.v1.*;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class Core extends Thread{
  EEXIST e;
  Display disp;

  String tname="titanic.csv"; // raw data from Kaggle.Com
  
  int popSize=15; // total # of individuals in the population
  int survivorSize=5; // # of individuals to keep after each generation

  PrintWriter pw;

  Genome[] population=new Genome[popSize];
  Genome[] best=new Genome[survivorSize]; // save best members here
  double[] score=new double[popSize]; // save scores

  public void run()
  {
    try{
      pw=new PrintWriter("raw.txt");
    } catch(Exception e){
      System.out.println("Can't write to output file ("+e+")- goodbye");
      return;
    }
    e=new EEXIST(0,60,0.0625); // main EEXIST object
    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display
    disp.cursor(36);
    disp.cursor(40);
    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,50,0,400,1,"Karma","cpHandler");
    cp.connect(this, ControlPanel.Slider,1,128,0,255,2,"Intensity","cpHandler");

    writeStatus(e,pw); // write relevant info about EEXIST
    int gen=0; // track generations

// make an initial population
    for (int i=0;i<popSize;i++){ 
      population[i]=new Genome(e.numTubes());
      population[i].randomize();
    }

// load titanic data
    ArrayList<Passenger> list=new ArrayList<Passenger>();
    try{
      Scanner sc=new Scanner(new File(tname));
      sc.nextLine(); // skip header
      while (sc.hasNextLine()){
        String buffer=sc.nextLine(); // read a line of data
        Passenger pass=new Passenger(buffer); // parse line, save in passenger object
        list.add(pass); // add to ArrayList
      }
      sc.close();
    } catch (Exception e){
      System.out.println("Error processing titanic file " + tname + ": " + e);
      return;
    }

    System.out.println("Processed " + list.size() + " records from " + tname);

// main simulation loop
    while (true){
      ++gen;
      for (int indiv=0;indiv<popSize;indiv++){
        score[indiv]=0; // initial score

        Iterator<Passenger> li=list.iterator(); // start an iterator
        score[indiv]=0; // initial score
        while (li.hasNext()){ // iterate over passengers
          Passenger p=li.next();
          if (p.ID <= 100){ // train on the first 400 for now
            population[indiv].load(e); // load this individual into the system
            score[indiv]+=assess(p,e); // run a test, tally the score
          }
        } // this individual assessed

        double score2=0;
        writeIndiv(gen,indiv,population[indiv],score[indiv],score2,pw);
        System.out.println(gen + ":" + indiv + "(" + score[indiv] +
                         "," + score2 + ")");

        //if (indiv%5==4) System.out.println();
      } // end of evaluation of all individuals
      System.out.println("\nGen " + gen + " Best scores=" + rank() + "\n"); // rank all individuals
      breed(.20); // breed them, with mutation rate of 10%
    } // and repeat forever
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      writeStatus(e,pw); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 2: // intensity
      disp.setIntensity(arg.getSliderPos());
      break;
    }
  }

  double assess(Passenger p,EEXIST e) // assess this individual
  {
// load inputs
    load(e,0,4,(p.sex=='m')?10:20); // sex, m or f
    load(e,4,8,p.cls*10); // passenger class, 1 2 or 3
    load(e,8,12,p.age/2); // age, nominally 0-80 (clip at 80)
    load(e,12,16,4+p.sibsp*4); // siblings/spouse, 0-8?
    load(e,16,20,10+p.parch*10); // parents/children, mostly 0-2
    load(e,20,24,p.fare/15.); // up to 512
    load(e,24,28,(p.embarked=='c')?10:((p.embarked=='q')?20:30));
    load(e,28,40,0.); // empty space

// step the EEXIST
    e.step(64);

// read output
    double sum=readAvg(e,36,40);
    if ((sum>=15 && !p.survived) ||
        (sum<15 && p.survived)) return(1);
    return(0);
  }

  double readAvg(EEXIST e, double start, double end)
  {
    double sum=0;
    for (double x=start;x<end;x+=e.getDx()){
      sum+=e.getTube(x,EEXIST.SRC)+e.getTube(x,EEXIST.DST);
    }
    sum=sum/((end-start)/e.getDx()); // average value
    return(sum);
  }

  void load(EEXIST e, double start, double end, double value)
  {
    if (value < 0) value=0;
    if (value > 60) value=60;

    for (double x=start;x<end;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,value);
      e.setTube(x,EEXIST.DST,value);
    }
  }

  double rank() // sort population[] by score[]
  {
    for (int loop=0;loop<popSize;loop++){ // good old bubble sort!
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (score[i+1] > score[i]){ // swap
          double temp=score[i+1];score[i+1]=score[i];score[i]=temp;
          Genome temp2=population[i+1];population[i+1]=population[i];population[i]=temp2;
        }
      }
    }

// copy best elements of population to best[] array
    double bestScore=0;
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i].copy();
      bestScore+=score[i];
    }
    return(bestScore);
  }
  
  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy(); // preserve this member!
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].merge(best[(int)(Math.random()*survivorSize)]);
        population[i].mutate(mutateRate); // random variation
      }
// don't mutate that best population...
      //population[i].mutate(mutateRate); // random variation
    }
  }

  boolean firstWS=true;
  void writeStatus(EEXIST e,PrintWriter pw)
  {
    if (firstWS){
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+
              e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
              ","+e.numTubes());
    pw.flush();
  }

  boolean firstWI=true;
  void writeIndiv(int gen,int indiv,Genome genome,
                  double score,double score2,PrintWriter pw)
  {
    if (firstWI){
      pw.println("*I,gen,ind,score,score2,len[i],var[i],init[i],initS[i],"+
                 "initD[i],delta[i],deltaS[i],deltaD[i]");
      firstWI=false;
    }
    pw.print("I,"+gen+","+indiv+","+score+","+score2);
    for (int i=0;i<Genome.GenomeSize;i++){
      pw.print(","+genome.gene[i].len+","+genome.gene[i].variation+","+
               genome.gene[i].init+","+genome.gene[i].initS+","+genome.gene[i].initD+","+
               genome.gene[i].delta+","+genome.gene[i].deltaS+","+genome.gene[i].deltaD);
    }
    pw.println();pw.flush();
  }
}
