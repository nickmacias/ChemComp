import com.cellmatrix.chemcomp.API.v1.*;
import java.awt.EventQueue;
import java.util.Scanner;
import java.io.File;
import java.net.Socket;
import java.io.PrintWriter;

//
// main execution thread. started by analyzecontrol, and used
// to run the simulation

public class AnalyzeCore extends Thread{
  int stepLimit=0;
  boolean running;
  EEXIST e;Display disp;

  boolean testing=false;
  int delay=0;
  AnalyzeControl parent;

// connetion to simulation server
  Socket server;
  PrintWriter toServer;
  Scanner fromServer;

// I/O regions
// locations of inputs
  double fuelIn=0;     // [0,4] diam=0
  double altIn=8;      // [8,12] diam=0
  double speedIn=16;   // [16,20] diam=0
// and thrust request
  double thrustOut=24; // [24,28]
// initiator
  double go=36;        // [36,40]


// save parent so we can read delay
  public void saveMe(AnalyzeControl parent)
  {
    this.parent=parent;
    server=parent.server;
    toServer=parent.toServer;
    fromServer=parent.fromServer;
  }

// constructor
  public AnalyzeCore(EEXIST e, Display disp)
  {
// save EEXIST objects
    this.e=e;this.disp=disp;
  }

  public void testing(boolean testing)
  {
    this.testing=testing;
  }

  public void halt() // allows halt to be requested
  {
    running=false;
    testing=false; // no output monitor
  }
  
  public void run()
  {
    String temp,fields[];
    boolean done;
    double fuel=0,speed=0,alt=0;
    double minAlt=10000000;

    int steps=0; // # of steps we've done
    running=true; // clear to tell thread to exit

    if (testing){ // start the test
// set diameters
      for (double x=0;x<4;x+=e.getDx()){
        e.setDiameter(fuelIn+x,0);
        e.setDiameter(altIn+x,0);
        e.setDiameter(speedIn+x,0);
      }

// prepare connection to server
      toServer.println("r");toServer.flush(); // reset the sim

      toServer.println("d");toServer.flush(); // request (initial) data
      temp=fromServer.nextLine(); // data
      fields=temp.split(","); // [1]=done, [3]=fuel, [5]=speed, [7]=alt

      //done=(fields[1].equals("true"));
      fuel=Double.parseDouble(fields[3]);
      speed=Double.parseDouble(fields[5]);
      alt=Double.parseDouble(fields[7]);

// Inject these params into (0-diameter) regions
      load(fuel,speed,alt);

// then inject START signal
      inject(go,go+4,20.); // this is the "go" signal
    }

// main simulation loop
    while (running){

// delay as indicated by user
      delay=parent.getDelay();
      if (delay != 0) myDelay(delay);

      if (!testing){ // just doing a normal run command...
        e.step(1);
        if ((stepLimit!=0) && (++steps==stepLimit)) running=false;
        continue;
      }

// here we're driving the simulated lander
      e.step(1); // advance sim

// read the thrust output
      boolean thrust=detect(thrustOut,thrustOut+4,10.);

// set thrust in sim
      toServer.println((thrust)?"T":"t");toServer.flush();

// step sim
      toServer.println("s");toServer.flush();

// read data
      toServer.println("d");toServer.flush();
      temp=fromServer.nextLine(); // data
      fields=temp.split(","); // [1]=done, [3]=fuel, [5]=speed, [7]=alt
      running=(fields[1].equals("false")); // if server reports true then exit
      fuel=Double.parseDouble(fields[3]);
      speed=Double.parseDouble(fields[5]);
      alt=Double.parseDouble(fields[7]);

// save minimum altitude
      if (alt < minAlt) minAlt=alt; // record how close we've gotten to the surface
// if we've gone too high, we're going to crash...
      if (alt > 1000){
        System.out.println("Too high: min alt=" + minAlt);
        System.out.println("last stat: Fuel=" + fuel + " alt=" + alt + " speed=" + speed);
        running=false;
      }

// update EEXIST regions with new data
      load(fuel,speed,alt);
    } // end of run loop
    System.out.println("Final stat: Fuel=" + fuel + " alt=" + alt + " speed=" + speed);
    testing=false;

  } // thread exits here

  public void setLimit(int stepLimit)
  {
    this.stepLimit=stepLimit;
  }

  public void myDelay(int t)
  {
    try{Thread.sleep(t);} catch(Exception e){}
    return;
  }

// inform EEXIST of current status
  public void load(double fuel, double speed, double alt)
  {
// fuel run from 20-0, so inject fuel*1.5 (0->30)
    inject(fuelIn,fuelIn+4,trim(.1*fuel*1.5));
// altitude roughly 0-250, so inject alt/6
    inject(altIn,altIn+4,trim(.1*alt/6.));
// speed roughly 10 (rising) to -30 (fall) so inject speed+30
    inject(speedIn,speedIn+4,trim(.1*(speed+30.)));
  }

  public double trim(double x)
  {
    if (x<0) x=0.;
    if (x>40) x=40.;
    return(x);
  }

// input and output routines
  public void inject(double start, double end, double level) // inject chemicals
  {
    for (double x=start;x<end;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,level);
      e.setTube(x,EEXIST.DST,level);
    }
  }

// read a region and check average vs. threshold
  public boolean detect(double start, double end, double threshold)
  {
    double sum=0;
    for (double x=start;x<end;x+=e.getDx()){
      sum=sum+e.getTube(x,EEXIST.SRC)+e.getTube(x,EEXIST.DST);
    }
    return(sum >= threshold*(end-start)/e.getDx()); // avg >= threshold
  }

}
