import com.cellmatrix.chemcomp.API.v1.*;
import java.io.PrintWriter;
import java.util.Scanner;
import java.net.Socket;

public class Core extends Thread{
  boolean head=true;
  EEXIST e;
  Display disp;

  Socket server;
  PrintWriter toServer;
  Scanner fromServer;

  int popSize=250; // total # of individuals in the population
  int survivorSize=10; // # of individuals to keep after each generation

  double initSpeed=0,initAlt=0,initFuel=30;
  double grav=-9.8,thrust=12,dt=0.125;

// locations of inputs
  double fuelIn=0;     // [0,4] diam=0
  double altIn=8;      // [8,12] diam=0
  double speedIn=16;   // [16,20] diam=0
// and thrust request
  double thrustOut=24; // [24,28]
// initiator
  double go=36;        // [36,40]

  PrintWriter pw;

  Genome[] population=new Genome[popSize];
  Genome[] best=new Genome[survivorSize]; // save best members here
  double[] score=new double[popSize]; // save scores

  public void run()
  {
// prepare for raw output
    try{
      pw=new PrintWriter("raw.txt");
    } catch(Exception e){
      System.out.println("Can't write to output file ("+e+")- goodbye");
      return;
    }

// connect to server
    try{
      server=new Socket("localhost",1213);
      toServer=new PrintWriter(server.getOutputStream());
      fromServer=new Scanner(server.getInputStream());
    } catch (Exception ee){
      System.out.println("Assess error: " + ee);
      return;
    }

// prepare EEXIST
    e=new EEXIST(); // main EEXIST object
    e.setKarma(5);

    if (head){ // skip if headless
      disp=new Display(e); // create a new display
      disp.setVisible(true);  // show the display
      ControlPanel cp=new ControlPanel("Control Panel");
      cp.connect(this, ControlPanel.Slider,0,50,0,400,1,"Karma","cpHandler");
      cp.connect(this, ControlPanel.Slider,1,128,0,255,2,"Intensity","cpHandler");
    }

    writeStatus(e,pw); // write relevant info about EEXIST
    int gen=0; // track generations

// make an initial population
    for (int i=0;i<popSize;i++){ 
      population[i]=new Genome(e.numTubes());
      population[i].randomize();
    }
// main simulation loop
    while (true){
      ++gen;
      for (int indiv=0;indiv<popSize;indiv++){
        System.out.println("----------- Gen " + gen + " Indiv " + indiv +
                           " -----------");
// assess...(FIXED DIAMETER!)
        population[indiv].load(e); // re-load
// set diameters
        for (double x=0;x<4;x+=e.getDx()){
          e.setDiameter(fuelIn+x,0);
          e.setDiameter(altIn+x,0);
          e.setDiameter(speedIn+x,0);
        }

        double s1=assess(e,300);
        score[indiv]=s1;
        double score2=0;
        writeIndiv(gen,indiv,population[indiv],score[indiv],score2,pw);
        System.out.printf("\n%d:%d(%.2f)\n",gen,indiv,score[indiv]);
      } // end of evaluation of all individuals
      double bestScore=rank(); // rank and merge
      System.out.println("\nGen " + gen + " Best score=" + bestScore + "\n"); // rank all individuals
      breed(.10); // breed them, with mutation rate of 5%
    } // and repeat forever
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      writeStatus(e,pw); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 2: // intensity
      if (head) disp.setIntensity(arg.getSliderPos());
      break;
    }
  }

//
// assessment test: single run
//
  double assess(EEXIST e,double initAlt) // assess this individual
  {

    //initAlt=200+100*Math.random(); // 100-300
// prepare connection to server
    toServer.println("I " + initFuel + " " + initSpeed +
                     " " + initAlt + " " + grav +
                     " " + thrust + " " + dt);

    toServer.println("r"); // reset the sim

    toServer.println("d");toServer.flush(); // request (initial) data
    String temp=fromServer.nextLine(); // data
    String fields[]=temp.split(","); // [1]=done, [3]=fuel, [5]=speed, [7]=alt

    boolean done=(fields[1].equals("true"));
    double fuel=Double.parseDouble(fields[3]);
    double speed=Double.parseDouble(fields[5]);
    double alt=Double.parseDouble(fields[7]);

// Inject these params into (0-diameter) regions
    load(fuel,speed,alt);
    
// then inject START signal
    inject(go,go+4,20.); // this is the "go" signal
    double minAlt=alt;

    while (!done){

// step the system
      e.step(1);

// read the thrust output
      boolean thrust=detect(thrustOut,thrustOut+4,10.);

// set thrust in sim
      toServer.println((thrust)?"T":"t");toServer.flush();

// step sim
      toServer.println("s");toServer.flush();

// read data
      toServer.println("d");toServer.flush();
      temp=fromServer.nextLine(); // data
      fields=temp.split(","); // [1]=done, [3]=fuel, [5]=speed, [7]=alt
      done=(fields[1].equals("true"));
      fuel=Double.parseDouble(fields[3]);
      speed=Double.parseDouble(fields[5]);
      alt=Double.parseDouble(fields[7]);

      if (alt < minAlt) minAlt=alt; // record how close we've gotten to the surface
// if we've gone too high, we're going to crash...
      if (alt > 1000){
        System.out.println("Too high: min alt=" + minAlt);
        System.out.println("last stat: \nFuel=" + fuel + " alt=" + alt + " speed=" + speed);
        //return(0);
        return(.1/Math.abs(minAlt)); // points for approaching surface
      }
      //System.out.println("speed=" + speed + " alt=" + alt);

// update EEXIST regions with new data
      load(fuel,speed,alt);
    }

    double score=Math.abs(1./speed);

// %%% This is speed when alt<0; is that a problem?

    System.out.println("Landing stat: \nFuel=" + fuel + " alt=" + alt + " speed=" + speed);
    System.out.print("Score=" + score);
    return(score);
  }

// inform EEXIST of current status
  public void load(double fuel, double speed, double alt)
  {
// fuel run from 20-0, so inject fuel*1.5 (0->30)
    inject(fuelIn,fuelIn+4,trim(.1*fuel*1.5));
// altitude roughly 0-250, so inject alt/6
    inject(altIn,altIn+4,trim(.1*alt/6.));
// speed roughly 10 (rising) to -30 (fall) so inject speed+30
    inject(speedIn,speedIn+4,trim(.1*(speed+30.)));
  }

  public double trim(double x)
  {
    if (x<0) x=0.;
    if (x>40) x=40.;
    return(x);
  }

// input and output routines
  public void inject(double start, double end, double level) // inject chemicals
  {
    for (double x=start;x<end;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,level);
      e.setTube(x,EEXIST.DST,level);
    }
  }

// read a region and check average vs. threshold
  public boolean detect(double start, double end, double threshold)
  {
    double sum=0;
    for (double x=start;x<end;x+=e.getDx()){
      sum=sum+e.getTube(x,EEXIST.SRC)+e.getTube(x,EEXIST.DST);
    }
    return(sum >= threshold*(end-start)/e.getDx()); // avg >= threshold
  }

  double rank() // sort population[] by score[]
  {
    for (int loop=0;loop<popSize;loop++){ // good old bubble sort!
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (score[i+1] > score[i]){ // swap
          double temp=score[i+1];score[i+1]=score[i];score[i]=temp;
          Genome temp2=population[i+1];population[i+1]=population[i];population[i]=temp2;
        }
      }
    }

// copy best elements of population to best[] array
    double bestScore=0;
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i].copy();
      if (score[i] > bestScore) bestScore=score[i];
    }
    return(bestScore);
  }
  
  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy();
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].merge(best[(int)(Math.random()*survivorSize)]);
        population[i].mutate(mutateRate); // random variation
      }
    }
  }

  boolean firstWS=true;
  void writeStatus(EEXIST e,PrintWriter pw)
  {
    if (firstWS){
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+
              e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
              ","+e.numTubes());
    pw.flush();
  }

  boolean firstWI=true;
  void writeIndiv(int gen,int indiv,Genome genome,
                  double score,double score2,PrintWriter pw)
  {
    if (firstWI){
      pw.println("*I,gen,ind,score,score2,len[i],var[i],init[i],initS[i],"+
                 "initD[i],delta[i],deltaS[i],deltaD[i]");
      firstWI=false;
    }
    pw.print("I,"+gen+","+indiv+","+score+","+score2);
    for (int i=0;i<Genome.GenomeSize;i++){
      pw.print(","+genome.gene[i].len+","+genome.gene[i].variation+","+
               genome.gene[i].init+","+genome.gene[i].initS+","+genome.gene[i].initD+","+
               genome.gene[i].delta+","+genome.gene[i].deltaS+","+genome.gene[i].deltaD);
    }
    pw.println();pw.flush();
  }
}
