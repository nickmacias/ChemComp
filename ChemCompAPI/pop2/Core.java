import com.cellmatrix.chemcomp.API.v1.*;
import java.io.PrintWriter;
import java.util.LinkedList;

public class Core extends Thread{
  int popSize=8; // used to set array size
  int survivorSize=3; // # of survivors per gen
  double mutateRate=.35; // mutation rate for breeding

  int stepsPerAssess=1000; // # of steps used to score a conversartion

  EEXIST e; // generic

// displays
  Display firstDisplay,secondDisplay;

  boolean doMore=true;
  boolean pause1=false,pause2=false; // set to suspend step() calls
  boolean doEvolve=true; // set for automatic evolution

  int delayTime=1; // delay in mSec
  int id=1;

  boolean autoRun=false; // when set, move on when variance is low
  
  PrintWriter pw;

  Individual[] population;
  Individual[] best=new Individual[survivorSize]; // save best members here

  public void run()
  {
    try{
      pw=new PrintWriter("raw.txt");
    } catch(Exception e){
      System.out.println("Can't write to output file ("+e+")- goodbye");
      return;
    }

    e=new EEXIST();
    firstDisplay=new Display(e);
    firstDisplay.cursor(4);firstDisplay.cursor(20);firstDisplay.cursor(24);
    secondDisplay=new Display(e);
    secondDisplay.cursor(4);secondDisplay.cursor(20);secondDisplay.cursor(24);
    firstDisplay.setLocation(10,50);
    firstDisplay.setVisible(true);
    secondDisplay.setLocation(10,400);
    secondDisplay.setVisible(true);
    firstDisplay.showGF(true);
    secondDisplay.showGF(true); // show detailed graphs

    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,1,1,50000,1,"Delay","cpHandler");
    cp.connect(this, ControlPanel.Button,0,10,"Next","cpHandler"); // click to move on
    cp.connect(this, ControlPanel.CheckBox,0,20,"Auto Run","cpHandler"); // click to move on
    cp.connect(this, ControlPanel.CheckBox,1,21,"Pause Indiv 1","cpHandler");
    cp.connect(this, ControlPanel.CheckBox,2,22,"Pause Indiv 2","cpHandler"); // pause steps
    cp.connect(this, ControlPanel.CheckBox,3,23,"Evolve Off","cpHandler"); // Check for no auto-evolve

    writeStatus(e,pw); // write relevant info about EEXIST
    int numTubes=e.numTubes(); // save this

    int gen=0; // track generations

// make an initial population
    population=new Individual[popSize];
    for (int indivNum=0;indivNum<popSize;indivNum++){ 
      EEXIST tempe=new EEXIST();

// randomize some tubes
      int limit=(int)(100.*Math.random());
      for (int count=-10;count<limit;count++){
        tempe.setTube(40*Math.random(),EEXIST.SRC,40*Math.random());
        tempe.setTube(40*Math.random(),EEXIST.DST,40*Math.random());
      }

      zeroDiam(tempe); // set diameter=0 in input region

      Individual tempInd=new Individual(++id,tempe,new Genome(numTubes));
      tempInd.g.randomize();
      tempInd.g.quietLoad(tempInd.e); // seed the EEXIST of this individual
      zeroDiam(tempInd.e);
      population[indivNum]=tempInd; // track these in a list
    }

// main simulation loop
    while (true){
// run one indiv against the entire population
      for (int ind1=0;ind1<popSize;ind1++){
        EEXIST first=population[ind1].e;
        population[ind1].score=0; // running sum of variations. Add these across all convos!
        firstDisplay.disconnect();
        first.saveDisplay(firstDisplay);
        firstDisplay.setGFTitle(ind1+" ");

        for (int ind2=0;ind2<popSize;ind2++){
          if (ind2==ind1) continue; // don't talk to yourself!
          //System.out.println("Connecting " + ind1 + " and " + ind2);
          EEXIST second=population[ind2].e;
          secondDisplay.disconnect();
          second.saveDisplay(secondDisplay);
          secondDisplay.setGFTitle(ind2+" ");

// let them talk
          double var1Quiet=0,var2Quiet=0;
          double var1Speak=0,var2Speak=0; // variations in chemical levels
          doMore=true;
          int lowCount=0; // when we get too many low counts, time to move on
          int stepCount=0; // # of steps!
          int lowCountLimit=100;
          boolean speak=false; // true means connect
          while (doMore){
            if (stepCount%100 == 0) speak=!speak; // toggle every 100 steps

            if (speak){
// set up a convo between the two individuals
              connect(first,0,4,second,20,24);
              connect(second,0,4,first,20,24); // copy from one indiv to another
              var1Speak+=monitorAndStep(first,1,!pause1);
              var2Speak+=monitorAndStep(second,1,!pause2);
            } else {
              var1Quiet+=monitorAndStep(first,1,!pause1);
              var2Quiet+=monitorAndStep(second,1,!pause2);
            }

            //System.out.println("Var=" + var1 + ":" + var2);
            if (doEvolve){
// see if we've run 1000 steps
              if (++stepCount > stepsPerAssess) doMore=false; // kick out after 500 steps
// also, kick out if this doesn't seem interesting
              if ((var1Speak < 500) || (var2Quiet > 250)) ++lowCount;
              if (lowCount > lowCountLimit){ // give up here
                reGen(ind1); // reset this individual
                stepCount=lowCount=0; // reset
                var1Speak=var2Speak=var1Quiet=var2Quiet=0;
              }
            } else {
// this case needs to be fixed...
              if (var1Speak+var2Speak < 1000) ++lowCount;
              if ((lowCount > lowCountLimit) && autoRun) doMore=false;
              myDelay(delayTime); // wait a bit
            }
          } // done with this pair
          if (var1Quiet+var2Quiet==0) var1Quiet=1; // avoid edges
// want both speaks large, both quiets small
          //population[ind1].score+=(var1Speak*var2Speak)/(var1Quiet+var2Quiet);
          population[ind1].score+=(var1Speak)/(var1Quiet); // just rate 1st indiv

          System.out.println(ind1 + "+" + ind2 + " ==> " + population[ind1].score);
        } // end of comparison of ind1 with all other inds
// write genome and data
        if (doEvolve){
          writeIndiv(gen,ind1,population[ind1].g,population[ind1].score,0,pw);
        }

      } // end of evaluation of all individuals
// find best individuals, breed, mutate, re-populate
      double bestScore=rank(); // sort population array by scores
      System.out.println("Gen " + gen + " best score=" + bestScore);
      breed(mutateRate);

      System.out.println("\n\nAgain...");
      ++gen;
    } // and repeat forever
  }

  void reGen(int ind) // shake up this individual
  {
      EEXIST tempe=population[ind].e;

      tempe.clearAllTubes();

// randomize some tubes
      int limit=(int)(100.*Math.random());
      for (int count=-10;count<limit;count++){
        tempe.setTube(40*Math.random(),EEXIST.SRC,40*Math.random());
        tempe.setTube(40*Math.random(),EEXIST.DST,40*Math.random());
      }

// new genome too...
      population[ind].g.randomize();
      population[ind].g.quietLoad(tempe); // seed the EEXIST of this individual
      zeroDiam(tempe);
  }

  void zeroDiam(EEXIST tempe)
  {
    for (double x=20;x<24;x+=e.getDx()){
      tempe.setDiameter(x,0); // don't allow any flow into or out of this region
      tempe.setTube(x,EEXIST.SRC,0);
      tempe.setTube(x,EEXIST.DST,0);
    }
  }

  double monitorAndStep(EEXIST e, int numSteps, boolean doStep)
  {
    double var=0;
    int numTubes=e.numTubes();
    double[][] before=new double[numTubes][2];
    double[][] after=new double[numTubes][2];
    before=e.getAllTubes();
    if (doStep) e.step(numSteps);
    after=e.getAllTubes();
    for (int i=0;i<numTubes;i++){
      var+=Math.abs(after[i][EEXIST.SRC] - before[i][EEXIST.SRC]);
      var+=Math.abs(after[i][EEXIST.DST] - before[i][EEXIST.DST]);
    }
    return(var);
  }

// copy chemicals from first to second
  public void connect(EEXIST first, double firstStart, double firstEnd,
                      EEXIST second, double secondStart, double secondEnd)
  {
    for (double from=firstStart,to=secondStart;
         from<firstEnd;
         from+=first.getDx(),to+=second.getDx()){
      second.setTube(to,EEXIST.SRC,first.getTube(from,EEXIST.SRC));
      second.setTube(to,EEXIST.DST,first.getTube(from,EEXIST.DST));
      first.setTube(to,EEXIST.SRC,second.getTube(from,EEXIST.SRC));
      first.setTube(to,EEXIST.DST,second.getTube(from,EEXIST.DST));
    }
  }

  public void myDelay(int t)
  {
    try{Thread.sleep(t);} catch(Exception e){}
    return;
  }

  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
      case 1: // delay
        delayTime=arg.getSliderPos();
        System.out.println("\nDelay set to " + delayTime);
        break;
      case 10: // next
        doMore=false;
        break;
      case 20: // Auto-Run
        autoRun=arg.getCheckState();
        break;
      case 21: // pause indiv 1
        pause1=arg.getCheckState();
        break;
      case 22: // pause indiv 2
        pause2=arg.getCheckState();
        break;
      case 23: // evolve
        doEvolve=!arg.getCheckState();
        break;
    }
  }

  double rank() // sort population[] by scores
  {
// bubble sort...
    for (int loop=0;loop<popSize;loop++){
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (population[i+1].score > population[i].score){ // swap
          Individual temp2=population[i+1];population[i+1]=population[i];population[i]=temp2;
        }
      }
    }

// copy best elements of population to best[] array
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i];
    }
    return(best[0].score);
  }
  
  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i];
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].g.merge(best[(int)(Math.random()*survivorSize)].g);
        zeroDiam(population[i].e); // force diameter to 0 in input region
// TODO set new ID
// TODO track generation #
// TODO reset score?
      }
      population[i].g.mutate(mutateRate); // random variation of genome
    }
  }

  boolean firstWS=true;
  void writeStatus(EEXIST e,PrintWriter pw)
  {
    if (firstWS){
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+
              e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
              ","+e.numTubes());
    pw.flush();
  }

  boolean firstWI=true;
  void writeIndiv(int gen,int indiv,Genome genome,
                  double score,double score2,PrintWriter pw)
  {
    if (firstWI){
      pw.println("*I,gen,ind,score,score2,len[i],var[i],init[i],initS[i],"+
                 "initD[i],delta[i],deltaS[i],deltaD[i]");
      firstWI=false;
    }
    pw.print("I,"+gen+","+indiv+","+score+","+score2);
    for (int i=0;i<Genome.GenomeSize;i++){
      pw.print(","+genome.gene[i].len+","+genome.gene[i].variation+","+
               genome.gene[i].init+","+genome.gene[i].initS+","+genome.gene[i].initD+","+
               genome.gene[i].delta+","+genome.gene[i].deltaS+","+genome.gene[i].deltaD);
    }
    pw.println();pw.flush();
  }
}
