import com.cellmatrix.chemcomp.API.v1.*;
//
// main execution thread. started by analyzecontrol, and used
// to run the simulation

public class AnalyzeCore extends Thread{
  int stepLimit=0;
  boolean running;
  EEXIST e;Display disp;
  boolean testing=false;
  int testNum=0; // specified by AnalyzeCore
  int delay=0;
  AnalyzeControl parent;

// save parent so we can read delay
  public void saveMe(AnalyzeControl parent)
  {
    this.parent=parent;
  }

  public AnalyzeCore(EEXIST e, Display disp)
  {
    this.e=e;this.disp=disp;
  }

  public void testing(boolean testing, int testNum)
  {
    this.testing=testing;
    this.testNum=testNum;
  }

  public void halt() // allows halt to be requested
  {
    running=false;
    testing=false; // no output monitor
  }
  
  public void run()
  {
    int steps=0; // # of steps we've done
    running=true; // clear to tell thread to exit

// test-related vars
    boolean level=false; // input starts LOW
    int halfPeriod=(testNum==0)?4:8; // half a period

// main simulation loop
    while (running){

// delay as indicated by user
      delay=parent.getDelay();
      if (delay != 0) myDelay(delay);

      if (!testing){ // just doing a normal run command...
        e.step(1);
        if ((stepLimit!=0) && (++steps==stepLimit)) running=false;
        continue;
      }

// here we're setting the input during a test

  // input a is placed in [0,4]; output is read from [24,28]
      //if (steps%halfPeriod==0) level=!level;
      if (testNum==0) level=false;
      level=true; // for now %%%

      if (steps==0){
// set input now
        for (double x=0;x<4;x+=e.getDx()){
          e.setTube(x,EEXIST.SRC,level?20:5);
          e.setTube(x,EEXIST.DST,level?20:5);
        }
      }
  
      e.step(1); // advance sim

      if (steps < 64) System.out.print(steps + " ");
      if (steps==64) System.out.println();

// display test results if requested
      if (steps>=64){
        System.out.printf("%4d:",steps);
        for (double x=24;x<28;x+=e.getDx()){
          double sum=e.getTube(x, EEXIST.SRC)+e.getTube(x, EEXIST.DST);
          if (sum > 15) System.out.print("1");
          else if (sum < 10) System.out.print("0");
          else System.out.print("?");
        } // this output evaluated
        System.out.println();
      } // end of this timestep's output analysis

      if (steps==127){
        System.out.println("----------------------------------------\n");
      }

// finished?
      if ((stepLimit!=0) && (++steps==stepLimit)) running=false;
    } // end of test mode
  } // thread exits here

  public void setLimit(int stepLimit)
  {
    this.stepLimit=stepLimit;
  }

  public void myDelay(int t)
  {
    try{Thread.sleep(t);} catch(Exception e){}
    return;
  }

}
