import com.cellmatrix.chemcomp.API.v1.*;
//
// main execution thread. started by analyzecontrol, and used
// to run the simulation

public class AnalyzeCore extends Thread{
  int stepLimit=0;
  boolean running;
  EEXIST e;Display disp;
  Game game;

  boolean testing=false;
  //int testNum=0; // specified by AnalyzeCore
  int delay=0;
  AnalyzeControl parent;

// save parent so we can read delay
  public void saveMe(AnalyzeControl parent)
  {
    this.parent=parent;
  }

  public void saveGame(Game game)
  {
    this.game=game;
  }

  public AnalyzeCore(EEXIST e, Display disp)
  {
    this.e=e;this.disp=disp;
  }

  public void testing(boolean testing)
  {
    this.testing=testing;
  }

  public void halt() // allows halt to be requested
  {
    running=false;
    testing=false; // no output monitor
  }
  
  public void run()
  {
    int steps=0; // # of steps we've done
    running=true; // clear to tell thread to exit

// main simulation loop
    while (running){

// delay as indicated by user
      delay=parent.getDelay();
      if (delay != 0) myDelay(delay);

      if (!testing){ // just doing a normal run command...
        e.step(1);
        if ((stepLimit!=0) && (++steps==stepLimit)) running=false;
        continue;
      }

// here we're playing a game
      e.step(1); // advance sim
      int move=game.findMove(); // see if EEXIST has moved
      if (move != -1){ // yes!
        game.board[move]=game.EEXISTMARK;
        game.pprint();
        running=false;

// game over?
        char status=game.gameOver();
        if (status==game.EEXISTMARK){ // EEXIST WINS!
          System.out.println("EEXIST wins");
          parent.insideGamePlay=false; // normal input again
          return;
        }
        if (status=='-'){ // draw
          System.out.println("Game is a draw");
          parent.insideGamePlay=false; // normal input
          return;
        }
        return; // else exit this thread but let the game continue...
      } // end of EEXIST's move

// need to keep stepping, waiting for EEXIST to move
// Out of timesteps?
      if ((stepLimit!=0) && (++steps==stepLimit)) running=false;
    } // end of test mode

// stepped 100 times and never detected a move from EEXIST - forfeit
    System.out.println("EEXIST has forfeited");
    parent.insideGamePlay=false; // normal input
  } // thread exits here

  public void setLimit(int stepLimit)
  {
    this.stepLimit=stepLimit;
  }

  public void myDelay(int t)
  {
    try{Thread.sleep(t);} catch(Exception e){}
    return;
  }

}
