import com.cellmatrix.chemcomp.API.v1.*;

/*
 * Tic Tac Toe machinery
 */
class Game{
  char board[]=new char[9]; // 'X' (EEXIST) 'O' (opponent) ' ' (unused)
/*
 *   0|1|2
 *   -+-+-
 *   3|4|5
 *   -+-+-
 *   6|7|8
 *
 */

  EEXIST e; // save this
  int INJECT_1_LEVEL=35; // how much we inject to input a 1
  //int DETECT_1_LEVEL=15; // required threshold to detect a 1
  int DETECT_1_LEVEL=10; // required threshold to detect a 1
  int MAXTRIES=250; // max # of steps we'll wait for a move from EEXIST
  char EEXISTMARK='X';
  char OPPONENTMARK='O';

  int WINSCORE=2;
  int DRAWSCORE=0;
  int LOSSSCORE=-5;
  int BADMOVESCORE=-1; // abort

// pass EEXIST to constructor so we can process inputs/outputs
  public Game(EEXIST e)
  {
    this.e=e;
  }

// main game playing loop
// only useful during evolution. For analysis, this should be broken up
// and placed inside AnalyzeCore to allow delayed stepping etc.
//
// oc.moves[i] shows where the opponent will move on turn i
// oc.turnNum will show the last turn# made by the opponent
  public int playGame(int gameNum,OpponentControl oc) // return score
  {
// assume individual has already been loaded into the system
    init(); // clear the gameboard and zero the system's chemicals
    inject(36,40,INJECT_1_LEVEL); // start the system
    print(); // show empty board (upper-left)

    boolean firstMove=true;

// wait up to MAXTRIES steps for a move
    int step=0;
    boolean firstEEMove=true; // clear after opponent moves
    oc.start();
    while (step++ < MAXTRIES){
      e.step(1);
      int move=findMove(); // detect EEXIST's turn
      if (move==-1) continue; // no move detected yet
      if (firstEEMove && (move != 0) && (move != 2) &&
                         (move != 6) & (move != 8)){ // not in a corner
        return(-99);
      } // don't test this individual any further...
      firstEEMove=false;

/*****************
// see if "move" is a good move
      if (!goodMove(move)){ // no it's not...was there a good option?
        for (int i=0;i<9;i++){
          if (legal(i)){
            if (goodMove(i)){ // there was a good move we could have made
              //System.out.println("EEXIST wanted to move at " + move + " but " + i + " would have been better. Forfeiting.");
              return(BADMOVESCORE);
            }
          }
        } // no better move available. Let this move stand
      } // "move" is the move!
****************/

// the system has made a move!
      step=0; // reset counter
      board[move]=EEXISTMARK; // record the move
      print();

// see if the game is over
      char status=gameOver();
      if (status==EEXISTMARK) return(WINSCORE); // we won!
      if (status=='-') return(DRAWSCORE); // this was a draw
      if (status==OPPONENTMARK) return(LOSSSCORE); // lost the game :(

// game is still in-play. Get opponent's move
      //move=opponentMove(gameNum,firstMove); // let the opponent make a move
      move=oc.nextMove(this);
      firstMove=false;
      board[move]=OPPONENTMARK;
      print();
      inject(move*4,move*4+4,INJECT_1_LEVEL); // inject this move into the system

// game over?
      status=gameOver();
      if (status=='-') return(DRAWSCORE); // draw
      if (status==OPPONENTMARK) return(LOSSSCORE); // opponent won

// No: game continues
    }

// out of time waiting for system to move
    return(-1); // no move found within required time: forfeit
  } // end of playGame() method

// initialize the game and EEXIST
  public void init()
  {
    for (int i=0;i<9;i++) board[i]=' ';
    e.clearAllTubes(); // return system to initial state
  }

// see if the system has made a move and return index (or -1 for no move yet)
  public int findMove()
  {
    for (int i=0;i<9;i++){
      if (legal(i)){ // this square is available
        if (detect(4*i,4*i+4,DETECT_1_LEVEL)){ // output detected here
          return(i);
        }
      }
    } // no move found
    return(-1);
  }

// see if move is legal
  public boolean legal(int index)
  {
    return(board[index]==' ');
  }

// input and output routines
  public void inject(double start, double end, double level) // inject chemicals
  {
    for (double x=start;x<end;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,level);
      e.setTube(x,EEXIST.DST,level);
    }
  }

// read a region and check average vs. threshold
  public boolean detect(double start, double end, double threshold)
  {
    double sum=0;
    for (double x=start;x<end;x+=e.getDx()){
      sum=sum+e.getTube(x,EEXIST.SRC)+e.getTube(x,EEXIST.DST);
    }
    return(sum >= threshold*(end-start)/e.getDx()); // avg >= threshold
  }

// was this a "good" move (meaning, if possible, it wins or blocks)?
  boolean goodMove(int m) // let's find out
  {
// would m win the game for *us*?
    if (m==blockCheck(0,1,2)) return(true); // (block for opp is win for us)
    if (m==blockCheck(3,4,5)) return(true);
    if (m==blockCheck(6,7,8)) return(true);
    if (m==blockCheck(0,3,6)) return(true);
    if (m==blockCheck(1,4,7)) return(true);
    if (m==blockCheck(2,5,8)) return(true);
    if (m==blockCheck(0,4,8)) return(true);
    if (m==blockCheck(2,4,6)) return(true);

// would m block the opponent from winning?
    if (m==winCheck(0,1,2)) return(true);
    if (m==winCheck(3,4,5)) return(true);
    if (m==winCheck(6,7,8)) return(true);
    if (m==winCheck(0,3,6)) return(true);
    if (m==winCheck(1,4,7)) return(true);
    if (m==winCheck(2,5,8)) return(true);
    if (m==winCheck(0,4,8)) return(true);
    if (m==winCheck(2,4,6)) return(true);

    return(false);
  }

// pick best move for opponent (win or block or random)
  public int opponentMove(int gameNum, boolean firstMove)
  {
    int m;

// if this is the first time the opponent is moving, pick a square
// based on the game number
    if (firstMove){
      int ind=0;
      for (int i=0;i<9;i++){
        if (board[i]==' '){ // available; is this the nth square?
          if (ind == gameNum%8) return(i);
          ++ind; // runs from 0-7
        }
      } // should have exited
      System.out.println("Help! Something went wrong!");
      System.exit(0);
    }

// can we win this turn?
    if (-1 != (m=winCheck(0,1,2))) return(m);
    if (-1 != (m=winCheck(3,4,5))) return(m);
    if (-1 != (m=winCheck(6,7,8))) return(m);
    if (-1 != (m=winCheck(0,3,6))) return(m);
    if (-1 != (m=winCheck(1,4,7))) return(m);
    if (-1 != (m=winCheck(2,5,8))) return(m);
    if (-1 != (m=winCheck(0,4,8))) return(m);
    if (-1 != (m=winCheck(2,4,6))) return(m);

// can we block?
    if (-1 != (m=blockCheck(0,1,2))) return(m);
    if (-1 != (m=blockCheck(3,4,5))) return(m);
    if (-1 != (m=blockCheck(6,7,8))) return(m);
    if (-1 != (m=blockCheck(0,3,6))) return(m);
    if (-1 != (m=blockCheck(1,4,7))) return(m);
    if (-1 != (m=blockCheck(2,5,8))) return(m);
    if (-1 != (m=blockCheck(0,4,8))) return(m);
    if (-1 != (m=blockCheck(2,4,6))) return(m);

// nope - do a random move
    while (true){
      m=(int)(9*Math.random()); // random move
      if (board[m]==' ') return(m);
    }
  }

// can we win in this line?
  public int winCheck(int a, int b, int c)
  {
    if ((board[a]==board[b]) &&
        (board[b]==OPPONENTMARK) &&
        (board[c]==' ')) return(c);
    if ((board[a]==board[c]) &&
        (board[c]==OPPONENTMARK) &&
        (board[b]==' ')) return(b);
    if ((board[c]==board[b]) &&
        (board[b]==OPPONENTMARK) &&
        (board[a]==' ')) return(a);
    return(-1); // no win possible
  }

// can we block in this line?
  public int blockCheck(int a, int b, int c)
  {
    if ((board[a]==board[b]) &&
        (board[b]==EEXISTMARK) &&
        (board[c]==' ')) return(c);
    if ((board[a]==board[c]) &&
        (board[c]==EEXISTMARK) &&
        (board[b]==' ')) return(b);
    if ((board[c]==board[b]) &&
        (board[b]==EEXISTMARK) &&
        (board[a]==' ')) return(a);
    return(-1); // no block possible
  }

// is the game over?
  public char gameOver() // return winning mark or ' ' for no winner
                        // return '-' for a draw
  {
    char c;

// see if someone has their mark in a row
    if (' ' != (c=check(0,1,2))) return(c);
    if (' ' != (c=check(3,4,5))) return(c);
    if (' ' != (c=check(6,7,8))) return(c);
    if (' ' != (c=check(0,3,6))) return(c);
    if (' ' != (c=check(1,4,7))) return(c);
    if (' ' != (c=check(2,5,8))) return(c);
    if (' ' != (c=check(0,4,8))) return(c);
    if (' ' != (c=check(2,4,6))) return(c);

// no decisive winner. Is the board full?
    if (draw()) return('-'); // no squares left

// else the game is still going
    return(' ');
  }

// see if this is a winning row. Return mark if so, else ' '
  public char check(int m1, int m2, int m3)
  {
    if ((board[m1]==board[m2]) &&
        (board[m2]==board[m3])) return(board[m1]);
    return(' '); // this means row is empty or marks differ
  }

  public boolean draw() // is the board full?
  {
    for (int i=0;i<9;i++){
      if (board[i] == ' '){
        return(false);
      }
    }
    return(true); // no squares are free
  }

  public void print() // display the board
  {
    System.out.println("\n\n");
    //System.out.print((char)27 + "[2H");
    System.out.println(board[0]+"|"+board[1]+"|"+board[2]);
    System.out.println("-+-+-");
    System.out.println(board[3]+"|"+board[4]+"|"+board[5]);
    System.out.println("-+-+-");
    System.out.println(board[6]+"|"+board[7]+"|"+board[8]);
  }

  public void pprint() // display the board with move markers
  {
    char b[]=new char[9];
    for (int i=0;i<9;i++){
      if (board[i] != ' ') b[i]=board[i];
      else b[i]="abcdefghi".charAt(i); // show user how to select that square
    }

    System.out.print((char)27 + "[2H");
    System.out.println(b[0]+"|"+b[1]+"|"+b[2]);
    System.out.println("-+-+-");
    System.out.println(b[3]+"|"+b[4]+"|"+b[5]);
    System.out.println("-+-+-");
    System.out.println(b[6]+"|"+b[7]+"|"+b[8]);
  }

}

class OpponentControl
{
  int[] moves=new int[4];
  int turnNum=-1;

  public void init() // full init
  {
    turnNum=-1;
    moves[0]=moves[1]=moves[2]=moves[3]=0;
  }

  public void start() // reset turn counter
  {
    turnNum=-1;
  }

  public int nextMove(Game g)
  {
    System.out.println("turnNum=" + turnNum +
                       " moves=[" + moves[0]+","+moves[1]+","+moves[2]+","+moves[3]+"]");
    int m=moves[++turnNum];
// find mth open square
    int ret=0;
    while (m >= 0){
      if (g.legal(ret)){
        --m;
        if (m == -1) return(ret);
      }
      ++ret;
      if (ret > 8){
        System.out.println("Something went wrong advancing ret in nextMove()");
        System.exit(1);
      }
    }
    System.out.println("Something went wrong looking for open square in nextMove()");
    System.exit(1);
    return(-1); // make compiler happy
  }

  public boolean advance() // advance moves[] to the next state. Return false when done
  {
    if (turnNum==0){ // shouldn't happen...means game ended after 1 move
                    // so moves[1] never got used (and thus never will)
                   // so we'll set the remaining moves[] values to cause a rollover
      moves[1]=5;moves[2]=3;moves[3]=1;
    } else if (turnNum==1){
      moves[2]=3;moves[3]=1;
    } else if (turnNum==2){
      moves[3]=1;
    }

// now add 1
    if (2==++moves[3]){
      moves[3]=0;
      if (4==++moves[2]){
        moves[2]=0;
        if (6==++moves[1]){
          moves[1]=0;
          if (8==++moves[0]){
            moves[0]=0;return(false); // done!
          }
        }
      }
    }
    return(true);
  }
}
