import com.cellmatrix.chemcomp.API.v1.*;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class Core extends Thread{
  EEXIST e;
  Display disp;

  int popSize=25; // total # of individuals in the population
  int survivorSize=5; // # of individuals to keep after each generation

  double mutRate=.10;

  PrintWriter pw;

  Genome[] population=new Genome[1000];
  Genome[] best=new Genome[1000]; // save best members here
  double[] score=new double[1000]; // save scores

  public void run()
  {
    try{
      pw=new PrintWriter("raw.txt");
    } catch(Exception e){
      System.out.println("Can't write to output file ("+e+")- goodbye");
      return;
    }
    e=new EEXIST(0,60,0.0625); // main EEXIST object
    e.setKarma(10);
    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display
    disp.cursor(16);
    disp.cursor(20);

    disp.cursor(36);
    disp.cursor(40);
    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,(int)(10*e.getKarma()),
                     0,400,1,"Karma","cpHandler");
    cp.connect(this, ControlPanel.Slider,1,popSize,
                     0,100,2,"Pop size","cpHandler");
    cp.connect(this, ControlPanel.Slider,2,(int)(mutRate*100),
                     0,100,3,"Mutation rate","cpHandler");

    writeStatus(e,pw); // write relevant info about EEXIST
    int gen=0; // track generations

// make an initial population
    for (int i=0;i<1000;i++){ 
      population[i]=new Genome(e.numTubes());
      population[i].randomize();
    }

// main simulation loop
    while (true){
      ++gen;
      for (int indiv=0;indiv<popSize;indiv++){
        score[indiv]=0; // initial score

        population[indiv].load(e); // load this individual into the system
        double success=assess(e); // run a test, tally the score
        score[indiv]+=success;

        double score2=0;
        writeIndiv(gen,indiv,population[indiv],score[indiv],score2,pw);
        System.out.println(gen + ":" + indiv + ":" +
                          score[indiv]);

      } // end of evaluation of all individuals
      double rank1=rank();
      System.out.println("\nGen " + gen + " Best score=" + rank1 + "\n"); // rank all individuals
      breed(mutRate); // breed them, with mutation rate of 15%
    } // and repeat forever
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      writeStatus(e,pw); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 2: // population size
      popSize=arg.getSliderPos();
      System.out.println("Population size: " + popSize);
      break;
    case 3: // mutation rate
      mutRate=arg.getSliderPos()/100.;
      System.out.println("Mutation rate: " + arg.getSliderPos() + "%");
      break;
    }
  }

  double assess(EEXIST e) // assess this individual
  {
    double score=0; // # of correct readings

// load inputs
// downweight sex, since this alone gets you above 80%
    load(e,0,60,0.); // empty space
    load(e,0,4,8);
    load(e,4,8,28);
    load(e,8,12,18); // randomish fodder

    load(e,16,20,30); // start here

    double s1=0,s2=0;

// step the EEXIST
    for (int i=0;i<1024;i++){ // want first region high, second low
      boolean first=(i%32 < 16); // 1st cycle? or 2nd
      double r1=readAvg(e,16,20);
      double r2=readAvg(e,36,40);
      if ( first && (r1 > 20) && (r2 < 20)) ++s1;
      if (!first && (r1 < 20) && (r2 > 20)) ++s2; // max 16 each
      e.step(1);
    } // max score=1024

    return(s1*s2); // max 256
  }

  double readAvg(EEXIST e, double start, double end)
  {
    double sum=0;
    for (double x=start;x<end;x+=e.getDx()){
      sum+=e.getTube(x,EEXIST.SRC)+e.getTube(x,EEXIST.DST);
    }
    sum=sum/((end-start)/e.getDx()); // average value
    return(sum);
  }

  void load(EEXIST e, double start, double end, double value)
  {
    if (value < 0) value=0;
    if (value > 60) value=60;

    for (double x=start;x<end;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,value);
      e.setTube(x,EEXIST.DST,value);
    }
  }

  double rank() // sort population[] by score[]
  {
    for (int loop=0;loop<popSize;loop++){ // good old bubble sort!
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (score[i+1] > score[i]){ // swap
          double temp=score[i+1];score[i+1]=score[i];score[i]=temp;
          Genome temp2=population[i+1].copy();population[i+1]=population[i].copy();population[i]=temp2.copy();
        }
      }
    }

// copy best elements of population to best[] array
    double bestScore=0;
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i].copy();
      bestScore+=score[i];
    }
    //return(bestScore);
    return(score[0]);
  }
  
  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy(); // preserve this member!
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].merge(best[(int)(Math.random()*survivorSize)]);
//System.out.println("Before mutate: " + population[i]);
        population[i].mutate(mutateRate); // random variation
//System.out.println("After mutate: " + population[i]);
      }
// don't mutate that best population...
      //population[i].mutate(mutateRate); // random variation
    }
  }

  boolean firstWS=true;
  void writeStatus(EEXIST e,PrintWriter pw)
  {
    if (firstWS){
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+
              e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
              ","+e.numTubes());
    pw.flush();
  }

  boolean firstWI=true;
  void writeIndiv(int gen,int indiv,Genome genome,
                  double score,double score2,PrintWriter pw)
  {
    if (firstWI){
      pw.println("*I,gen,ind,score,score2,len[i],var[i],init[i],initS[i],"+
                 "initD[i],delta[i],deltaS[i],deltaD[i]");
      firstWI=false;
    }
    pw.print("I,"+gen+","+indiv+","+score+","+score2);
    for (int i=0;i<Genome.GenomeSize;i++){
      pw.print(","+genome.gene[i].len+","+genome.gene[i].variation+","+
               genome.gene[i].init+","+genome.gene[i].initS+","+genome.gene[i].initD+","+
               genome.gene[i].delta+","+genome.gene[i].deltaS+","+genome.gene[i].deltaD);
    }
    pw.println();pw.flush();
  }
}
