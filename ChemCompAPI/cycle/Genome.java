import com.cellmatrix.chemcomp.API.v1.*;

/*
 * Genome of an organism
 * This is comprised of an array of genes, plus
 * a set of methods for manipulating the genome
 */
public class Genome {
  static int GenomeSize=15; // # of genes in the genome
  int numTubes;
  
  // TODO Use start/slope for initial generation of e.g. bias levels, but then save tube-by-tube
  
  Gene[] gene=new Gene[Genome.GenomeSize];
  
  // just construct each gene
  Genome(int numTubes)
  {
	this.numTubes=numTubes;

    for (int i=0;i<GenomeSize;i++){
      gene[i]=new Gene();
      gene[i].len=numTubes/GenomeSize;
    }
  }
  
  void load(EEXIST e) // load the genome into the machine
  {
	  int nextTube=0; // running index
	  
	  // zero-out the system
	  e.clearAllTubes();
//### Should we zero the biases?

	  for (int i=0;i<GenomeSize;i++){
		  Gene g=gene[i]; // next gene to load
		  double s=gene[i].initS;
		  double d=gene[i].initD;
		  double l=gene[i].init; // initial levels
		  
		  for (int j=0;j<g.len;j++){
	              e.setBias(nextTube, EEXIST.SRC , 0);
		      e.setBias(nextTube, EEXIST.DST , 0);
		      e.setTube((int)nextTube, EEXIST.SRC, 0);
		      e.setTube((int)nextTube, EEXIST.DST, 0);
		      e.setDiameter(nextTube,1.);

		      s+=gene[i].deltaS+gene[i].variation*Math.random();
		      d+=gene[i].deltaD+gene[i].variation*Math.random();
		      l+=gene[i].delta+gene[i].variation*Math.random();
		      if (s<0) s=0;
		      if (d<0) d=0;
		      /***/
		      if (s>60) s-=60;
		      if (s<0) s+=60;
		      if (d>60) d-=60;
		      if (d<0) d+=60;
		      if (l>5) l=5;
		      if (l<0) l=0;
//### Check addresses here make sure every location gets set...
		        e.setBias(nextTube, EEXIST.SRC, s);
		        e.setBias(nextTube, EEXIST.DST, d);
		      // end of loading this tube
		      ++nextTube;
		  } // end of gene
	  } // end of genome
  }
  
  Genome copy() // make a copy
  {
	  Genome temp=new Genome(numTubes);
	  for (int i=0;i<GenomeSize;i++){
		  temp.gene[i]=gene[i].copy();
	  }
	  return(temp);
  }
  
  void mutate(double amount) // amount says how likely a mutation is
  {
    double variation=2.5; // add random amount from -2.5 to +2.5

    double mutateBase=-variation,mutateAmount=variation*2;

    for (int i=0;i<GenomeSize;i++){
      if (Math.random() < amount) gene[i].delta+=mutateBase+
               mutateAmount*Math.random(); // e.g. +/- 2.5
      if (Math.random() < amount) gene[i].deltaD+=mutateBase+
               mutateAmount*Math.random();
      if (Math.random() < amount) gene[i].deltaS+=mutateBase+
               mutateAmount*Math.random();
      if (Math.random() < amount) gene[i].init+=mutateBase+
               mutateAmount*Math.random();
      if (Math.random() < amount) gene[i].initD+=mutateBase+
               mutateAmount*Math.random();
      if (Math.random() < amount) gene[i].initS+=mutateBase+
               mutateAmount*Math.random();
// we want the variation to remain 0!
//      if (Math.random() < amount) gene[i].variation+=mutateBase+
//               mutateAmount*Math.random();

      if (Math.random() < amount/5) gene[i].type=(int)(3.*Math.random()); // less likely!
    }
  }

  public String toString()
  {
    String out="";
    for (int i=0;i<GenomeSize;i++){
      out=out+String.format("%10f %10f %10f %10f",
        gene[i].initD,
        gene[i].deltaD,
        gene[i].initS,
        gene[i].deltaS);
    }
    return(out);
  }
   
  void merge(Genome g)
  {
	  for (int i=0;i<GenomeSize;i++){ // select one parent
		  // TODO may want to actually merge (average) the 2 genes
// average the parents' genes

/***/
		  gene[i].delta=(gene[i].delta+g.gene[i].delta)/2.;
		  gene[i].deltaD=(gene[i].deltaD+g.gene[i].deltaD)/2.;
		  gene[i].deltaS=(gene[i].deltaS+g.gene[i].deltaS)/2.;
		  gene[i].init=(gene[i].init+g.gene[i].init)/2.;
		  gene[i].initD=(gene[i].initD+g.gene[i].initD)/2.;
		  gene[i].initS=(gene[i].initS+g.gene[i].initS)/2.;
		  gene[i].variation=(gene[i].variation+g.gene[i].variation)/2.;
/***/

// select a gene from one parent or the other

/***
		  gene[i].delta=(Math.random() < .5)?
                                 gene[i].delta:g.gene[i].delta;
		  gene[i].deltaD=(Math.random() < .5)?
                                 gene[i].deltaD:g.gene[i].deltaD;
		  gene[i].deltaS=(Math.random() < .5)?
                                 gene[i].deltaS:g.gene[i].deltaS;
		  gene[i].init=(Math.random() < .5)?
                                 gene[i].init:g.gene[i].init;
		  gene[i].initD=(Math.random() < .5)?
                                 gene[i].initD:g.gene[i].initD;
		  gene[i].initS=(Math.random() < .5)?
                                 gene[i].initS:g.gene[i].initS;
		  gene[i].variation=(Math.random() < .5)?
                                 gene[i].variation:g.gene[i].variation;
***/

		  /***
		  if (Math.random() < .5){ // replace this gene
			  gene[i]=g.gene[i].copy();
		  }
		  ***/
		  
	  }
  }

  void randomize()
  {
    for (int i=0;i<GenomeSize;i++){
      gene[i].randomize();
    }
  }
}
