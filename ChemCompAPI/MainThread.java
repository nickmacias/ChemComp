package fft;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class MainThread extends Thread{
  Main parent;
  String fname; // remember these
  final int NUMPATTERNS=25;
  double SAMPLESIZE=0.05;

  Pattern[] allPatterns;  // all pattern buffers
  PrintWriter pw = null;

  public MainThread(Main parent_,String fname_) // save info for running
  {
    parent=parent_;
    fname=fname_;

    try
    {
    pw = new PrintWriter(new File(FileIO.COMPFILE));
    }
    catch(Exception e)
    {

    }
  }

  @SuppressWarnings("unused")
  public void run() // all processing
  {
    double[] rawData;
    double len;
    String zeroString;
    DataOutputStream dos;

    System.out.println("RUNNING!");
    parent.superRepaint();

    FileInputStream fis;
    fis=FileIO.openWaveData(fname);  // open WAV file for reading and skip header
    if (fis==null) return;

  //initialization loop: read NUMPATTERNS samples with random lengths
    allPatterns=new Pattern[NUMPATTERNS];
    for (int i=0;i<NUMPATTERNS;i++){
      len=(Math.random()/3.);
      len=SAMPLESIZE; // fixed length
      System.out.println("Reading sample of length " + len);
        rawData=FileIO.readWaveData(fis,len); // read a sample
        allPatterns[i]=Pattern.FFT(rawData); // rawData's length tells you how long the sample lasts
        parent.savePat(allPatterns[i],i);
        parent.repaint();
    } // all patterns loaded

    // now start sucking in samples from fis, and adjusting the pattern buffers based on each one
    Pattern thisP;
    int pNum=0;

    // main loop...
    if (0==0){ // rewind file
      try {
        fis.close();
      } catch (IOException e1) {
        e1.printStackTrace();
      }
      fis=FileIO.openWaveData(fname);  // rewind file so we compare same patterns
    }

  while (true){
      len=(Math.random()/3.); // sample length
      len=SAMPLESIZE; // fixed length
      rawData=FileIO.readWaveData(fis,len); // grab a sample
      if (rawData==null){ // hit EOF
        System.out.println("Hit EOF - goodbye!");
        System.exit(0);
      }

      ++pNum;
      if (pNum%NUMPATTERNS == 0){ // rewind file
        try {
          fis.close();
        } catch (IOException e1) {
          e1.printStackTrace();
        }
        fis=FileIO.openWaveData(fname);
      }

      if (pNum%NUMPATTERNS == 0){ // dump the buffers to a wave file

        if (pNum/NUMPATTERNS >= 10000) zeroString = "";
        else if(pNum/NUMPATTERNS >= 1000) zeroString = "0";
        else if(pNum/NUMPATTERNS >= 100) zeroString = "00";
        else if(pNum/NUMPATTERNS >= 10) zeroString = "000";
        else zeroString = "0000";

        System.out.println("Writing buffers to " + FileIO.OUTPUTFILEBASE + zeroString + (pNum/NUMPATTERNS) + ".wav");

        //if(pNum == NUMPATTERNS)
        {
        dos=FileIO.writeHeader(FileIO.OUTPUTFILEBASE+zeroString+(pNum/NUMPATTERNS)+".wav"); // open the file, and write the header
        //dos=FileIO.writeHeader(FileIO.OUTPUTFILEBASE+".wav"); // open the file, and write the header
        }

        //FileIO.writeSpacer(dos);

        for (int i=0;i<NUMPATTERNS;i++){
          FileIO.writeData(dos,allPatterns[i]);
          //FileIO.writeSpacer(dos);
        }

        try {
          //dos.flush();
          dos.close();
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }

      }

      thisP=Pattern.FFT(rawData); // find the frequency spectrum
      double result,maxSim=0,maxStale=0;
      int maxSimPat=-1,maxStalePat=-1;

      for (int i=0;i<NUMPATTERNS;i++){
        //System.out.print(" " + i);

        //double[] prevCoeff = new double[Pattern.NUMFREQ];

        //for(int j = 0; j < Pattern.NUMFREQ; j++) prevCoeff[j] = allPatterns[i].coeff[j];

        System.out.print("\nIt " + pNum + ": Pat " + i + ": ");

        result=allPatterns[i].compare(thisP); // update the saved patterns
        if(result > maxSim){
          maxSim = result;
          maxSimPat = i;
        } else if(result < 0) {
          if (allPatterns[i].staleness > maxStale) {
            maxStale = allPatterns[i].staleness;
            maxStalePat = i;
          }
        }

        parent.savePat(allPatterns[i],i);
        parent.repaint();

       /***
       // break out of this loop if we reset a pattern buffer
          if (result < 0){
            System.out.println("Aborting compare loop");
            break;
         }
       ***/

          //for(int j = 0; j < Pattern.NUMFREQ; j++) pw.println(i+"="+prevCoeff[j]+" ? "+pNum+"="+thisP.coeff[j]+" : "+i+"="+allPatterns[i].coeff[j]+" , c="+allPatterns[i].c+" , diff="+allPatterns[i].aDiff);
      }
      if(maxSimPat >= 0) {
        System.out.println("Morphing with pattern "+maxSimPat);
        allPatterns[maxSimPat].morph(thisP);
      }
      if(maxStalePat >= 0) {
        System.out.println("Reseeding pattern "+maxStalePat);
        allPatterns[maxStalePat].reseed(thisP);
      }
      //System.out.println();
    }
  }
}
