//
// Simplify raw file
//

import java.awt.EventQueue;
import java.util.Scanner;
import java.io.File;

public class Clean{

  public static void main(String[] args)
  {
    Scanner sc; // for reading from raw file
    String fileName="raw.txt";
    try{
      sc=new Scanner(new File(fileName));
      System.out.println(fileName + " opened succesfully for reading");
    } catch(Exception e) {
      System.out.println("ERROR: Can't open raw.txt: " + e);
      return;
    }

// prepare to parse
    while (sc.hasNextLine()){
      String temp=sc.nextLine();
// comment
      if (temp.charAt(0)=='*'){ // comment
        System.out.println(temp);
        continue;
      }
// setup
      if (temp.charAt(0)=='S'){ // setup EEXIST
        System.out.println(temp);
        continue;
      }
// individual
      if (temp.charAt(0)=='I'){
        String[] toks=temp.split(",");
        int gen=Integer.parseInt(toks[1]);
        int ind=Integer.parseInt(toks[2]);
        double score=Double.parseDouble(toks[3]);
        double score2=Double.parseDouble(toks[4]);
        System.out.print("I,"+gen+","+ind+","+score+","+score2);

        for (int i=0;i<Genome.GenomeSize;i++){
          int len=Integer.parseInt(toks[5+i*8]);
          double variation=Double.parseDouble(toks[6+i*8]);
          double init=Double.parseDouble(toks[7+i*8]);
          double initS=Double.parseDouble(toks[8+i*8]);
          double initD=Double.parseDouble(toks[9+i*8]);
          double delta=Double.parseDouble(toks[10+i*8]);
          double deltaS=Double.parseDouble(toks[11+i*8]);
          double deltaD=Double.parseDouble(toks[12+i*8]);
// print these with a , in the front
          System.out.printf(",%d,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f",
           len,variation,init,initS,initD,delta,deltaS,deltaS);
        }
        System.out.println();
      }
// keep reading
    } // EOF
    sc.close();
    return;
  }
}
