/*
 * Definition of a single gene. The genome of the organism is comprised of a
 * (fixed-size) array of genes.
 */
public class Gene {
  int type; // distribution of chemicals, diameters or biases
    static int CHEM=0;
    static int DIAM=1;
    static int BIAS=2;
  
  //int start; // tube index for start of this effect
  double init; // initial setting
  double initS, initD; // for chemicals
  double delta; // how much things change from one tube to the next
  double deltaS,deltaD; // for chemicals
  int len; // # of tubes in this effect
  double variation; // how much we deviate from a simple linear change
  
  Gene copy() // make a copy
  {
	  Gene temp=new Gene();
	  temp.type=type;
	  temp.delta=delta;
	  temp.deltaS=deltaS;
	  temp.deltaD=deltaD;
	  temp.init=init;
	  temp.initS=initS;
	  temp.initD=initD;
	  temp.len=len;
	  temp.variation=variation;
	  return(temp);
  }

  void randomize()
  {
    type=1+(int)(2.*Math.random()); // only 1 or 2!
    delta=(Math.random())-.5; //-.5 to +.5
    deltaS=(Math.random())-.5;
    deltaD=(Math.random())-.5;
    delta=delta/10.;
    deltaD=deltaD/10.;
    deltaS=deltaS/10.;
    init=Math.random()-.5; //-.5 to +.5
    initS=60.*Math.random();
    initD=60.*Math.random();
    //variation=(Math.random()-.5)/20.; // multiply this by Math.random() and add at each step
    //TODO re-install variation, but only for first run
    variation=0;
  }
}
