import com.cellmatrix.chemcomp.API.v1.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;

//
// main execution thread. started by analyzecontrol, and used
// to run the simulation

public class AnalyzeCore extends Thread{
  int stepLimit=0;
  boolean running;
  EEXIST e;Display disp;
  boolean testing=false;
  int startInd=0,endInd=0; // range of individuals to test
  int delay=0;
  AnalyzeControl parent;
  String tname="titanic.csv";
  ArrayList<Passenger> list; // passenger data
  Genome thisInd=null;

// save parent so we can read delay
  public void saveMe(AnalyzeControl parent)
  {
    this.parent=parent;
  }

  public AnalyzeCore(EEXIST e, Display disp)
  {
    this.e=e;this.disp=disp;

// load passenger data
    list=new ArrayList<Passenger>();
    try{
      Scanner sc=new Scanner(new File(tname));
      sc.nextLine(); // skip header
      while (sc.hasNextLine()){
        String buffer=sc.nextLine(); // read a line of data
        Passenger pass=new Passenger(buffer); // parse line, save in passenger object
        list.add(pass); // add to ArrayList
      }
      sc.close();
    } catch (Exception ex){
      System.out.println("Error processing titanic file " + tname + ": " + ex);
      return;
    }

    System.out.println("Processed " + list.size() + " records from " + tname);
  }

  public void testing(boolean testing, Genome thisInd,
                      int startInd,int endInd)
  {
    this.testing=testing;
    this.thisInd=thisInd; // genetic code for individual under test
    this.startInd=startInd;
    this.endInd=endInd; // starting and ending passenger ID
  }

  public void halt() // allows halt to be requested
  {
    running=false;
    testing=false; // no output monitor
  }
  
  public void run()
  {
    int steps=0; // # of steps we've done
    running=true; // clear to tell thread to exit

// test-related vars
    boolean level=false; // input starts LOW

// main simulation loop
    while (running){

// delay as indicated by user
      delay=parent.getDelay();
      if (delay != 0) myDelay(delay);

      if (!testing){ // just doing a normal run command...
        e.step(1);
        if ((stepLimit!=0) && (++steps==stepLimit)) running=false;
        continue;
      }

// testing here
      Iterator<Passenger> li=list.iterator(); // start an iterator
      double totalScore=0;
      double pcount=0;
      int PID=0; // ignore passenger ID's: just count males
      while (li.hasNext()){ // iterate over passengers
        Passenger p=li.next();
        if (p.sex=='m') ++PID;
        if ((p.sex=='m') && (PID >= startInd) && (PID <= endInd)){
          thisInd.load(e); // load IUT into the system
          totalScore+=assess(p,e); // run a test, tally the score
          ++pcount;
        }
      } // this individual assessed

      System.out.println("Total score=" + totalScore +"/" + pcount + "(" +
                         (100.*totalScore/pcount) + "%)");

      running=false;
// finished?
      //if ((stepLimit!=0) && (++steps==stepLimit)) running=false;
    } // end of test mode
  } // thread exits here

  double assess(Passenger p,EEXIST e) // assess this individual
  {
// load inputs
    load(e,0,4,(p.sex=='m')?10:20); // sex, m or f
    load(e,4,8,p.cls*10); // passenger class, 1 2 or 3
    load(e,8,12,p.age/2); // age, nominally 0-80 (clip at 80)
    load(e,12,16,4+p.sibsp*4); // siblings/spouse, 0-8?
    load(e,16,20,10+p.parch*10); // parents/children, mostly 0-2
    load(e,20,24,p.fare/15.); // up to 512
    load(e,24,28,(p.embarked=='c')?10:((p.embarked=='q')?20:30));
    load(e,28,40,0.); // empty space

// step the EEXIST
    e.step(64);

// read output
    double sum=readAvg(e,36,40);
    if ((sum>=15 && !p.survived) ||
        (sum<15 && p.survived)) return(1);
    return(0);
  }

  double readAvg(EEXIST e, double start, double end)
  {
    double sum=0;
    for (double x=start;x<end;x+=e.getDx()){
      sum+=e.getTube(x,EEXIST.SRC)+e.getTube(x,EEXIST.DST);
    }
    sum=sum/((end-start)/e.getDx()); // average value
    return(sum);
  }

  void load(EEXIST e, double start, double end, double value)
  {
    if (value < 0) value=0;
    if (value > 60) value=60;

    for (double x=start;x<end;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,value);
      e.setTube(x,EEXIST.DST,value);
    }
  }

  public void setLimit(int stepLimit)
  {
    this.stepLimit=stepLimit;
  }

  public void myDelay(int t)
  {
    try{Thread.sleep(t);} catch(Exception e){}
    return;
  }

}
