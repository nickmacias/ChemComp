//
// control panel class
//

import com.cellmatrix.chemcomp.API.v1.*;
import java.awt.EventQueue;
import java.util.Scanner;
import java.io.File;

public class AnalyzeControl extends Thread{
  EEXIST e;
  Display disp;
  AnalyzeCore core=null; // create this when we want to run forward

  String fileName;
  Scanner fileScanner=null; // for reading from raw file

  int delayTime=0; // pass this to core before running a test

  Genome thisInd=null; // loaded from file

// main method: construct EEXIST, display and control panel
// then idle until user starts thread

  public void run()
  {
    e=new EEXIST(); // main EEXIST object
    e.proportionalFlow(true); // Different from latest EA2!
    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display

    disp.cursor(0);
    disp.cursor(2);
    disp.cursor(4);
    disp.cursor(8);
    disp.cursor(10);
    disp.cursor(12);
    disp.cursor(16);
    disp.cursor(18);
    disp.cursor(20);
    disp.cursor(24);
    disp.cursor(26);
    disp.cursor(28);

    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this,ControlPanel.Slider,0,10,0,400,1,"Karma","cpHandler");
    cp.connect(this,ControlPanel.Slider,1,128,0,255,2,"Intensity","cpHandler");
    cp.connect(this,ControlPanel.CheckBox,0,10,"Run/Pause","cpHandler");
    cp.connect(this,ControlPanel.TextIn,0,11,"Command","cpHandler");
  }
// gene.load(e);e.step(1);
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 2: // intensity
      disp.setIntensity(arg.getSliderPos());
      break;
    case 10: // Run/Pause
      System.out.println(arg.getCheckState()?"Running":"Paused");
      break;
    case 11: // text input
      System.out.println("<"+arg.getTextIn()+">");
      if (!parse(arg.getTextIn())) System.exit(0); // returns FALSE after Q command
      break;
    }
  }

  boolean parse(String s)
//
// r - run forever
// r n - run n steps
// h - halt
// f filename - name input file
// l gen ind - load this individual
// l - reload last individual
// t b0 b1 - run a test!
// x - load a random tube (debug gadget)
// Q - quit all
//
  {
    String[] sa=s.split(" ");
    for (int i=0;i<sa.length;i++){
      System.out.println("["+i+"]:<"+sa[i]+">");
    }
    if (sa.length < 1) return(true); // just an ENTER?

// QUIT
    if (sa[0].equals("Q")){
      if (core!=null) core.halt();
      return(false);
    }

// RUN commands
    if (sa[0].equals("h")){
      if (core != null){
        core.halt(); // signal core to exit
        core=null; // and remember that it's gone
        return(true);
      }
    }
    if (sa[0].equals("r")){
      if (core==null){
        core=new AnalyzeCore(e,disp);
      }
// is a length specified?
      if (sa.length==2){ // yes
        try{
          core.setLimit(Integer.parseInt(sa[1]));
        }catch(Exception e){
          System.out.println("Error parsing " + sa[1]);return(true);
        }
        core.start(); // start the thread
        core=null; // since this thread will exit shortly
      } else {
        core.setLimit(0); // no limit
        core.start(); // start the core thread; will exit after HALT of when done
      }
    }

// set delay
    if (sa[0].equals("d")){
      if (sa.length==2){ // yes
        delayTime=Integer.parseInt(sa[1]);
        System.out.println("Delay set to " + delayTime);
      }
      return(true);
    }

// test :)
    if (sa[0].equals("t")){
      if (sa.length == 3){ // 2-input gate
// get bit values
        boolean b0=sa[1].equals("1");
        boolean b1=sa[2].equals("1");
        singleTest(b0,b1,e); // load these inputs
// now tell the core system to tally the output, and run 50 steps
        if (core==null){
          core=new AnalyzeCore(e,disp);
        }
        core.setLimit(25);
        core.setDelayTime(delayTime);
        core.checkOutput(true); // tally and output running score
        core.start();
        core=null;
        return(true);
      } // end of 2-input test

     if (sa.length == 4){ // 3-input gate
// get bit values
        boolean b0=sa[1].equals("1");
        boolean b1=sa[2].equals("1");
        boolean b2=sa[3].equals("1");
        singleTest(b0,b1,b2,e); // load these inputs
// now tell the core system to tally the output, and run 50 steps
        e.step(50); // pre-advance
        if (core==null){
          core=new AnalyzeCore(e,disp);
        }
        core.setDelayTime(delayTime);
        core.setLimit(25);
        core.checkOutput(true); // tally and output running score
        core.start();
        core=null;
        return(true);
      }
      System.out.println("Expecting t b0 b1   or t b0 b1 b2");
      return(true);
    } // end of "t"


// file
    if (sa[0].equals("f")){
      if (sa.length<2) return(true); // need a filename
      fileName=sa[1]; // save this
      try{
        fileScanner=new Scanner(new File(fileName));
        System.out.println(fileName + " opened succesfully for reading");
        fileScanner.close();
      }catch(Exception e){
        System.out.println("ERROR: Can't open " + sa[1]);
      }
      return(true);
    }

// random
    if (sa[0].equals("x")){
      e.setTube(40*Math.random(),(int)(2.*Math.random()),40.*Math.random());
      return(true);
    }

// load an individual
    if (sa[0].equals("l")){ // check usage
      if (sa.length==1){ // load last individual
        e.clearAllTubes();
        thisInd.load(e);
        return(true);
      }

      if (sa.length != 3){ // error
        System.out.println("Expecting l or l gen ind");
        return(true);
      }

// prepare to parse
      int gen,ind; // generation # and individual #
      try{
        gen=Integer.parseInt(sa[1]);
        ind=Integer.parseInt(sa[2]);
      }catch(Exception e){
        System.out.println("Parse error: l " + sa[1] + " " + sa[2]);
        return(true);
      }
// open the file and scan
      try{
        fileScanner=new Scanner(new File(fileName));
        System.out.println("Searching " + fileName);
        thisInd=load(fileScanner,gen,ind,e); // scan file and load this individual
        e.clearAllTubes();
        thisInd.load(e); // load into system
// note that the karma etc. will be setup as the file is scanned
        fileScanner.close();
      }catch(Exception e){
        System.out.println("Can't open " + fileName);
        return(true);
      }
    }

    return(true);

  }

  Genome load(Scanner sc, int tgtGen, int tgtInd, EEXIST e)
  {
    Genome ret=new Genome(e.numTubes());
    while (sc.hasNextLine()){
      String temp=sc.nextLine();
// comment
      if (temp.charAt(0)=='*') continue; // comment
// setup
      if (temp.charAt(0)=='S'){ // setup EEXIST
        String[] toks=temp.split(",");
        double karma=Double.parseDouble(toks[1]);
        e.setKarma(karma); // adjust this as we scan the file
        continue;
      }
// individual
      if (temp.charAt(0)=='I'){
        String[] toks=temp.split(",");
        int gen=Integer.parseInt(toks[1]);
        int ind=Integer.parseInt(toks[2]);
        double score=Double.parseDouble(toks[3]);
        double score2=Double.parseDouble(toks[4]);
        if ((gen != tgtGen)||(ind != tgtInd)) continue;
// found the individual here
        for (int i=0;i<Genome.GenomeSize;i++){
          ret.gene[i].len=Integer.parseInt(toks[5+i*8]);
          ret.gene[i].variation=Double.parseDouble(toks[6+i*8]);
          ret.gene[i].init=Double.parseDouble(toks[7+i*8]);
          ret.gene[i].initS=Double.parseDouble(toks[8+i*8]);
          ret.gene[i].initD=Double.parseDouble(toks[9+i*8]);
          ret.gene[i].delta=Double.parseDouble(toks[10+i*8]);
          ret.gene[i].deltaS=Double.parseDouble(toks[11+i*8]);
          ret.gene[i].deltaD=Double.parseDouble(toks[12+i*8]);
        }
        System.out.println("Score=" + score + "," + score2);
        sc.close(); // done with file for now
        return(ret); // return the genome
      }
// keep reading
    } // EOF
    sc.close();
    System.out.println("WARNING: Didn't find gen " + tgtGen +
                       ", Individual " + tgtInd);
    return(null);
  }


  void singleTest(boolean a, boolean b, EEXIST e)
  {

    double score=0; // running score

    // input a is placed in [0,4]; b in [8,12]; c in [16,20]; y is read from [24,28]
    for (double x=0;x<4;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,a?20:5);
      e.setTube(x,EEXIST.DST,a?20:5); // chemical balance (input)
//e.setDiameter(x,0);
    }

    for (double x=8;x<12;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,b?20:5);
      e.setTube(x,EEXIST.DST,b?20:5);
//e.setDiameter(x,0);
    }

    return;
  }

  void singleTest(boolean a, boolean b, boolean c, EEXIST e)
  {

    
    double score=0; // running score

    // input a is placed in [0,4]; b in [8,12]; c in [16,20]; y is read from [24,28]
    for (double x=0;x<4;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,a?20:5);
      e.setTube(x,EEXIST.DST,a?20:5); // chemical balance (input)
//e.setDiameter(x,0);
    }

    for (double x=8;x<12;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,b?20:5);
      e.setTube(x,EEXIST.DST,b?20:5);
//e.setDiameter(x,0);
    }

    for (double x=16;x<20;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,c?20:5);
      e.setTube(x,EEXIST.DST,c?20:5);
      //e.setDiameter(x,0);
    }

    return;
  }

}
