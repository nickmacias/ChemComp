import com.cellmatrix.chemcomp.API.v1.*;
import java.io.PrintWriter;

public class Core extends Thread{
  EEXIST e;
  Display disp;
  
  PrintWriter pw;

  public void run()
  {
    try{
      pw=new PrintWriter("raw.txt");
    } catch(Exception e){
      System.out.println("Can't write to output file ("+e+")- goodbye");
      return;
    }
    e=new EEXIST(); // main EEXIST object
    e.proportionalFlow(true);
    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display
    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,50,0,400,1,"Karma","cpHandler");
    cp.connect(this, ControlPanel.Slider,1,128,0,255,2,"Intensity","cpHandler");

    writeStatus(e,pw); // write relevant info about EEXIST
    int step=0; // # of iterations

//
// Start with 2 (random) individuals
// then either (a) mate them with each other; (b) Mate one with rand; or
// (c) introduce a random. Keep best 2 and repeat forever...
//

    Genome indA, indB, indTemp, indRand;
    indA=new Genome(e.numTubes());indA.randomize();
    double scoreA=score(indA);
    indB=new Genome(e.numTubes());indB.randomize();
    double scoreB=score(indB);
    double scoreTemp;

// main simulation loop
    while (true){
      ++step;
      double bestScore=(scoreA > scoreB)?scoreA:scoreB;
      double way=Math.random(); // which way to go
      System.out.print("Step " + step);

// set mutation rate based on how close we are to perfection
      double mRate=(12800.-bestScore)/12800.;
      if (mRate < .05) mRate=.05;

      if (way < .25){ // just mate top 2
        System.out.print(":[A+B]");
        indTemp=indA.copy();
        indTemp.merge(indB);
        indTemp.mutate(mRate);
        scoreTemp=score(indTemp);
      } else if (way > .45){ // mate indA with random
        System.out.print(":[A+?]");
        indRand=new Genome(e.numTubes());
        indRand.randomize();
        indTemp=indA.copy();
        indTemp.merge(indRand);
        indTemp.mutate(mRate);
        scoreTemp=score(indTemp);
      } else if (way > .65){ // mate indB with random (never done)
        System.out.print(":[B+?]");
        indRand=new Genome(e.numTubes());
        indRand.randomize();
        indTemp=indB.copy();
        indTemp.merge(indRand);
        indTemp.mutate(mRate);
        scoreTemp=score(indTemp);
      } else { // just make new random individual
        System.out.print(":[?]  ");
        indTemp=new Genome(e.numTubes());
        indTemp.randomize();
        scoreTemp=score(indTemp);
      }
      System.out.print(" A:" + scoreA + " B:" + scoreB + " New:" + scoreTemp);

// write new genome to file
        double score2=0;
        writeIndiv(step,0,indTemp,scoreTemp,score2,pw);
// decide who to keep
        if (scoreTemp > scoreA){
          indA=indTemp.copy();
          scoreA=scoreTemp;
          System.out.print(" *");
        } else if (scoreTemp > scoreB){
          indB=indTemp.copy();
          scoreB=scoreTemp;
          System.out.print(" *");
        }
        System.out.println();
    } // and repeat forever
  }

// run all tests on g and return their score
  public double score(Genome g)
  {
    double score=0.;
    for (int test=0;test<8;test++){
      g.load(e);
      score+=assess(test,e); // run a test, tally the score
    }
    return(score);
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      writeStatus(e,pw); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 2: // intensity
      disp.setIntensity(arg.getSliderPos());
      break;
    }
  }

  double randomAssess(int testNum,EEXIST e) // assess this individual
  {
  // randomize a test
    boolean a=(Math.random()<.5);
    boolean b=(Math.random()<.5);
    boolean c=(Math.random()<.5);
    return(singleTest(a,b,c,testNum,e));
  }

  double assess(int testNum,EEXIST e) // assess this individual
  {
    boolean a=((testNum&1) != 0);
    boolean b=((testNum&2) != 0);
    boolean c=((testNum&4) != 0);
    return(singleTest(a,b,c,testNum,e));
  }

  double singleTest(boolean a, boolean b, boolean c, int testNum,EEXIST e)
  {

// %%% Test Function Here

    //boolean y=!(a&b&c); // nand
    //boolean y=!(a|b|c); // nor
    boolean y=(a^b^c); // XOR
    
    double score=0; // running score

    // input a is placed in [0,4]; b in [8,12]; c in [16,20]; y is read from [24,28]
    for (double x=0;x<4;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,a?20:5);
      e.setTube(x,EEXIST.DST,a?20:5); // chemical balance (input)
    }

    for (double x=8;x<12;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,b?20:5);
      e.setTube(x,EEXIST.DST,b?20:5);
    }

    for (double x=16;x<20;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,c?20:5);
      e.setTube(x,EEXIST.DST,c?20:5);
    }

    // step 50 ticks, then read value for 25 more
    e.step(50);
    for (int i=0;i<25;i++){
      e.step(1);
      for (double x=24;x<28;x+=e.getDx()){
        double sum=e.getTube(x, EEXIST.SRC)+e.getTube(x, EEXIST.DST);
        if ((y && sum>=15) || ((!y) && sum<10)) ++score;
      } // this output evaluated
    } // end of tests
    return(score);
  }

  boolean firstWS=true;
  void writeStatus(EEXIST e,PrintWriter pw)
  {
    if (firstWS){
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+
              e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
              ","+e.numTubes());
    pw.flush();
  }

  boolean firstWI=true;
  void writeIndiv(int step,int indiv,Genome genome,
                  double score,double score2,PrintWriter pw)
  {
    if (firstWI){
      pw.println("*I,step,ind,score,score2,len[i],var[i],init[i],initS[i],"+
                 "initD[i],delta[i],deltaS[i],deltaD[i]");
      firstWI=false;
    }
    pw.print("I,"+step+","+indiv+","+score+","+score2);
    for (int i=0;i<Genome.GenomeSize;i++){
      pw.print(","+genome.gene[i].len+","+genome.gene[i].variation+","+
               genome.gene[i].init+","+genome.gene[i].initS+","+genome.gene[i].initD+","+
               genome.gene[i].delta+","+genome.gene[i].deltaS+","+genome.gene[i].deltaD);
    }
    pw.println();pw.flush();
  }
}
