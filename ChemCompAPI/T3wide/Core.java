import com.cellmatrix.chemcomp.API.v1.*;
import java.io.PrintWriter;

public class Core extends Thread{
  EEXIST e;
  Display disp;
  Game game; // :)
  OpponentControl oc=new OpponentControl(); // used to get opponent's moves

  int popSize=50; // total # of individuals in the population (was 250)
  int survivorSize=10; // # of individuals to keep after each generation

  PrintWriter pw;

  Genome[] population=new Genome[popSize];
  Genome[] best=new Genome[survivorSize]; // save best members here
  double[] score=new double[popSize]; // save scores

  public void run()
  {
    try{
      pw=new PrintWriter("raw.txt");
    } catch(Exception e){
      System.out.println("Can't write to output file ("+e+")- goodbye");
      return;
    }
    e=new EEXIST(); // main EEXIST object
    e.setKarma(2.5);
    game=new Game(e); // game-playing object
    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display
    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,25,0,400,1,"Karma","cpHandler");
    cp.connect(this, ControlPanel.Slider,1,128,0,255,2,"Intensity","cpHandler");

    writeStatus(e,pw); // write relevant info about EEXIST
    int gen=0; // track generations

// make an initial population
    for (int i=0;i<popSize;i++){ 
      population[i]=new Genome(e.numTubes());
      population[i].randomize();
    }
// main simulation loop
    System.out.println((char)27 + "[2J"); // clear screen
    while (true){
      ++gen;
      for (int indiv=0;indiv<popSize;indiv++){
        System.out.println("----------- Gen " + gen + " Indiv " + indiv +
                           " -----------");
        population[indiv].load(e); // re-load
        score[indiv]=assess(0,e); // run a test, save the score
        double score2=0;
        writeIndiv(gen,indiv,population[indiv],score[indiv],score2,pw);
        System.out.printf("\n%d:%d(%.2f)\n",gen,indiv,score[indiv]);
      } // end of evaluation of all individuals
      double bestScore=rank(); // rank and merge
      System.out.println("\nGen " + gen + " Best score=" + bestScore + "\n"); // rank all individuals
      breed(.15); // breed them, with mutation rate of 15%
    } // and repeat forever
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      writeStatus(e,pw); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 2: // intensity
      //disp.setIntensity(arg.getSliderPos());
      break;
    }
  }

//
// assessment test: single test
//

  double assess(int testNum,EEXIST e) // assess this individual
  {

    double score=0; // running score
    int forfeits=0;
    oc.init(); // reset oc's moves[] array to [0,0,0,0]
    boolean playMore=true;
    int gameNum=0;
    while (playMore){
      oc.start(); // reset turnNum
      int thisScore=game.playGame(gameNum,oc); // pass game number so opp can cycle 1st move
      System.out.print("Game " + gameNum + " score: " + thisScore);
      if (thisScore==-1){
        System.out.println("\nGame " + gameNum + " forfeit, giving up.     ");
        return(0); // give up!
      }
      score+=thisScore;
      System.out.print(" Sum=" + score + "           ");
      playMore=oc.advance(); // advance oc's moves[] array. If out of moves, playMore=false
      ++gameNum; // # of games
    }
    return(score);
  }

  double rank() // sort population[] by score[]
  {
    for (int loop=0;loop<popSize;loop++){ // good old bubble sort!
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (score[i+1] > score[i]){ // swap
          double temp=score[i+1];score[i+1]=score[i];score[i]=temp;
          Genome temp2=population[i+1];population[i+1]=population[i];population[i]=temp2;
        }
      }
    }

// copy best elements of population to best[] array
    double bestScore=0;
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i].copy();
      if (score[i] > bestScore) bestScore=score[i];
    }
    return(bestScore);
  }
  
  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy();
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].merge(best[(int)(Math.random()*survivorSize)]);
        population[i].mutate(mutateRate); // random variation
      }
    }
  }

  boolean firstWS=true;
  void writeStatus(EEXIST e,PrintWriter pw)
  {
    if (firstWS){
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+
              e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
              ","+e.numTubes());
    pw.flush();
  }

  boolean firstWI=true;
  void writeIndiv(int gen,int indiv,Genome genome,
                  double score,double score2,PrintWriter pw)
  {
    if (firstWI){
      pw.println("*I,gen,ind,score,score2,len[i],var[i],init[i],initS[i],"+
                 "initD[i],delta[i],deltaS[i],deltaD[i]");
      firstWI=false;
    }
    pw.print("I,"+gen+","+indiv+","+score+","+score2);
    for (int i=0;i<Genome.GenomeSize;i++){
      pw.print(","+genome.gene[i].len+","+genome.gene[i].variation+","+
               genome.gene[i].init+","+genome.gene[i].initS+","+genome.gene[i].initD+","+
               genome.gene[i].delta+","+genome.gene[i].deltaS+","+genome.gene[i].deltaD);
    }
    pw.println();pw.flush();
  }
}
