import com.cellmatrix.chemcomp.API.v1.*;
import java.io.PrintWriter;

public class Core extends Thread{
  EEXIST e;
  Display disp;
  
  int popSize=250; // total # of individuals in the population
  int survivorSize=10; // # of individuals to keep after each generation

  PrintWriter pw;

  Genome[] population=new Genome[popSize];
  Genome[] best=new Genome[survivorSize]; // save best members here
  double[] score=new double[popSize]; // save scores

  public void run()
  {
    try{
      pw=new PrintWriter("raw.txt");
    } catch(Exception e){
      System.out.println("Can't write to output file ("+e+")- goodbye");
      return;
    }
    e=new EEXIST(); // main EEXIST object
    disp=new Display(e); // create a new display
    disp.setVisible(true);  // show the display
    ControlPanel cp=new ControlPanel("Control Panel");
    cp.connect(this, ControlPanel.Slider,0,50,0,400,1,"Karma","cpHandler");
    cp.connect(this, ControlPanel.Slider,1,128,0,255,2,"Intensity","cpHandler");

    writeStatus(e,pw); // write relevant info about EEXIST
    int gen=0; // track generations

// make an initial population
    for (int i=0;i<popSize;i++){ 
      population[i]=new Genome(e.numTubes());
      population[i].randomize();
    }
// main simulation loop
    while (true){
      ++gen;
      for (int indiv=0;indiv<popSize;indiv++){
        population[indiv].load(e); // load this individual into the system
        score[indiv]=0; // initial score
// should we disable the pre-stabilize step? %%%
        for (int test=0;test<8;test++){
          population[indiv].load(e); // re-load
          score[indiv]+=assess(test,e); // run a test, tally the score
        } // this individual assessed
// now try out-of-band data
        double score2=0;
        writeIndiv(gen,indiv,population[indiv],score[indiv],score2,pw);
        System.out.print(gen + ":" + indiv + "(" + score[indiv] +
                         "," + score2 + ")");

        if (indiv%5==4) System.out.println();
      } // end of evaluation of all individuals
      System.out.println("\nGen " + gen + " Best score=" + rank() + "\n"); // rank all individuals
      breed(.05); // breed them, with mutation rate of 5%
    } // and repeat forever
  }
  
  public void cpHandler(ControlArgs arg)
  {
    switch(arg.getUserID()){
    case 1: // karma
      e.setKarma(((double)arg.getSliderPos())/10);
      writeStatus(e,pw); // write new EEXIST data
      System.out.println("\n\nKARMA SET TO " + e.getKarma() + "\n");
      break;
    case 2: // intensity
      disp.setIntensity(arg.getSliderPos());
      break;
    }
  }

  double randomAssess(int testNum,EEXIST e) // assess this individual
  {
  // randomize a test
    boolean a=(Math.random()<.5);
    boolean b=(Math.random()<.5);
    boolean c=(Math.random()<.5);
    return(singleTest(a,b,c,testNum,e));
  }

  double assess(int testNum,EEXIST e) // assess this individual
  {
    boolean a=((testNum&1) != 0);
    boolean b=((testNum&2) != 0);
    boolean c=((testNum&4) != 0);
    return(singleTest(a,b,c,testNum,e));
  }

  double singleTest(boolean a, boolean b, boolean c, int testNum,EEXIST e)
  {

// %%% Test Function Here

    //boolean y=!(a&b&c); // nand
    //boolean y=!(a|b|c); // nor
    boolean y=(a^b^c); // XOR
    
    double score=0; // running score

    // input a is placed in [0,4]; b in [8,12]; c in [16,20]; y is read from [24,28]
    for (double x=0;x<4;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,a?20:5);
      e.setTube(x,EEXIST.DST,a?20:5); // chemical balance (input)
    }

    for (double x=8;x<12;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,b?20:5);
      e.setTube(x,EEXIST.DST,b?20:5);
    }

    for (double x=16;x<20;x+=e.getDx()){
      e.setTube(x,EEXIST.SRC,c?20:5);
      e.setTube(x,EEXIST.DST,c?20:5);
    }

    // step 50 ticks, then read value for 25 more
    e.step(50);
    for (int i=0;i<25;i++){
      e.step(1);
      for (double x=24;x<28;x+=e.getDx()){
        double sum=e.getTube(x, EEXIST.SRC)+e.getTube(x, EEXIST.DST);
        if ((y && sum>=15) || ((!y) && sum<10)) ++score;
      } // this output evaluated
    } // end of tests
    return(score);
  }

  double rank() // sort population[] by score[]
  {
    for (int loop=0;loop<popSize;loop++){ // good old bubble sort!
      for (int i=0;i<popSize-1;i++){ // compare score[i] with score[i+1]
        if (score[i+1] > score[i]){ // swap
          double temp=score[i+1];score[i+1]=score[i];score[i]=temp;
          Genome temp2=population[i+1];population[i+1]=population[i];population[i]=temp2;
        }
      }
    }

// copy best elements of population to best[] array
    double bestScore=0;
    for (int i=0;i<survivorSize;i++){
      best[i]=population[i].copy();
      bestScore+=score[i];
    }
    return(score[0]);
  }
  
  void breed(double mutateRate) // copy best individuals to best[] array, then breed them into the population[] array
  {
    for (int i=0;i<popSize;i++){
      if (i<survivorSize){
        population[i]=best[i].copy();
      } else { // merge 2 members
        population[i]=best[(int)(Math.random()*survivorSize)].copy();
        population[i].merge(best[(int)(Math.random()*survivorSize)]);
        population[i].mutate(mutateRate); // random variation
      }
    }
  }

  boolean firstWS=true;
  void writeStatus(EEXIST e,PrintWriter pw)
  {
    if (firstWS){
      pw.println("*S,karma,dx,maxx,minx,timestep,numtubes");
      firstWS=false;
    }
    pw.println("S,"+e.getKarma()+","+e.getDx()+","+
              e.getMaxX()+","+e.getMinX()+","+e.getTimeStep()+
              ","+e.numTubes());
    pw.flush();
  }

  boolean firstWI=true;
  void writeIndiv(int gen,int indiv,Genome genome,
                  double score,double score2,PrintWriter pw)
  {
    if (firstWI){
      pw.println("*I,gen,ind,score,score2,len[i],var[i],init[i],initS[i],"+
                 "initD[i],delta[i],deltaS[i],deltaD[i]");
      firstWI=false;
    }
    pw.print("I,"+gen+","+indiv+","+score+","+score2);
    for (int i=0;i<Genome.GenomeSize;i++){
      pw.print(","+genome.gene[i].len+","+genome.gene[i].variation+","+
               genome.gene[i].init+","+genome.gene[i].initS+","+genome.gene[i].initD+","+
               genome.gene[i].delta+","+genome.gene[i].deltaS+","+genome.gene[i].deltaD);
    }
    pw.println();pw.flush();
  }
}
