package com.cellmatrix.EEXIST.API_V2;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Method;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Nicholas J. Macias, Cell Matrix Corporation
 * @version 1.0
 *
 * ControlPanel - a customizable widget interface for use manipulation of the run-time environment.
 * <p>
 * Constructing a ControlPanel causes the appearance of an empty panel with a given title.
 * User code can create buttons, checkboxes, sliders, and input- and output- text areas,
 * and link them to user code of their choice with a simple connect() method.
 */
public class ControlPanel extends JFrame{

// controls
  private JButton butt0,butt1,butt2,butt3;
  private JCheckBox cb0,cb1,cb2,cb3;
  private JSlider slider0,slider1,slider2,slider3;
  private JTextArea textOut;
  private JTextField textIn;
  private JLabel textInLabel;
  private JLabel sliderLab0,sliderLab1,sliderLab2,sliderLab3;
  private boolean textInInstant=false; // wait for ENTER
  private JButton btnClear;

// control arrays
  private ButtonControl[] buttons=new ButtonControl[4];
  private CheckBoxControl[] cbs=new CheckBoxControl[4];
  private SliderControl[] sliders=new SliderControl[4];
  private JLabel[] sliderLabs=new JLabel[4];
  private TextInControl textIns=new TextInControl();

  /**
   * Controls when the text handler is called.
   * @param textInInstant true means call the handler after any key stroke; false means call only after ENTER
   */
  public void textInInstant(boolean textInInstant)
  {
    this.textInInstant=textInInstant;
  }

/**
 * Creates an empty control panel, on which the user can place buttons,
 * check boxes and sliders. The panel can also contain text input and
 * output areas.
 * @param title the title to display on the top of the panel
 */
	public ControlPanel(String title) {
		butt0 = new JButton("New button");
		butt0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			// call user-specified method
			  callButton(0); // call user method
			}
		});

		 butt1 = new JButton("New button");
		 butt1.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 	  callButton(1);
		 	}
		 });

		 butt2 = new JButton("New button");
		 butt2.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 	  callButton(2);
		 	}
		 });

		 butt3 = new JButton("New button");
		 butt3.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent e) {
		 	  callButton(3);
		 	}
		 });

		 cb0 = new JCheckBox("New check box");
		 cb0.addItemListener(new ItemListener() {
		 	public void itemStateChanged(ItemEvent e) {
		 	  callcb(0);
		 	}
		 });

		 cb1 = new JCheckBox("New check box");
		 cb1.addItemListener(new ItemListener() {
		 	public void itemStateChanged(ItemEvent arg0) {
		 	  callcb(1);
		 	}
		 });

		 cb2 = new JCheckBox("New check box");
		 cb2.addItemListener(new ItemListener() {
		 	public void itemStateChanged(ItemEvent e) {
		 	  callcb(2);
		 	}
		 });

		 cb3 = new JCheckBox("New check box");
		 cb3.addItemListener(new ItemListener() {
		 	public void itemStateChanged(ItemEvent e) {
		 	  callcb(3);
		 	}
		 });

		textIn = new JTextField();
		textIn.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textIn.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
			  if (e.getKeyCode() == KeyEvent.VK_ENTER){
			    callTextIn(0);
			    textIn.setText("");
			  } else if (textInInstant){
			    callTextIn(0);
			    textIn.setText("");
			  }
			}
		});
		textIn.setColumns(10);

		textInLabel = new JLabel("Command:");

		slider0 = new JSlider();
		slider0.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
			  callSlider(0);
			}
		});

		slider1 = new JSlider();
		slider1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
			  callSlider(1);
			}
		});

		slider2 = new JSlider();
		slider2.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
			  callSlider(2);
			}
		});

		slider3 = new JSlider();
		slider3.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
			  callSlider(3);
			}
		});

		sliderLab0 = new JLabel("New label");

		sliderLab1 = new JLabel("New label");

		sliderLab2 = new JLabel("New label");

		sliderLab3 = new JLabel("New label");

		scrollPane = new JScrollPane();
		scrollPane.setAutoscrolls(true);
		scrollPane.setVisible(false);

		btnClear = new JButton("Clear");
		btnClear.setVisible(false);
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			  textOut.setText("");
			}
		});
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(60)
							.addComponent(textInLabel)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(textIn, GroupLayout.DEFAULT_SIZE, 657, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(50)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
									.addGroup(groupLayout.createSequentialGroup()
										.addComponent(butt0)
										.addGap(42)
										.addComponent(cb0))
									.addGroup(groupLayout.createSequentialGroup()
										.addComponent(butt3)
										.addGap(42)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
											.addComponent(cb3)
											.addComponent(cb1)
											.addComponent(cb2))))
								.addComponent(butt1)
								.addComponent(butt2))
							.addPreferredGap(ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(slider0, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(sliderLab0, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(slider3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(sliderLab3, GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(slider2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(sliderLab2, GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(slider1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(sliderLab1, GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)))
							.addGap(23)))
					.addGap(83))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(51)
					.addComponent(btnClear)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 677, Short.MAX_VALUE)
					.addGap(73))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(36)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(butt0)
								.addComponent(cb0))
							.addGap(34)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(butt1)
								.addComponent(cb1))
							.addGap(30)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(butt2)
								.addComponent(cb2))
							.addGap(27)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(butt3)
								.addComponent(cb3)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(slider0, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(sliderLab0))
							.addGap(34)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(slider1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(sliderLab1))
							.addGap(30)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(slider2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(sliderLab2))
							.addGap(27)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(sliderLab3)
								.addComponent(slider3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addGap(43)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(textInLabel)
						.addComponent(textIn, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(18)
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 257, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(70)
							.addComponent(btnClear)))
					.addContainerGap())
		);

		textOut = new JTextArea();
		textOut.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {
			}
		});
		scrollPane.setViewportView(textOut);
		textOut.setDisabledTextColor(Color.YELLOW);
		textOut.setForeground(Color.GREEN);
		textOut.setBackground(Color.BLACK);
		textOut.setLineWrap(true);
		textOut.setEnabled(true);
		getContentPane().setLayout(groupLayout);

		finalSetup(title);
	}

	void showTextOut(Boolean show)
	{
	  textOut.setVisible(show);
	  btnClear.setVisible(show);
	  scrollPane.setVisible(show);
	}

  private void finalSetup(String title) // condition the display
  {
    this.setTitle(title);
    this.setVisible(true);
    this.setSize(907,664);

// setup control arrays
    for (int i=0;i<4;i++){
      buttons[i]=new ButtonControl();
      sliders[i]=new SliderControl();
      cbs[i]=new CheckBoxControl();
    }

    // load controls into arrays
    buttons[0].button=butt0; buttons[1].button=butt1; buttons[2].button=butt2; buttons[3].button=butt3;
    cbs[0].cb=cb0; cbs[1].cb=cb1; cbs[2].cb=cb2; cbs[3].cb=cb3;
    sliders[0].slider=slider0; sliders[1].slider=slider1; sliders[2].slider=slider2; sliders[3].slider=slider3;
    sliderLabs[0]=sliderLab0; sliderLabs[1]=sliderLab1; sliderLabs[2]=sliderLab2; sliderLabs[3]=sliderLab3;
    textIns.textIn=textIn;

// hide controls for now
    for (int i=0;i<4;i++){
      buttons[i].button.setVisible(false);
      cbs[i].cb.setVisible(false);
      sliders[i].slider.setVisible(false);
      sliderLabs[i].setVisible(false);
    }
    textOut.setVisible(false);
    textIn.setVisible(false);
    textInLabel.setVisible(false);
  }

  /**
   * Set the visibility of the text output area
   * @param visible make text output area visible (true) or invisible (false)
   */
  public void setText(boolean visible)
  {
    showTextOut(visible); // need to display/hide textOut, button and scrollpane
  }

  /**
   * set text in the text output area
   * @param text string to display
   */
  public void setText(String text)
  {
    textOut.setText(text);
  }

  /**
   * Read the text area's text
   * @return a String containing the text
   */
  public String getText()
  {
    return(textOut.getText());
  }

  /**
   * append text to the text output window
   * @param text string to append to text output window
   */
  public void addText(String text)
  {
    textOut.setText(textOut.getText()+text);
  }

/** connect() argument for a button */
  public static final int Button=1;
/** connect() argument for a check box */
  public static final int CheckBox=2;
/** connect() argument for a slider */
  public static final int Slider=3;
/** connect() argument for text input area */
  public static final int TextIn=4;
private JScrollPane scrollPane;

  /**
   * display and connect a panel's control to a user-specified method
   * @param userObject usually "this": the object whose method should be called
   * @param type one of ControlPanel.Button, CheckBox, Slider or TextIn
   * @param number index (0-n) of the control
   * @param id user-specified integer to identify this control. Will be passed as the first argument to the method
   * @param label string to display on the button
   * @param method string containing the name of the method to call when the control changes.
   * method is called with a ControlArgs object as its single argument
   */
  public void connect(Object userObject, int type, int number, int id, String label, String method)
  {
    if (type==ControlPanel.Button){
      buttons[number].button.setText(label);
      buttons[number].button.setVisible(true);
      buttons[number].userID=id;
      buttons[number].userMethod=method;
      buttons[number].userObject=userObject;
    }
    if (type==ControlPanel.CheckBox){
      cbs[number].cb.setText(label);
      cbs[number].cb.setVisible(true);
      cbs[number].userID=id;
      cbs[number].userMethod=method;
      cbs[number].userObject=userObject;
    }
    if (type==ControlPanel.Slider){
      sliderLabs[number].setText(label);
      sliderLabs[number].setVisible(true);
      sliders[number].slider.setVisible(true);
      sliders[number].userID=id;
      sliders[number].userMethod=method;
      sliders[number].userObject=userObject;
    }
    if (type==ControlPanel.TextIn){
      textInLabel.setText(label);
      textInLabel.setVisible(true);
      textIn.setVisible(true);
      textIn.requestFocus();
      textIns.textIn.setVisible(true);
      textIns.userID=id;
      textIns.userMethod=method;
      textIns.userObject=userObject;
    }
  }

  // connect for slider with limits
    /**
   * display and connect a panel's control to a user-specified method
   * @param userObject usually "this": the object whose method should be called
   * @param type one of ControlPanel.Button, CheckBox, Slider or TextIn
   * @param number index (0-n) of the control
   * @param low lower limit of slider
   * @param high upper limit of slider
   * @param start initial starting value
   * @param id user-specified integer to identify this control. Will be passed as the first argument to the method
   * @param label string to display on the button
   * @param method string containing the name of the method to call when the control changes.
   * method is called with a ControlArgs object as its single argument
   */
  public void connect(Object userObject, int type, int number,
                      int start, int low, int high,
                      int id, String label, String method)
  {
    connect(userObject, type, number, id, label, method); // default method

      if (type==ControlPanel.Slider){
        sliders[number].slider.setMinimum(low);
        sliders[number].slider.setMaximum(high);
        sliders[number].slider.setValue(start);
    } else {
      System.out.println("low/high limits ignored for controls other than sliders");
    }
  }

// called when a button is pressed
  private void callButton(int index)
  {
	try {
		Method m=buttons[index].userObject.getClass().getMethod(buttons[index].userMethod, new Class[]{ControlArgs.class});
		m.invoke(buttons[index].userObject,new Object[]{new ControlArgs(buttons[index].userID,0,false,"")});
	} catch (Exception e) {
	  System.out.println("Error while trying to call " + buttons[index].userMethod);
	  e.printStackTrace();
	}
  }

// called when a checkbox is checked
  private void callcb(int index)
  {
	try {
		Method m=cbs[index].userObject.getClass().getMethod(cbs[index].userMethod, new Class[]{ControlArgs.class});
		m.invoke(cbs[index].userObject,new Object[]{new ControlArgs(cbs[index].userID,0,cbs[index].cb.isSelected(),"")});
	} catch (Exception e) {
	  System.out.println("Error while trying to call " + cbs[index].userMethod);
	  e.printStackTrace();
	}
  }

// called when a button is pressed
  private void callSlider(int index)
  {
	try {
		Method m=sliders[index].userObject.getClass().getMethod(sliders[index].userMethod, new Class[]{ControlArgs.class});
		m.invoke(sliders[index].userObject,new Object[]{new ControlArgs(sliders[index].userID,sliders[index].slider.getValue(),false,"")});
	} catch (Exception e) {
	  System.out.println("Error while trying to call " + sliders[index].userMethod);
	  e.printStackTrace();
	}
  }

// called when text is typed
  private void callTextIn(int index)
  {
	try {
		Method m=textIns.userObject.getClass().getMethod(textIns.userMethod, new Class[]{ControlArgs.class});
		m.invoke(textIns.userObject,new Object[]{new ControlArgs(textIns.userID,0,false,textIns.textIn.getText())});
	} catch (Exception e) {
      System.out.println("Error while trying to call " + textIns.userMethod);
	  e.printStackTrace();
	}
  }
}

// private class for storing info about control
class ButtonControl{
  JButton button; // the button object itself
  String userMethod; // name of the method to call when the button is pressed
  int userID; // user-specified ID code: passed as first argument to method
  Object userObject; // object whose method should be called
}

class SliderControl{
  JSlider slider; // the button object itself
  String userMethod; // name of the method to call when the button is pressed
  int userID; // user-specified ID code: passed as first argument to method
  Object userObject; // object whose method should be called
}

class CheckBoxControl{
  JCheckBox cb; // the button object itself
  String userMethod; // name of the method to call when the button is pressed
  int userID; // user-specified ID code: passed as first argument to method
  Object userObject; // object whose method should be called
}

class TextInControl{
  JTextField textIn; // the button object itself
  String userMethod; // name of the method to call when the button is pressed
  int userID; // user-specified ID code: passed as first argument to method
  Object userObject; // object whose method should be called
}