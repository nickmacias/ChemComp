package com.cellmatrix.EEXIST.API_V2;

// not used

public class NewCore extends Thread{
  EEXIST e; // save this
  Display d;

  NewCore(EEXIST e, Display d)
  {
    this.e=e;
    this.d=d;
  }

  public void run()
  {
    while (true){
      System.out.println("Reloading chemicals");
      newChem(e);

      for (int i=0;i<100;i++){
        while (d.simPaused) mySleep(100);
        e.step(1);
        d.updateLabels();
        mySleep(50);
      }
    }
  }

  private void newChem(EEXIST e)
  {
  // load some chemicals
    double maxX=e.getMaxX();
    double maxY=e.getMaxY();
    for (int i=0;i<50;i++){
      double xx=maxX*Math.random();
      double yy=maxY*Math.random();
      double r1=maxX*Math.random();
      double r2=maxY*Math.random();
      double r3=maxX*Math.random();
      double r4=maxY*Math.random();

      for (double x=xx;x<xx+2;x+=e.getDx()){
        for (double y=yy;y<yy+2;y+=e.getDy()){
          e.setTube(x,y,EEXIST.SRCX,r1);
          e.setTube(x,y,EEXIST.SRCY,r2);
          e.setTube(x,y,EEXIST.DSTX,r3);
          e.setTube(x,y,EEXIST.DSTY,r4);
        }
      }
    }
  }

  private void mySleep(int n)
  {
    try {
	Thread.sleep(n);
    } catch (InterruptedException e) {
	e.printStackTrace();
    }
  }
}
