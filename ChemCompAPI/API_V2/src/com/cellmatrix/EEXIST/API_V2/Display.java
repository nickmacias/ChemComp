package com.cellmatrix.EEXIST.API_V2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseMotionAdapter;

public class Display extends JFrame {

  private JPanel contentPane;
  private DisplayPanel drawingPanel;
  EEXIST e;
  private JPanel infoPanel;
  private JLabel locationLabel;
  private JLabel statusLabel;
  private JLabel modeLabel;
  public boolean simPaused=false; // set when paused
  static final int DIAM=1,CHEM=2,BIAS=3,ADD=4, KARMA=6; // possible display modes
  public int showMode=Display.CHEM;
  public boolean showSrc=true; // false means show dst
  double lastX=0,lastY=0; // loc of last mouse position (in EEXIST coords)

  private void processKeyPress(char c)
  {
    switch (c){
      case 'd': showMode=Display.DIAM;break; // dhow diameters
      case 'c': showMode=Display.CHEM;break; // display chemical levels
      case 'b': showMode=Display.BIAS;break; // display bias levels
      case 'a': showMode=Display.ADD;break; // display sum of chemical and bias levels
      case 'S': showSrc=true;break; // display SRC levels
      case 'D': showSrc=false;break; // display DST levels
      case 'k': showMode=Display.KARMA;break; // display karma*10
      case 'r': simPaused=!simPaused;break; // toggle between running and paused
      case '?': System.out.println("d: show diameters\nc: show chems\nb: show biases\na: show sum of chem and bias\nS: show SRC\nD: show DST\nk: show karma\nr: toggle pause/run\n?: help");
    }
  }

  public void update()
  {
    updateLabels(); // show new info
    drawingPanel.saveDisplay(this);
    drawingPanel.repaint();
  }

  /**
   * Launch the application.
   */

  public void updateLabels() // display useful info
  {

  // display status messages
    statusLabel.setText("Timestep: " + e.getTimeStep() + " (" + (simPaused?"Paused":"Running")+")");
    String text="Displaying ";
    switch(showMode){
      case Display.CHEM: text=text +(showSrc?"Src":"Dst") + " chemical levels";break;
      case Display.BIAS: text=text + (showSrc?"Src":"Dst") + " bias levels";break;
      case Display.DIAM: text=text + "diameters";break;
      case Display.KARMA: text=text + "karma";break;
      case Display.ADD: text=text + (showSrc?"Src":"Dst") + " sum (chem+bias) levels";break;
    }
    modeLabel.setText(text);

// now show detaikled information
    if (showMode==Display.CHEM){
      locationLabel.setText("Loc:(" +
                             String.format("%5.2f", lastX)+","+
                             String.format("%5.2f",lastY)+") SRC: (" +
                             String.format("%5.2f",e.getTube(lastX,lastY,EEXIST.SRCX)) + "," +
                             String.format("%5.2f",e.getTube(lastX,lastY,EEXIST.SRCY)) + ") DST: (" +
                             String.format("%5.2f",e.getTube(lastX,lastY,EEXIST.DSTX)) + "," +
                             String.format("%5.2f",e.getTube(lastX,lastY,EEXIST.DSTY)) + ")");
    } else if (showMode==Display.BIAS){
      locationLabel.setText("LOC:(" +
                             String.format("%5.2f", lastX)+","+
                             String.format("%5.2f",lastY)+") SRC: (" +
                             String.format("%5.2f",e.getBias(lastX,lastY,EEXIST.SRCX)) + "," +
                             String.format("%5.2f",e.getBias(lastX,lastY,EEXIST.SRCY)) + ") DST: (" +
                             String.format("%5.2f",e.getBias(lastX,lastY,EEXIST.DSTX)) + "," +
                             String.format("%5.2f",e.getBias(lastX,lastY,EEXIST.DSTY)) + ")");
    } else if (showMode==Display.DIAM){
      locationLabel.setText("LOC:(" +
                             String.format("%5.2f", lastX)+","+
                             String.format("%5.2f",lastY)+") Diam:(" +
                             String.format("%5.2f",e.getDiameter(lastX,lastY)));
    } else if (showMode==Display.KARMA){
      locationLabel.setText("LOC:(" +
                             String.format("%5.2f", lastX)+","+
                             String.format("%5.2f",lastY)+") KARMA: " +
                             String.format("%5.2f",e.getKarma(lastX,lastY)));
    } else if (showMode==Display.ADD){
      locationLabel.setText("LOC:(" +
                             String.format("%5.2f", lastX)+","+
                             String.format("%5.2f",lastY)+") SRC: (" +
                             String.format("%5.2f",e.getTube(lastX,lastY,EEXIST.SRCX) +e.getBias(lastX,lastY,EEXIST.SRCX)) + "," +
                             String.format("%5.2f",e.getTube(lastX,lastY,EEXIST.SRCY) +e.getBias(lastX,lastY,EEXIST.SRCY)) + ") DST: (" +
                             String.format("%5.2f",e.getTube(lastX,lastY,EEXIST.DSTX) +e.getBias(lastX,lastY,EEXIST.DSTX)) + "," +
                             String.format("%5.2f",e.getTube(lastX,lastY,EEXIST.DSTY) +e.getBias(lastX,lastY,EEXIST.DSTY)) + ")");
    }
  }

// generic sleep routine
    public static void sleep(int n)
    {
      try {
	Thread.sleep(n);
      } catch (InterruptedException e) {
	e.printStackTrace();
      }
    }

  /**
   * Create the frame.
   */
  public Display(EEXIST e) {
    this.e=e; // The main EEXIST associated with this display...
    e.saveDisplay(this); // and tell EEXIST about us
    setTitle("EEXIST");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 672, 504);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(new BorderLayout(0, 0));
    drawingPanel = new DisplayPanel(e);
    drawingPanel.addMouseMotionListener(new MouseMotionAdapter() {
    	@Override
    	public void mouseMoved(MouseEvent arg0) {
    	  if (drawingPanel.e==null) return;
    	  lastX=drawingPanel.fromXInt(arg0.getX());lastY=drawingPanel.fromYInt(arg0.getY());updateLabels();
    	}
    });
    drawingPanel.addMouseListener(new MouseAdapter() {
    	@Override
    	public void mouseClicked(MouseEvent e) {
    	  //System.out.println("Click");
    	  drawingPanel.setFocusable(true);
    	  drawingPanel.requestFocusInWindow();
    	}
    });
    drawingPanel.addKeyListener(new KeyAdapter() {
    	@Override
    	public void keyPressed(KeyEvent ke) {
    	  processKeyPress(ke.getKeyChar());
    	  drawingPanel.repaint();
    	  updateLabels();
    	}
    });
    drawingPanel.setForeground(Color.WHITE);
    drawingPanel.setBackground(Color.BLACK);
    contentPane.add(drawingPanel, BorderLayout.CENTER);

    infoPanel = new JPanel();
    contentPane.add(infoPanel, BorderLayout.NORTH);
                                            GridBagLayout gbl_infoPanel = new GridBagLayout();
                                            gbl_infoPanel.columnWidths = new int[]{71, 208, 0};
                                            gbl_infoPanel.rowHeights = new int[]{15, 15, 15, 0};
                                            gbl_infoPanel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
                                            gbl_infoPanel.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
                                            infoPanel.setLayout(gbl_infoPanel);

                                                                    statusLabel = new JLabel("Timestep 0: Paused");
                                                                    GridBagConstraints gbc_statusLabel = new GridBagConstraints();
                                                                    gbc_statusLabel.anchor = GridBagConstraints.NORTHWEST;
                                                                    gbc_statusLabel.insets = new Insets(0, 0, 5, 0);
                                                                    gbc_statusLabel.gridx = 1;
                                                                    gbc_statusLabel.gridy = 0;
                                                                    infoPanel.add(statusLabel, gbc_statusLabel);

                                                            modeLabel = new JLabel("Displaying Source Chemical Levels");
                                                            GridBagConstraints gbc_modeLabel = new GridBagConstraints();
                                                            gbc_modeLabel.anchor = GridBagConstraints.NORTHWEST;
                                                            gbc_modeLabel.insets = new Insets(0, 0, 5, 0);
                                                            gbc_modeLabel.gridx = 1;
                                                            gbc_modeLabel.gridy = 1;
                                                            infoPanel.add(modeLabel, gbc_modeLabel);
                                                            modeLabel.setHorizontalAlignment(SwingConstants.LEFT);

                                                locationLabel = new JLabel("Loc=(2,3) SRC=(x,y) DST=(x,y)");
                                                GridBagConstraints gbc_locationLabel = new GridBagConstraints();
                                                gbc_locationLabel.anchor = GridBagConstraints.NORTHWEST;
                                                gbc_locationLabel.gridx = 1;
                                                gbc_locationLabel.gridy = 2;
                                                infoPanel.add(locationLabel, gbc_locationLabel);
  }
}
