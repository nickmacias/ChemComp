package com.cellmatrix.EEXIST.API_V2;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class DisplayPanel extends JPanel{

  EEXIST e; // used for painting
  int drawingScale=5; // size of each dot
  Display display;

// constructor is given EEXIST object
  public DisplayPanel(EEXIST e)
  {
    this.e=e;
  }

  public void saveDisplay(Display display)
  {
    this.display=display;
  }

  public void paint(Graphics g)
  {
    super.paint(g);
    if ((e==null) || (display==null)) return; // haven't associated an EEXIST yet
    // show all chem levels
    for (double x=e.getMinX();x<e.getMaxX();x+=e.getDx()){
      for (double y=e.getMinY();y<e.getMaxY();y+=e.getDy()){

        int xi=toXInt(x);
        int yi=toYInt(y);

        double srcX=0, srcY=0, dstX=0, dstY=0;

// Decide what values to display
        if (display.showMode==Display.CHEM){ // show chemical levels
          srcX=e.getTube(x,y,EEXIST.SRCX);
          srcY=e.getTube(x,y,EEXIST.SRCY);
          dstX=e.getTube(x,y,EEXIST.DSTX);
          dstY=e.getTube(x,y,EEXIST.DSTY);
        } else if (display.showMode==Display.BIAS){ // show bias levels
          srcX=e.getBias(x,y,EEXIST.SRCX);
          srcY=e.getBias(x,y,EEXIST.SRCY);
          dstX=e.getBias(x,y,EEXIST.DSTX);
          dstY=e.getBias(x,y,EEXIST.DSTY);
        } else if (display.showMode==Display.KARMA){ // show karma at each point
          srcX=srcY=dstX=dstY=10*e.getKarma(x,y);
        } else if (display.showMode==Display.ADD){ // show sum of chemical and bias levels
          srcX=e.getTube(x,y,EEXIST.SRCX) + e.getBias(x,y,EEXIST.SRCX);
          srcY=e.getTube(x,y,EEXIST.SRCY) + e.getBias(x,y,EEXIST.SRCY);
          dstX=e.getTube(x,y,EEXIST.DSTX) + e.getBias(x,y,EEXIST.DSTX);
          dstY=e.getTube(x,y,EEXIST.DSTY) + e.getBias(x,y,EEXIST.DSTY);
        }
        int red,green,blue;
        if (display.showSrc){ // display source values
          red=(int)(255*srcX/e.getMaxX());
          blue=(int)(255*srcY/e.getMaxY());
          green=(int)(255*(dstX+dstY)/(e.getMaxX()+e.getMaxY()));
        } else {
          red=(int)(255*dstX/e.getMaxX());
          blue=(int)(255*dstY/e.getMaxY());
          green=(int)(255*(srcX+srcY)/(e.getMaxX()+e.getMaxY()));
        }
        green=0;

// special case for diameters
        if (display.showMode==Display.DIAM){
          green=(int)(e.getDiameter(x,y)*255);
          blue=red=0;
        }
        //if (red+blue != 0) System.out.println("("+xi+","+yi+":"+red+" "+blue);
        red=(red > 255)?255:red;
        green=(green > 255)?255:green;
        blue=(blue > 255)?255:blue;

        red=(red < 0)?0:red;
        green=(green < 0)?0:green;
        blue=(blue < 0)?0:blue;

        g.setColor(new Color(red,green,blue));
        g.fillRect(xi,yi,drawingScale,drawingScale);
      }
    }
  }

  /** find integer coordinate from EEXIST coordinate
   * @param x location in EEXIST system
   * @return location in drawing panel
   */
  public int toXInt(double x)
  {
    return((int)(drawingScale*(x/e.getDx())));
  }

  /** find integer coordinate from EEXIST coordinate
   * @param y location in EEXIST system
   * @return location in drawing panel
   */
  public int toYInt(double y)
  {
    return((int)(drawingScale*(y/e.getDy())));
  }

/** convert from drawing panel coords to EEXIST coords
 *  @param x location in drawing panel
 *  @return location in EEXIST coordinates
 */
  public double fromXInt(int x)
  {
    return(x*e.getDx()/drawingScale);
  }

/** convert from drawing panel coords to EEXIST coords
 *  @param x location in drawing panel
 *  @return location in EEXIST coordinates
 */
  public double fromYInt(int y)
  {
    return(y*e.getDy()/drawingScale);
  }

}
