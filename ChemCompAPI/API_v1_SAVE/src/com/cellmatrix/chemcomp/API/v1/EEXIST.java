package com.cellmatrix.chemcomp.API.v1;

import java.awt.Color;
import java.awt.Graphics;

// EEXIST - Egoless infinitE eXtended contInuous Space Time


/**
 * @author Nicholas J. Macias, Cell Matrix Corporation
 * @version 1.0
 * 
 * API for EEXIST (Egoless EXtened effect contInuous Space Time system)
 * <p>
 * A series of methods and fields for simulating the behavior of an EEXIST system
 * 
 * 
 */
public class EEXIST{

  /** index used to specify source chemical
   */
  public static int SRC=0;
  /** index used to specify destination chemical
   */
  public static int DST=1;

  protected int binMax;
  private double mem[][]; // will be constructed before first use
  private double[] diameters; // how freely chemicals flow from tube to tube
  private double memDelta[][]; // save changes in here, then apply all at once
  private double minX, maxX, dx; // fixed discretization
  private double dt=0.05; // timestep size
  private double karma=20; // range of impact
  private double maxVal=40,minVal=0;
  // TODO Allow setting of min and max vals
  private boolean volumeClip=false;
  private double totalVar=0; // sum of all variations since last reset
  private double variation;
  private boolean equilibriumFlow=false;
  private boolean sdFlow=true;
  // TODO Allow selectable SD or Equilibrium flow
  private boolean DEBUG=false; // set to print debug outputs
  private Display display=null; // points to display frame if available
  private Graphics localG; // paint here if we have a display
  private int timeStep=0; // tracks time units
  private double bias[][]; // fixed per-tube delta for SRC and DST

  /** chemical values specify absolute coordinates */
  public static final int Absolute=1;
  /** chemical values specify relative coordinates */
  public static final int Relative=2;
  private int xferMode=EEXIST.Absolute; // set to one of the above
  private boolean proportionalFlow=true; // set false for flow rate that's independent of the current chemical level
  
  /** constructor with no arguments uses minX=0, maxX=40, dx=0.0625
   */
  public EEXIST() // use default values
  {
	  this(0,40,.0625);
  }

/** main class for EEXIST system
 * @param minX smallest bin number
 * @param maxX largest bin number
 * @param dx spatial increment
 */
  public EEXIST(double minX, double maxX, double dx)
  {
	this.minX=minX;this.maxX=maxX;this.dx=dx; // these are fixed
    allocateMemory(); // allocate memory for storage of active individual...
  }
  
  /**
   * Control whether flow rate is based on current chemical level
   * @param state - set true to make flow rate proportional to current chemical level
   */
  public void proportionalFlow(boolean state)
  {
    proportionalFlow=state;
  }
  
  /** Set timestep for simulation (default=0.05)
   * @param dt specifies the timestep
   */
  public void setDt(double dt)
  {
    this.dt=dt;
  }
  
/** Set karma (interlinking of cause and effect) of transfers. Transfers centered at a location of x will also occur
 * nearby. The diameter of the transfer conduit drops off linearly,
 * from 1 (max) at the original coordinate, to 0 (min) at a distance of "karma."
 * @param karma is the spatial extent of the transfer's karma
 */
  public void setKarma(double karma)
  {
    this.karma=karma;
  }
  
  /**
   * returns the current karma of each transfer
   * @return current karma
   */
  public double getKarma()
  {
    return(karma);
  }
  
  /**
   * set all tube diameters from an array of values
   * @param diameters array of tube diameters [0,binMax]
   */
  public void setAllDiameters(double[] diameters)
  {
    for (int i=0;i<binMax;i++){
      this.diameters[i]=diameters[i];
    }
  }
  
  /**
   * set diameter of a single tube
   * @param tube integer tube number [0,binMax]
   * @param diameter diameter to set
   */
  public void setDiameter(int tube,double diameter)
  {
    diameters[tube]=diameter;
  }
  
  /**
   * set diameter of a tube biasd on its spatial coordinates
   * @param xLoc X location of the tube
   * @param diameter diameter to set
   */
  public void setDiameter(double xLoc,double diameter)
  {
    diameters[toBin(xLoc)]=diameter;
  }
  
  /**
   * set bias value for a given tube (integer index)
   * @param tube index of tube to set
   * @param sd EEXIST.SRC or EEXIST.DST
   * @param biasVal delta (default 0)
   */
  public void setBias(int tube, int sd, double biasVal)
  {
	  bias[tube][sd]=biasVal;
  }

  /**
   * set SRC/DST deltas (added to chemical levels to choose src/dst addresses)
   * @param xLoc spatial location of tube
   * @param sd EEXIST.SRC or SSXIST.DST
   * @param biasVal delta (default 0)
   */
  public void setBias(double xLoc, int sd, double biasVal)
  {
    bias[toBin(xLoc)][sd]=biasVal;
  }
  
  /**
   * load all bias values from 2D array
   * @param bias 2-D array of bias values: [x][SRC/DST]
   */
  public void setAllBiass(double[][] bias)
  {
    for (int i=0;i<1+binMax;i++){
      this.bias[i][0]=bias[i][0];
      this.bias[i][1]=bias[i][1];
    }
  }
  //TODO allow all biases to be set and read; also add to Individual
  
/** Set the mode for chemical transfers.
 * @param mode specifies the type of transfers that take place. Options are:
 * <p>
 * EEXIST.Absolute - values are treated as absolute spatial coordinates
 * <p>
 * EEXIST.Relative - values are treated as displacements from the current location (+/-)
 * This should be changed sometime to allow for negative displacements.
 */
 public void setTransferMode(int mode)
 {
   xferMode=mode;
 }
  
  /** Reset the internal timeclock to t=0 */
  public void resetTimeStep()
  {
    timeStep=0;
  }
  
  /**
   * How many simulated time steps have passed since the last reset
   * @return Number of time steps
   */
  public int getTimeStep()
  {
    return(timeStep);
  }
  
  /** read (fixed) minimum X value (tube 0) 
   * @return minimum X value
   */
  public double getMinX()
  {
	return(minX);
  }

  /** read (fixed) maximum X value (tube n) 
   * @return maximum X value
   */
  public double getMaxX()
  {
	return(maxX);
  }

  /** read (fixed) spatial increment (dx)
   * @return dx
   */
  public double getDx()
  {
	return(dx);
  }
  
  /** associate a display with this EEXIST. step() will record activity for the display to paint
   * @param d Display object to link to this EEXIST
   */
  public void saveDisplay(Display d)
  {
    display=d; // this display is linked to us (set to null to disconnect)
    localG=display.getImg().getGraphics(); // local graphics context
  }
  
  /**
   * de-associate a display from an EEXIST. The Display itself is not affected by this.
   */
  public void releaseDisplay()
  {
    display=null;
  }

// allocate memory for the bins and their delta containers
  private void allocateMemory()
  {
    binMax=(int)((maxX-minX)/dx);
    mem=new double[binMax+1][2]; // memory - holds all fluid levels
    memDelta=new double[binMax+1][2]; // will hold changes
    diameters=new double[1+binMax];
    bias=new double[1+binMax][2];
    for (int i=0;i<binMax+1;i++){
      diameters[i]=1.;
      bias[i][0]=bias[i][1]=0;
    }
  }
  
  /**
   * Return number of tubes
   * 
   * @return number of tubes
   */
  public int numTubes()
  {
    return(binMax+1);
  }

/**
 * load an individual into the system
 * @param indiv the individual to load. indiv's contents will completely
 * replace the contents of the EEXIST system's tubes
 */
  public void loadIndividual(Individual indiv)
  {
    for (int i=0;i<binMax+1;i++){
      mem[i][0]=indiv.mem[i][0];
      mem[i][1]=indiv.mem[i][1];
    }
  }
  
  /**
   * read the contents of the EEXIST system's tubes, and create a new individual
   * matching those concentrations
   * @return a new Individual
   */
  Individual readIndividual()
  {
    Individual indiv=new Individual(this);
    for (int i=0;i<binMax+1;i++){
      indiv.mem[i][0]=mem[i][0];
      indiv.mem[i][1]=mem[i][1];
    }
    return(indiv);
  }
  
/** set a tube's contents
 * 
 * @param tube tube's number (integer index)
 * @param SD EEXIST.SRC or EEXIST.DST
 * @param value total amount of SRC or DST chemical to store in tube
 */
  public void setTube(int tube, int SD, double value)
  {
	mem[tube][SD]=value;
  }
  

/** set a tube's contents
 * 
 * @param x tube's x coordinate
 * @param SD EEXIST.SRC or EEXIST.DST
 * @param value total amount of SRC or DST chemical to store in tube
 */
  public void setTube(double x, int SD, double value)
  {
	writeBin(x,SD,value);
  }
  
  /**
   * load all SRC/DST values from 2-D array
   * @param mem array of values. [x][SRC/DST]
   */
  public void setAllTubes(double[][] mem)
  {
    for (int i=0;i<1+binMax;i++){
      this.mem[i][0]=mem[i][0];
      this.mem[i][1]=mem[i][1];
    }
  }
  
  /** read the amount of SRC or DST chemical in a tube
   * @param x tube's x coordinate
   * @param SD EEXIST.SRC or EEXIST.DST
   * @return total amount of SRC or DST chemical in tube
   */
  public double getTube(double x, int SD)
  {
    return(readBin(x,SD));
  }
  
 /**
   * read all chemical levels
   * @return 2D array of chemical values, suitable for setAllTubes
   */
  public double[][] getAllTubes()
  {
    double[][] mem=new double[1+binMax][2];
    for (int i=0;i<1+binMax;i++){
      mem[i][0]=this.mem[i][0];
      mem[i][1]=this.mem[i][1];
    }
    return(mem);
  }

 /**
   * read all bias levels
   * @return 2D array of bias levels, suitable for setAllBiass
   */
  public double[][] getAllBiass()
  {
    double[][] bias=new double[1+binMax][2];
    for (int i=0;i<1+binMax;i++){
      bias[i][0]=this.bias[i][0];
      bias[i][1]=this.bias[i][1];
    }
    return(bias);
  }


  /** zero-out all tubes. This properly belongs more to the Individual class than the EEXIST class */
  public void clearAllTubes()
  {
    for (int i=0;i<binMax+1;i++){
      mem[i][0]=mem[i][1]=0;
    }
  }

  /** advance the simulation a number of steps
   * 
   * @param steps number of timesteps (dt each)
   */
  public void step(int steps)
  {
	for (int i=0;i<steps;i++){
      step();
	}
    ++timeStep; // track time since last resetTimeStep()
    if (display != null) localPaint(); // update display if available
  }
  
// advance the simulation one time step
// scale by dt, but also by karma:
//   karma controls how widespread the change is felt...
//     if karma~0, then most change occurs near src
//     if karma~1, then the change occurs ~everywhere!
//
// delta M(src+dx) = -M(src+dx)*dt*(karma^dx)
// delta M(dst+dx) =  M(src+dx)*dt*(karma^dx)
  
  private void step()
  {
    double x=minX;
    double src,dst;
    initMemDelta(); // zero-out our delta arrays (then add cumulative karmas)
    for (int i=0;i<=binMax;i++){
// "x" is the location of this instruction...
      src=mem[i][EEXIST.SRC]+bias[i][EEXIST.SRC];
      dst=mem[i][EEXIST.DST]+bias[i][EEXIST.DST];
      if (xferMode==EEXIST.Absolute){
        transfer(src,dst); // add up the karmas of this single src->dst command
      } else { // src/dst are relative bin IDs
   //TODO add an offset etc. to transfer code to allow negative relative displacements
        double x1,x2;
        x1=x+src;x2=x+dst;
        if (x1 > maxX) x1=x1-maxX; //%%%
        if (x2 > maxX) x2=x2-maxX;
        if (x1 < 0) x1+=maxX;
        if (x2 < 0) x2+=maxX;
        transfer(x1,x2);
        x1=x-src;x2=x-dst;
        if (x1 > maxX) x1=x1-maxX;
        if (x2 > maxX) x2=x2-maxX;
        if (x1 < 0) x1+=maxX;
        if (x2 < 0) x2+=maxX;
        transfer(x1,x2);
      }
      x=x+dx; // track the current location
    } // do this across the entire space

/***
// conservative?
double all=0;
for (int i=0;i<=640;i++) all=all+memDelta[i][0];
System.out.println("all=" + all);
***/

// all transfers have been tallied...now adjust the memory
// Make sure tubes don't overflow or underflow
    for (int i=0;i<=binMax;i++){
      mem[i][EEXIST.SRC]+=memDelta[i][EEXIST.SRC];
      mem[i][EEXIST.DST]+=memDelta[i][EEXIST.DST];
      variation+=Math.abs(memDelta[i][EEXIST.SRC])+Math.abs(memDelta[i][EEXIST.DST]); // sum the total change
//variation+=Math.abs(mem[i][EEXIST.SRC]-mem[i][EEXIST.DST]); // sum the total pressure
      if (volumeClip){
      // just clip for now
        if (mem[i][EEXIST.SRC] < minVal) mem[i][EEXIST.SRC]=minVal;
        if (mem[i][EEXIST.SRC] > maxVal) mem[i][EEXIST.SRC]=maxVal;
        if (mem[i][EEXIST.DST] < minVal) mem[i][EEXIST.DST]=minVal;
        if (mem[i][EEXIST.DST] > maxVal) mem[i][EEXIST.DST]=maxVal;
      }
    }

    totalVar+=variation;
    return;
  } // end of step()

// transfer() computes the karma of a single src->dst transfer
// the karma is not only on src and dst, but also (potentially) on nearby bins
  private void transfer(double src, double dst)
  {
    if (DEBUG) System.out.println(src + "-->" + dst);
//
// NOTE: You need to multiply by dx!
// You're integrating (fool!) so as the bins become more numerous and shrink, the contribution from
// summing each bin needs to shrink accordingly...
//
// if karma=0, only transfer src->dst
    //if (toBin(src) < 0) return;
    //if (toBin(dst) < 0) return;

    connect(src,dst,dt,1); // connect these two bins with diameter=1.0
    if (karma != 0){
      for (double d=dx;d<karma;d+=dx){ // tally these karmas
        connect(src+d,dst+d,dt,(karma-d)/karma);
        connect(src-d,dst-d,dt,(karma-d)/karma); // diameter runs from 1 down to 0 as d runs from 0 to karma...
      }
    } // end of distributed-karma processing

// All deltas computed for this particular transfer
  }

// connect() temporarily joins two bins (b1 and b2) and allows transfer of fluid through a connection
// of the given diameter, for a time of dt
// While connected, the S and D fluids move towards equilibrium
//
  private void connect(double db1, double db2, double dt, double diameter)
  {
    int b1=toBin(db1);
    int b2=toBin(db2); // connect these
    
// if b1 and b2 are the same, this should be a NOP
// but the code below will calculate working deltas for SRC and DST (-X, +X)
// and then (after sanity checking) assign delta=-X and then delta=+X
// which leads to saturation potential

    if (b1==b2) return;

// WRAP THESE
    while (b1<0) b1+=binMax;
    while (b2<0) b2+=binMax;
    while (b1 > binMax) b1-=binMax;
    while (b2 > binMax) b2-=binMax;
    //if ((b1<0)||(b2<0)) return;
    //if ((b1>binMax)||(b2>binMax)) return;
    //b1=(int)(100+100*Math.random());
    //b2=(int)(100+100*Math.random()); if (b2==b1)++b2;

    if (DEBUG) System.out.println(b1 +":"+b2+"("+dt+","+diameter+")");
    if (equilibriumFlow){ // fluids flow to reach equilibrium
      // so for example, if mem[b1]=60 and mem[b2]=20, then b1 flows to b2 with a rate of 40...
      memDelta[b1][0]-=(mem[b1][0]-mem[b2][0])*dt*diameter*diameters[b1]*diameters[b2]*dx; // but if b1 is smaller, this is negative flow from b1 (i.e. TO b1)
      memDelta[b2][0]+=(mem[b1][0]-mem[b2][0])*dt*diameter*diameters[b1]*diameters[b2]*dx;
      memDelta[b1][1]-=(mem[b1][1]-mem[b2][1])*dt*diameter*diameters[b1]*diameters[b2]*dx;
      memDelta[b2][1]+=(mem[b1][1]-mem[b2][1])*dt*diameter*diameters[b1]*diameters[b2]*dx;
      return;
    }
    if (sdFlow){ // fluids flow strictly from src to dst
// see what the new memory changes should be
      double m10,m11,m20,m21;
      if (proportionalFlow){ // flow rate depends on current level
        m10=memDelta[b1][0]-mem[b1][0]*dt*diameter*diameters[b1]*diameters[b2]*dx;
        m11=memDelta[b1][1]-mem[b1][1]*dt*diameter*diameters[b1]*diameters[b2]*dx;
        m20=memDelta[b2][0]+mem[b1][0]*dt*diameter*diameters[b1]*diameters[b2]*dx;
        m21=memDelta[b2][1]+mem[b1][1]*dt*diameter*diameters[b1]*diameters[b2]*dx;
      } else {
        m10=memDelta[b1][0]-20*dt*diameter*diameters[b1]*diameters[b2]*dx;
        m11=memDelta[b1][1]-20*dt*diameter*diameters[b1]*diameters[b2]*dx;
        m20=memDelta[b2][0]+20*dt*diameter*diameters[b1]*diameters[b2]*dx;
        m21=memDelta[b2][1]+20*dt*diameter*diameters[b1]*diameters[b2]*dx; // 20 is sort of random, but speeds things up?
      }

// and if a delta is still legal, update memDelta
      if ((mem[b1][0]+m10 >= 0) && (mem[b2][0]+m20 <= maxVal)){
        memDelta[b1][0]=m10;memDelta[b2][0]=m20;
      }
      if ((mem[b1][1]+m11 >= 0) && (mem[b2][1]+m21 <= maxVal)){
        memDelta[b1][1]=m11;memDelta[b2][1]=m21;
      }
    }
  }

// bin read/write routines
  private void writeBin(double x,int sd,double value)
  {
    writeBin(toBin(x),sd,value);
  }
  private void writeBin(int bin,int sd,double value)
  {
    mem[bin][sd]=value;
  }
  private double readBin(double x,int sd)
  {
    return(readBin(toBin(x),sd));
  }
  private double readBin(int bin,int sd)
  {
    return(mem[bin][sd]);
  }

// convert from double (x) to int (bin)
  private int toBin(double x) // convert real # to bin #
  {
    int retVal=(int)((x-minX)/dx);
    //if (retVal>binMax) retVal=(-1); // 20170614
    //if (retVal < 0) retVal=(-1); // 20170614
    return(retVal%(binMax+1)); // rollover from 0 to binMax
  }

  private void initMemDelta()
  {
    double x=minX;
    for (int i=0;i<=binMax;i++){
      memDelta[i][EEXIST.SRC]=memDelta[i][EEXIST.DST]=0;
      x=x+dx; // move along
    }
  }

 /**
  * Draw into buffered image, then display in main display window
  * Also update graphFrame (detail display)
  */
 public void localPaint() // draw on our bufferedImage
 {
   if (display==null) return;
//displace y by timesteps; x by memory bins. Color = combo of src(red)+dst(green)
   for (int x=0;x<=binMax;x++){
     double src=mem[x][EEXIST.SRC];
     double dst=mem[x][EEXIST.DST];
     int red=(int)(src*display.getIntensity());
     int green=0;
     int blue=(int)(dst*display.getIntensity());
     if (red > 255) red=255;if (red < 0) red=0;
     if (blue > 255) blue=255;if (blue < 0) blue=0;
     Color c=new Color(red,green,blue); // set color by blending src and dst concentrations
     localG.setColor(c);
     localG.fillRect(x*display.scaleX,(timeStep*display.scaleY)%display.numStepsOnDisplay,display.scaleX,display.scaleY);
   }
   if (display.panel != null) display.panel.repaint(); // this will repaint the DrawingPanel
 // if we're doing a live update, call it here
   if (display.gf != null){
     if (display.gf.liveUpdates()){
       display.gf.setup(mem,diameters,bias,(int)(minX/dx), (int)(maxX/dx), timeStep%display.numStepsOnDisplay);
       display.gf.repaint();
     }
   }
 }
 
}