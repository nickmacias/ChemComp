package com.cellmatrix.chemcomp8;

// load individual
// step()
// reset()
// saveHist()?
// readBin(x,t), writeBin(...) (double or int)
//
// allocate memory
// allocate memory
// seed memory (loadMem(genome))
// step simulation (step(dt))
// update display(s) based on list? of displays/display modes

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

// main class for transfer-based chemical computer
// EXIST - Egoless eXtended contInuous Space Time system
//

public class EXIST extends JPanel{
  private static final long serialVersionUID = -9025412994581896991L;
  int binMax;
  double mem[][]; // will be constructed before first use
  double memSave[][]; // will be constructed before first use
  double memDelta[][]; // save changes in here, then apply all at once
  double flowRates[]; // save flow rate (0-1) for each bin
  double allBins[][][]; // [x][s/d][t] save of bins on display...
  int data[][][]; // [row][col][S/D]
  boolean equilibriumFlow=false;
  boolean sdFlow=true;
  double totalVar=0; // sum of all variations since last reset
  boolean snapshotValid=false;
  double snapshot[][]; // this is our snapshot
  double snapshotTime=0; // t of this snapshot
  double snapshotMinCycle=0;


  Universe universe;
  Graphics localG;

  public EXIST(Universe u)
  {
    universe=u;
    localG=universe.getDisplay().getImg().getGraphics();
    allocateMemory(); // allocate memory for storage of active individual...
  }

// do graphics on a local image
  public void localPaint() // draw on our bufferedImage
  {
// displace y by timesteps; x by memory bins. Color = combo of src(red)+dst(green)
    for (int x=0;x<=binMax;x++){
      double src=mem[x][C.S];
      double dst=mem[x][C.D];
      int red=(int)(src*C.rgbScale);
      int green=0;
      int blue=(int)(dst*C.rgbScale);
      if (red > 255) red=255;if (red < 0) red=0;
      if (blue > 255) blue=255;if (blue < 0) blue=0;
      Color c=new Color(red,green,blue); // set color by blending src and dst concentrations
      localG.setColor(c);
      localG.fillRect(x*C.scaleX,(C.timeStep*C.scaleY)%C.numStepsOnDisplay,C.scaleX,C.scaleY);
      allBins[x][0][C.timeStep%C.numStepsOnDisplay]=mem[x][0];
      allBins[x][1][C.timeStep%C.numStepsOnDisplay]=mem[x][1];
    }
    // show some bin boundaries
    localG.setColor(Color.GREEN);
    for (int x=0;x<=40;x+=5){
      localG.drawLine(toBin(x)*C.scaleX,0,toBin(x)*C.scaleX,1000);
    }
    universe.getDisplay().repaint();

    // if we're doing a live update, call it here
    if (universe.gf.liveUpdates()){
      universe.gf.setup(universe.exist.allBins,0,(int)(C.maxX/C.dx),C.timeStep%C.numStepsOnDisplay);
      universe.gf.repaint();
    }
  }

// allocate memory for the bins and their delta containers
  public void allocateMemory()
  {
    binMax=(int)((C.maxX-C.minX)/C.dx);
    mem=new double[binMax+1][2]; // memory - holds all fluid levels
    memSave=new double[binMax+1][2]; // memory - holds all fluid levels
    memDelta=new double[binMax+1][2]; // will hold changes
    allBins=new double[binMax+1][2][C.numStepsOnDisplay];
    snapshot=new double[binMax+1][2]; // copy of an earlier state
    flowRates=new double[binMax+1]; // how quickly fluid flows *from* a bin
    //data=new int[(int)(C.maxTime/C.dt)][binMax][2]; // store record of S/D values across time in this array
// only use this when we're in STIMULATE mode

// clear graphics
    localG.setColor(Color.BLACK);
    localG.fillRect(0,0,1000,1000);
    repaint();

    C.timeStep=0;
  }

// load flow rates
  public void loadFlowRates()
  {
    for (int i=0;i<=binMax;i++){
      flowRates[i]=1.; // fixed here
    }
  }

// seed bins from outside
  public void loadBins(double in[][])
  {
    for (int i=0;i<=binMax;i++){
      memSave[i][C.S]=mem[i][C.S]=in[i][C.S];
      memSave[i][C.D]=mem[i][C.D]=in[i][C.D]; // this saves a copy for restoring between runs
      allBins[i][C.S][0]=mem[i][C.S];
      allBins[i][C.D][0]=mem[i][C.D]; // save first row!
    }
    C.timeStep=0;
  }

// advance the simulation one time step
// scale by dt, but also by effect
//   effect controls how widespread the change is felt...
//     if effect~0, then most change occurs near src
//     if effect~1, then the change occurs ~everywhere!
//
// delta M(src+dx) = -M(src+dx)*dt*(effect^dx)
// delta M(dst+dx) =  M(src+dx)*dt*(effect^dx)
  public double step(boolean finalMeasure)
  {
    double variation=0; // tally total change
    double x=C.minX;
    double src,dst;

    ++C.timeStep;

    initMemDelta(); // zero-out our delta arrays (then add cumulative effects)
    for (int i=0;i<=binMax;i++){
// "x" is the location of this instruction...
      src=mem[i][C.S];
      dst=mem[i][C.D];
      if (C.ABSOLUTEXFER){
        transfer(src,dst,flowRates[i]); // add up the effects of this single src->dst command
      } else { // src/dst are relative bin IDs
        double x1,x2;
        x1=x+src;x2=x+dst;
        if (x1 > C.maxX) x1=x1-C.maxX; //%%%
        if (x2 > C.maxX) x2=x2-C.maxX;
        if (x1 < 0) x1+=C.maxX;
        if (x2 < 0) x2+=C.maxX;
        transfer(x1,x2,flowRates[i]);
        x1=x-src;x2=x-dst;
        if (x1 > C.maxX) x1=x1-C.maxX;
        if (x2 > C.maxX) x2=x2-C.maxX;
        if (x1 < 0) x1+=C.maxX;
        if (x2 < 0) x2+=C.maxX;
        transfer(x1,x2,flowRates[i]);
      }
      x=x+C.dx; // track the current location
    } // do this across the entire space

// all transfers have been tallied...now adjust the memory
// Make sure tubes don't overflow or underflow
    for (int i=0;i<=binMax;i++){
      mem[i][C.S]+=memDelta[i][C.S];
      mem[i][C.D]+=memDelta[i][C.D];
       if (finalMeasure){
         if (i < binMax/8) variation+=memDelta[i][C.S]+memDelta[i][C.D]; // actual change (+/-), only first first 1/4 of bins
       } else {
         variation+=Math.abs(memDelta[i][C.S])+Math.abs(memDelta[i][C.D]); // sum the total change
       }
       //variation+=Math.abs(mem[i][C.S]-mem[i][C.D]); // sum the total pressure
      if (C.VOLUMECLIP){
      // just clip for now
        if (mem[i][C.S] < 0) mem[i][C.S]=0;
        if (mem[i][C.S] > C.MAXVAL) mem[i][C.S]=C.MAXVAL;
        if (mem[i][C.D] < 0) mem[i][C.D]=0;
        if (mem[i][C.D] > C.MAXVAL) mem[i][C.D]=C.MAXVAL;
      }
    }

// draw bin's contents
    localPaint();
    repaint();

    totalVar+=variation;
    return(totalVar);
  } // end of step()

// transfer() computes the effect of a single src->dst transfer
// the effect is not only on src and dst, but also (potentially) on nearby bins
  public void transfer(double src, double dst, double diam)
  {
    if (C.debug) System.out.println(src + "-->" + dst);
//
// NOTE: You need to multiply by dx!
// You're integrating (fool!) so as the bins become more numerous and shrink, the contribution from
// summing each bin needs to shrink accordingly...
//
// if effect=0, only transfer src->dst
    //if (toBin(src) < 0) return;
    //if (toBin(dst) < 0) return;

    connect(src,dst,C.dt,diam); // connect these two bins with diameter=1.0
    if (C.effect != 0){
      for (double d=C.effect/25;d<C.effect;d+=(C.effect)/25){ // tally these effects
        connect(src+d,dst+d,C.dt,diam*(C.effect-d)/C.effect);
        connect(src-d,dst-d,C.dt,diam*(C.effect-d)/C.effect); // diameter runs from 1 down to 0 as d runs from 0 to effect...
      }
    } // end of distributed-effect processing

// All deltas computed for this particular transfer
  }

// connect() temporarily joins two bins (b1 and b2) and allows transfer of fluid through a connection
// of the given diameter, for a time of dt
// While connected, the S and D fluids move towards equilibrium
//
  void connect(double db1, double db2, double dt, double diameter)
  {
    int b1=toBin(db1);
    int b2=toBin(db2); // connect these
    //b1=(b1+640)%320;
    //b2=(b2+640)%320; // delete these %%%
    if ((b1<0)||(b2<0)) return;
    //%%% WRAP THESE!!!
    if (C.debug) System.out.println(b1 +":"+b2+"("+dt+","+diameter+")");
    if (equilibriumFlow){ // fluids flow to reach equilibrium
      // so for example, if mem[b1]=60 and mem[b2]=20, then b1 flows to b2 with a rate of 40...
      memDelta[b1][0]-=(mem[b1][0]-mem[b2][0])*dt*diameter*C.dx; // but if b1 is smaller, this is negative flow from b1 (i.e. TO b1)
      memDelta[b2][0]+=(mem[b1][0]-mem[b2][0])*dt*diameter*C.dx;
      memDelta[b1][1]-=(mem[b1][1]-mem[b2][1])*dt*diameter*C.dx;
      memDelta[b2][1]+=(mem[b1][1]-mem[b2][1])*dt*diameter*C.dx;
      return;
    }
    if (sdFlow){ // fluids flow strictly from src to dst
      memDelta[b1][0]-=mem[b1][0]*dt*diameter*C.dx;
      memDelta[b1][1]-=mem[b1][1]*dt*diameter*C.dx;
      memDelta[b2][0]+=mem[b1][0]*dt*diameter*C.dx;
      memDelta[b2][1]+=mem[b1][1]*dt*diameter*C.dx;
    }
  }

// bin read/write routines
  public void writeBin(double x,int sd,double value)
  {
    writeBin(toBin(x),sd,value);
  }
  public void writeBin(int bin,int sd,double value)
  {
    mem[bin][sd]=value;
  }
  public double readBin(double x,int sd)
  {
    return(readBin(toBin(x),sd));
  }
  public double readBin(int bin,int sd)
  {
    return(mem[bin][sd]);
  }

// convert from double (x) to int (bin)
  int toBin(double x) // convert real # to bin #
  {
    int retVal=(int)((x-C.minX)/C.dx);
    if (retVal>binMax) retVal=(-1);
    if (retVal < 0) retVal=(-1);
    return(retVal%(binMax+1)); // rollover from 0 to binMax
  }

  public void initMemDelta()
  {
    double x=C.minX;
    for (int i=0;i<=binMax;i++){
      memDelta[i][C.S]=memDelta[i][C.D]=0;
      x=x+C.dx; // move along
    }
  }

  // snapshot code
  // used to detect cycles by making a copy of the current bins' contents, and then
  // looking for this same pattern in the future.
  //

  // just clear the snapshotValid flag
    void clearSnapshot() //
    {
      snapshotValid=false;
      universe.control.lblCF.setText("");
    }

  // record a snapshot
    void createSnapshot()
    {
      snapshotValid=true;
      snapshotMinCycle=Double.POSITIVE_INFINITY;
      snapshotTime=C.timeStep; // save snapshot time
      totalVar=0;
      for (int i=0;i<=binMax;i++){
        snapshot[i][0]=mem[i][0];
        snapshot[i][1]=mem[i][1];
      }
    }

  double activity(int start, int end) // tally bin contents between these bins
  {
    double sum=0.;
    for (int i=start;i<=end;i++){
      sum+=mem[i][0]+mem[i][1];
    }
    return(sum);
  }

// return difference from snapshot...
  double getCurrentSnapshotDiff()
  {
    double diff=0;
    for (int i=0;i<=20+0*binMax;i++){
      diff+=Math.abs(snapshot[i][0]-mem[i][0])+Math.abs(snapshot[i][1]-mem[i][1]);
    }
    return(diff);
  }

  // compare current state to saved snapshot
    void compareSnapshot()
    {
      if (!snapshotValid) return;
      double diff=0;
      for (int i=0;i<=binMax;i++){
        diff+=Math.abs(snapshot[i][0]-mem[i][0])+Math.abs(snapshot[i][1]-mem[i][1]);
      }
      if (diff < snapshotMinCycle){
        //System.out.println("diff=" + diff + "  smc=" + snapshotMinCycle);
        snapshotMinCycle=diff;
      }
      universe.control.lblCF.setText("CF: " + snapshotMinCycle);
    }

  // return snapshot-related variables
    double getSnapshotDiff()
    {
      return(snapshotMinCycle);
    }

    double getSnapshotVar() // later - return total variation so we know how much things changed during this cycle
    {
      return(totalVar);
    }

}