package com.cellmatrix.chemcomp8;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import java.awt.event.InputEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import java.awt.Color;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

@SuppressWarnings("serial")
public class GraphFrame extends JFrame {
  private JPanel contentPane;
  Universe universe;
  MyScrollPane scrollPane;
  int startingX,startingY; // set when the mouse is clicked
  JCheckBox chckbxUpdateLive;
  JToggleButton tglbtnFlipRb;

  public boolean liveUpdates()
  {
    return(chckbxUpdateLive.isSelected());
  }

  // save access to universe
  public void saveUniverse(Universe u)
  {
    universe=u;
  }

  public GraphFrame() {
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 691, 161);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);

    scrollPane = new MyScrollPane();
    scrollPane.setBackground(Color.DARK_GRAY);
    scrollPane.addMouseListener(new MouseAdapter() {
    	@Override
    	public void mousePressed(MouseEvent arg0) {
    	// mouse button pressed - record current (x,y) location so we can update during drags
    	startingX=arg0.getX();
    	startingY=arg0.getY();
    	}
    });
    scrollPane.addMouseMotionListener(new MouseMotionAdapter() {
    	@Override
    	public void mouseDragged(MouseEvent arg0) {
          scrollPane.shift(startingX-arg0.getX(),startingY-arg0.getY());
          startingX=arg0.getX();
          startingY=arg0.getY();
    	}
    	@Override
    	public void mouseMoved(MouseEvent arg0) {
    	  double bin=(arg0.getX()+scrollPane.xStart)/scrollPane.xZF;
    	  bin=bin*C.dx;
    	  if (bin < 0) bin=0;
    	  if (bin >= C.maxX) bin=C.maxX-C.dx;
    	  setTitle("Bin x=" + bin +
    	           " y=" + (arg0.getY()+scrollPane.yStart)/scrollPane.yZF +
    	           "  S=" + scrollPane.s[universe.exist.toBin(bin)] +
    	           "  D=" + scrollPane.d[universe.exist.toBin(bin)]);
    	}
    });
    scrollPane.addMouseWheelListener(new MouseWheelListener() {
    	public void mouseWheelMoved(MouseWheelEvent arg0) {
    	  scrollPane.zoom(arg0.getWheelRotation(),(0!=(arg0.getModifiers()&InputEvent.SHIFT_MASK))); // -1 means zoom in +1 means zoom out
    	  //System.out.println("mod=" + arg0.getModifiers());
    	  repaint();
    	}
    });

    JButton btnResetScroll = new JButton("Reset Scroll");
    btnResetScroll.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent arg0) {
    	  shiftReset();
    	}
    });

    JButton btnResetZoom = new JButton("Reset Zoom");
    btnResetZoom.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent arg0) {
    	  scrollPane.xZF=scrollPane.yZF=1.;
    	  repaint();
    	}
    });

    chckbxUpdateLive = new JCheckBox("Update Live");
    chckbxUpdateLive.setSelected(true);

    tglbtnFlipRb = new JToggleButton("Flip R/B");
    tglbtnFlipRb.addChangeListener(new ChangeListener() {
    	public void stateChanged(ChangeEvent arg0) {
    	  scrollPane.flip=tglbtnFlipRb.isSelected();
    	}
    });
    GroupLayout gl_contentPane = new GroupLayout(contentPane);
    gl_contentPane.setHorizontalGroup(
    	gl_contentPane.createParallelGroup(Alignment.LEADING)
    		.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)
    		.addGroup(gl_contentPane.createSequentialGroup()
    			.addComponent(btnResetScroll)
    			.addGap(18)
    			.addComponent(btnResetZoom)
    			.addGap(110)
    			.addComponent(chckbxUpdateLive)
    			.addGap(40)
    			.addComponent(tglbtnFlipRb)
    			.addGap(115))
    );
    gl_contentPane.setVerticalGroup(
    	gl_contentPane.createParallelGroup(Alignment.LEADING)
    		.addGroup(gl_contentPane.createSequentialGroup()
    			.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
    				.addComponent(btnResetScroll)
    				.addComponent(btnResetZoom)
    				.addComponent(chckbxUpdateLive)
    				.addComponent(tglbtnFlipRb))
    			.addPreferredGap(ComponentPlacement.RELATED)
    			.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
    			.addGap(0))
    );
    contentPane.setLayout(gl_contentPane);
  }

  public void shiftReset()
  {
    scrollPane.shiftReset();
  }

// prepare to paint
  public void setup(double allBins[][][],int xMi,int xMa,int time)
  {
    scrollPane.xMax=xMa;
    scrollPane.xMin=xMi;

    scrollPane.s=new double[scrollPane.xMax];
    scrollPane.d=new double[scrollPane.xMax];

    double thisY,maxY=0;

    for (int x=scrollPane.xMin;x<scrollPane.xMax;x++){
      scrollPane.s[x]=allBins[x][C.S][time];
      scrollPane.d[x]=allBins[x][C.D][time];
      thisY=scrollPane.s[x]+scrollPane.d[x];
      if (thisY > maxY) maxY=thisY;
    }
    scrollPane.maxY=maxY;
    //scrollPane.setSize(scrollPane.xMax-scrollPane.xMin,(int)(maxY));
  }
}
