package com.cellmatrix.chemcomp8;

// allows adjustment of: maxtime, effect, dt, dx(1),maxx(1),mutation rate, survivor rate, pop size(2)
// (1) need to re-allocate memory in EEXIST
// (2) need to re-allocate memory in Universe
// can select Evolve or Pause/Stimulate (can interact with live individual or pick a member and simulate/interact with)
// save/load
// examine/modify bins
//
// plus display options: show live individual; show top n individuals
//

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JFileChooser;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import javax.swing.JLabel;
import javax.swing.JScrollBar;

import java.awt.event.AdjustmentListener;
import java.awt.event.AdjustmentEvent;

import javax.swing.JCheckBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.border.MatteBorder;

import java.awt.Color;

import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;

public class Control extends JFrame {
  private static final long serialVersionUID = 1L;
  JCheckBox loopCheckButton;
  Universe universe;
  JScrollBar RGBscrollBar;
  JLabel lblCF;
  boolean loadRequested=false;
  boolean saveRequested=false;
  JCheckBox chkSD;
  JCheckBox chkGenome;
  JCheckBox chkConstants;
  JCheckBox chckbxConstantLogging;
  JCheckBox chckbxExpire;

  public boolean loadRequest()
  {
    return(loadRequested);
  }

  public boolean saveRequest()
  {
    return(saveRequested);
  }

  private JPanel contentPane;
  private final ButtonGroup runModeGroup = new ButtonGroup();
  private JTextField textdt;
  private JTextField textMutRate;
  private JTextField textField_2;
  private JTextField textField_3;
  private JTextField textEffect;
  private JSlider sliderBins;
  private final ButtonGroup sdXferGroup = new ButtonGroup();
  public Control(Universe u) {
    universe=u;
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 787, 468);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);

    JPanel panel = new JPanel();
    panel.setBounds(501, 276, 203, 91);
    panel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));

    JPanel panel_1 = new JPanel();
    panel_1.setBounds(308, 18, 188, 248);
    panel_1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));

    JPanel panel_2 = new JPanel();
    panel_2.setBounds(542, 18, 129, 180);
    panel_2.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));

    JPanel panel_3 = new JPanel();
    panel_3.setBounds(10, 18, 173, 244);
    panel_3.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));

    JPanel panel_4 = new JPanel();
    panel_4.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
    panel_4.setBounds(136, 289, 238, 78);

    JPanel panel_5 = new JPanel();
    panel_5.setBorder(new TitledBorder(new CompoundBorder(null, UIManager.getBorder("CheckBoxMenuItem.border")), "RGB", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
    panel_5.setBounds(213, 18, 45, 235);
    panel_5.setLayout(null);

            RGBscrollBar = new JScrollBar();
            RGBscrollBar.setBounds(10, 21, 17, 190);
            panel_5.add(RGBscrollBar);
            RGBscrollBar.addAdjustmentListener(new AdjustmentListener() {
            	public void adjustmentValueChanged(AdjustmentEvent arg0) {
            	  C.rgbScale=arg0.getValue();
            	  //System.out.println("rgbScale=" + C.rgbScale);
            	}
            });
            RGBscrollBar.setVisibleAmount(0);
            RGBscrollBar.setMaximum(256);
    panel_4.setLayout(null);

        JLabel lblNewLabel_2 = new JLabel("# Initial Bins:");
        lblNewLabel_2.setBounds(35, 11, 82, 14);
        panel_4.add(lblNewLabel_2);

            JLabel labelBins = new JLabel("2");
            labelBins.setBounds(107, 11, 42, 14);
            panel_4.add(labelBins);

                sliderBins = new JSlider();
                sliderBins.setBounds(28, 36, 200, 26);
                panel_4.add(sliderBins);
                sliderBins.addChangeListener(new ChangeListener() {
                	public void stateChanged(ChangeEvent arg0) {
                	  int pos=((JSlider)(arg0.getSource())).getValue();
                	  C.NumInitBins=pos; // # of bins to initialize next time
                	  labelBins.setText(""+pos);
                	}
                });
                sliderBins.setValue(2);
                sliderBins.setMinimum(1);
    panel_3.setLayout(null);

        JRadioButton rdbtnPause = new JRadioButton("Pause");
        rdbtnPause.setBounds(10, 17, 155, 23);
        panel_3.add(rdbtnPause);
        runModeGroup.add(rdbtnPause);
        rdbtnPause.setSelected(true);

            JRadioButton rdbtnEvolve = new JRadioButton("Evolve");
            rdbtnEvolve.setBounds(10, 40, 155, 23);
            panel_3.add(rdbtnEvolve);
            rdbtnEvolve.addItemListener(new ItemListener() {
            	public void itemStateChanged(ItemEvent arg0) {
            	  universe.setEvolveState(arg0.getStateChange()==1);
            	}
            });
            runModeGroup.add(rdbtnEvolve);

                JRadioButton rdbtnStimulate = new JRadioButton("Stimulate");
                rdbtnStimulate.setBounds(10, 63, 155, 23);
                panel_3.add(rdbtnStimulate);
                runModeGroup.add(rdbtnStimulate);

                    loopCheckButton = new JCheckBox("Loop");
                    loopCheckButton.setBounds(10, 104, 72, 23);
                    panel_3.add(loopCheckButton);

                        JButton btnStim = new JButton("Stim");
                        btnStim.setBounds(10, 134, 98, 23);
                        panel_3.add(btnStim);

                            lblCF = new JLabel("Cyclic Factor:");
                            lblCF.setBounds(10, 172, 155, 14);
                            panel_3.add(lblCF);

                                JButton btnSnapshot = new JButton("Snapshot");
                                btnSnapshot.setBounds(10, 192, 98, 23);
                                panel_3.add(btnSnapshot);
                                btnSnapshot.addActionListener(new ActionListener() {
                                	public void actionPerformed(ActionEvent arg0) {
                                	  universe.exist.createSnapshot();
                                	}
                                });
                        btnStim.addActionListener(new ActionListener() {
                        	public void actionPerformed(ActionEvent arg0) {
                        	  C.doStim=true;
                        	}
                        });
                    loopCheckButton.addActionListener(new ActionListener() {
                    	public void actionPerformed(ActionEvent arg0) {
                    	  //System.out.println(arg0);
                    	  C.looping=loopCheckButton.isSelected();
                    	}
                    });
    panel_2.setLayout(null);

        JButton btnLoad = new JButton("Load");
        btnLoad.setBounds(10, 11, 92, 23);
        panel_2.add(btnLoad);
        chkGenome = new JCheckBox("Genome");
        chkGenome.setBounds(10, 93, 135, 23);
        panel_2.add(chkGenome);

            chkSD = new JCheckBox("S/D");
            chkSD.setBounds(10, 41, 92, 23);
            panel_2.add(chkSD);
            chkSD.setSelected(true);
            chkConstants = new JCheckBox("Constants");
            chkConstants.setBounds(10, 67, 105, 23);
            panel_2.add(chkConstants);

                JButton btnSave = new JButton("Save");
                btnSave.setBounds(10, 136, 92, 23);
                panel_2.add(btnSave);
                btnSave.addActionListener(new ActionListener() {
                	public void actionPerformed(ActionEvent arg0) {
                      saveRequested=true;
                	}
                });
        btnLoad.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
              loadRequested=true;
        	}
        });
    panel_1.setLayout(null);

        textEffect = new JTextField();
        textEffect.setBounds(66, 45, 86, 20);
        panel_1.add(textEffect);
        textEffect.setColumns(10);

            JLabel lblEffect = new JLabel("Effect");
            lblEffect.setBounds(16, 48, 59, 14);
            panel_1.add(lblEffect);

                textdt = new JTextField();
                textdt.setBounds(65, 76, 86, 20);
                panel_1.add(textdt);
                textdt.setColumns(10);

                    JLabel lblNewLabel = new JLabel("dt");
                    lblNewLabel.setBounds(26, 79, 49, 14);
                    panel_1.add(lblNewLabel);

                        JButton btnSet = new JButton("SET");
                        btnSet.setBounds(65, 11, 87, 23);
                        panel_1.add(btnSet);

                            textMutRate = new JTextField();
                            textMutRate.setBounds(64, 107, 86, 20);
                            panel_1.add(textMutRate);
                            textMutRate.setColumns(10);

                                JLabel lblMutRate = new JLabel("Mut Rate");
                                lblMutRate.setBounds(10, 110, 76, 14);
                                panel_1.add(lblMutRate);

                                    textField_2 = new JTextField();
                                    textField_2.setBounds(66, 131, 86, 20);
                                    panel_1.add(textField_2);
                                    textField_2.setColumns(10);

                                        JLabel label_1 = new JLabel("New label");
                                        label_1.setBounds(16, 134, 76, 14);
                                        panel_1.add(label_1);

                                            textField_3 = new JTextField();
                                            textField_3.setBounds(66, 157, 86, 20);
                                            panel_1.add(textField_3);
                                            textField_3.setColumns(10);

                                                JLabel label_2 = new JLabel("New label");
                                                label_2.setBounds(16, 160, 76, 14);
                                                panel_1.add(label_2);
                        btnSet.addActionListener(new ActionListener() {
                        	public void actionPerformed(ActionEvent arg0) {
                        	  C.effect=Double.parseDouble(textEffect.getText());
                        	  C.dt=Double.parseDouble(textdt.getText());
                        	  C.MUTATIONRATE=Double.parseDouble(textMutRate.getText());
                        	}
                        });
    panel.setLayout(null);

    JLabel lblSdTransferAddressing = new JLabel("SD Transfer Addressing");
    lblSdTransferAddressing.setBounds(23, 5, 170, 14);
    panel.add(lblSdTransferAddressing);

        JRadioButton rdbtnAbsXfer = new JRadioButton("Absolute");
        rdbtnAbsXfer.setBounds(33, 26, 128, 23);
        rdbtnAbsXfer.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        	  C.ABSOLUTEXFER=rdbtnAbsXfer.isSelected();
        	}
        });

        rdbtnAbsXfer.setSelected(true);
        panel.add(rdbtnAbsXfer);
        sdXferGroup.add(rdbtnAbsXfer);

            JRadioButton rdbtnRelXfer = new JRadioButton("Relative");
            rdbtnRelXfer.setBounds(35, 51, 126, 23);
            rdbtnRelXfer.addActionListener(new ActionListener() {
            	public void actionPerformed(ActionEvent arg0) {
        	  C.ABSOLUTEXFER=rdbtnAbsXfer.isSelected();
            	}
            });
            panel.add(rdbtnRelXfer);
            sdXferGroup.add(rdbtnRelXfer);
    contentPane.setLayout(null);
    contentPane.add(panel_4);
    contentPane.add(panel_5);
    contentPane.add(panel_3);

    chckbxExpire = new JCheckBox("Expire");
    chckbxExpire.setBounds(83, 104, 82, 23);
    panel_3.add(chckbxExpire);
    contentPane.add(panel);
    contentPane.add(panel_2);
    contentPane.add(panel_1);

    chckbxConstantLogging = new JCheckBox("Constant Logging");
    chckbxConstantLogging.setBounds(542, 225, 149, 23);
    contentPane.add(chckbxConstantLogging);

    JButton btnAudio = new JButton("Audio");
    btnAudio.addActionListener(new ActionListener() {
    	public void actionPerformed(ActionEvent arg0) {
    	  C.recordAudio=true;
    	  loopCheckButton.setSelected(true); // turn on looping
    	  C.looping=true;
    	}
    });
    btnAudio.setBounds(26, 344, 89, 23);
    contentPane.add(btnAudio);

    updateTextFields();
  }

// update displayed text fields...
  public void updateTextFields()
  {
    textEffect.setText(""+C.effect);
    textdt.setText(""+C.dt);
    textMutRate.setText(""+C.MUTATIONRATE);
  }

  // write state to a file
  @SuppressWarnings("resource")
  public void doOutput()
  {
    saveRequested=false;
    JFileChooser jfc=new JFileChooser();
    jfc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    int retVal=jfc.showSaveDialog(this);
    if (retVal != JFileChooser.APPROVE_OPTION) return; // user gave up
    DataOutputStream dos=null;

// open the file
    try {
      System.out.println("Saving to " + jfc.getSelectedFile().getAbsolutePath());
      dos=new DataOutputStream(new FileOutputStream(jfc.getSelectedFile().getAbsolutePath()));
    } catch (Exception e) {
      System.out.println("Error opening save file: " + e);
      return;
    }

    // save constants
    try {
      dos.writeDouble(C.dt);
      dos.writeDouble(C.effect);
      dos.writeDouble(C.MUTATIONRATE);
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
      return;
    }

    int binMax=(int)((C.maxX-C.minX)/C.dx);

// write chemical levels to file
    for (int i=0;i<=binMax;i++){
      try {
	dos.writeDouble(universe.exist.mem[i][C.S]);
        dos.writeDouble(universe.exist.mem[i][C.D]);
      } catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
      }
    }

 // write transfer diameters
    for (int i=0;i<=binMax;i++){
      try {
        System.out.println("Diam[" + i + "]=" + universe.exist.flowRates[i]);
	dos.writeDouble(universe.exist.flowRates[i]);
      } catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
      }
    }

    try {
      dos.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

// read a saved state
  public void doInput()
  {
    loadRequested=false; // unset request flag

    JFileChooser jfc=new JFileChooser();
    jfc.setCurrentDirectory(new File(System.getProperty("user.dir")));
    int retVal=jfc.showOpenDialog(this);
    if (retVal != JFileChooser.APPROVE_OPTION) return; // user gave up
    DataInputStream dis=null;

// open the file
    try {
      System.out.println("Opening " + jfc.getSelectedFile().getAbsolutePath());
      dis=new DataInputStream(new FileInputStream(jfc.getSelectedFile().getAbsolutePath()));
    } catch (Exception e) {
      System.out.println("Error opening input file: " + e);
    }

    int binMax=(int)((C.maxX-C.minX)/C.dx);
    double sss=0,ddd=0;

    try {
      if (chkConstants.isSelected()){
        System.out.println("Loading parameters");
        C.dt=dis.readDouble();
        C.effect=dis.readDouble();
        C.MUTATIONRATE=dis.readDouble();
        updateTextFields(); // update displays on Control panel
      } else {dis.readDouble();dis.readDouble();dis.readDouble();}
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }

    for (int i=0;i<=binMax;i++){
      try{
        sss=dis.readDouble();
        ddd=dis.readDouble();
      } catch(Exception e){
        System.out.println("Read error while loading file");
        break;
      }
      if (chkSD.isSelected()){
        universe.exist.mem[i][C.S]=sss;
        universe.exist.mem[i][C.D]=ddd;
      }
    } // end of pattern ingest

    // read transfer diameters
    for (int i=0;i<=binMax;i++){
      try {

	ddd=dis.readDouble();
	if (chkGenome.isSelected()){
	  universe.exist.flowRates[i]=ddd;
	}
      } catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
      }
    }

    try {
      dis.close();
    } catch (IOException e) {
      System.out.println("Error closing file: " + e);
    }
  }
}
