package com.cellmatrix.chemcomp8;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

// get/setGenome()
// randomize()
// readBin(), writeBin() - can take a double (x) or an integer (bin #)

// class for each member of the population
public class Individual {
  //double genome[]; // flowrate of each bin: 0=stop, 1=full
  double genome[][]; // genome is the initial chemical setup
  double fitness=0; // measure of success
  int generation; // why not!
  EXIST exist;

// constructor
  Individual(EXIST ee)
  {
    exist=ee;
    genome=new double[exist.binMax+1][2];
    for (int i=0;i<exist.binMax+1;i++) genome[i][0]=genome[i][1]=0.;
    fitness=0;
    generation=0;
  }

// set and retrieve genome
  public double[][] getGenome()
  {
    return(genome);
  }

  public void setGenome(double[][] g)
  {
    genome=g;
  }

// seed the genome
  public void randomize()
  {
    double val1=0,val2=0;
    for (int i=0;i<exist.binMax+1;i++){
      if (0==i%C.CSIZE){ // define new parameters
        val1=Math.random()*C.MAXVAL;
        val2=Math.random()*C.MAXVAL;
      }
      genome[i][0]=val1;
      genome[i][1]=val2;
    }

/***
    // smooth the genome now
    for (int i=0;i<eexist.binMax+1;i++){
      genome[i][0]=smooth(genome,i,0);
      genome[i][1]=smooth(genome,i,1);
    }
***/
  }

  //
  // when smoothing, average elements from 0 to 20, rolling over 20->0 and 0->20;
  // then do 21-40, 41-60, etc. for a total of 16 chunks, each smooth within itself
  // but likely discontinuous relative to its neighboring chunks
  //
  // When breeding, just copy chunks from one parent or the other.
  //

  double smooth(double genome[][],int index, int sd)
  {
    int i,low,high;
    low=index-10;if (low<0) low=0;
    high=index+10;if (high > exist.binMax) high=exist.binMax; // this is our range of bins to average
    double n=0,sum=0;
    for (i=low;i<=high;i++){
      n=n+1;
      sum=sum+genome[i][sd];
    }
    return(sum/n);
  }

}