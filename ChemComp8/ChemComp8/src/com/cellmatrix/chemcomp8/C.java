package com.cellmatrix.chemcomp8;

import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

// class of constants!

public class C {
  static double minX=0,maxX=40; // pure X values for the entire universe
  static double MAXVAL=40; // this is the most S or D a tube can hold...
  //static double dx=.03125; // this is our delta-X
  static double dx=.125; // this is our delta-X
  static double effect=20; // each transfer also affects nearby bins // was 20
  static int timeStep=0; // increment each step
  static boolean debug=false;
  static double rgbScale=7;
  static double dt=0.05; // Step the simulation this much. Used to scale pouring impact
  static double maxTime=50;
  static int scaleX=2,scaleY=1;
  static int numStepsOnDisplay=600; // y scale :)
  static boolean VOLUMECLIP=true; // set to clip volumes between [0,MAXVAL]
  static int clearTime=20;
  static int POPSIZE=40; // # of individuals in the population
  static int SELECT=6; // preserve the most fit individuals, then breed them
  static double MUTATIONRATE=0.05;
  static int CSIZE=16; // # of bins in a single gene
  static int S=0,D=1;
  static boolean looping=false; // set true to let the current member run forever
  static boolean doStim=false;
  static double snapshotDiffThreshold=0.01; // report changes smaller than this
  static int NumInitBins=2;
  static boolean ABSOLUTEXFER=false; // abs or relative
  static int loopCountExpire=100; // if expire checkbox is checked, only loop this many timesteps
  static boolean recordAudio=false; // set to start audio recording
  static String WAVEFILENAME="c:/users/nmacias/Desktop/sample.wav";

               /************** STATIC CODE ***************/

// FFT code. Create a .wav file to write reverse-fft data

  static int granularity=64;
  static double rTime=0.0; // running time - avoid the click!

  static public DataOutputStream writeHeader(String fname, int duration) // duration in seconds
  {
  // ref http://soundfile.sapp.org/doc/WaveFormat/
    DataOutputStream os=null;
    long size;

    rTime=0; // reset running time
    size=44100*4*duration; // # bytes in duration seconds;
// write header for wave file
    try {
      os = new DataOutputStream(new FileOutputStream(fname));
    // ChunkID
      os.write('R');os.write('I');os.write('F');os.write('F');
    // ChunkSize
      os.write((byte)((36+size)&0xff));
      os.write((byte)(((36+size)>>8)&0xff));
      os.write((byte)(((36+size)>>16)&0xff));
      os.write((byte)(((36+size)>>24)&0xff));
    // Format
      os.write('W');os.write('A');os.write('V');os.write('E');
    // Subchunk1ID
      os.write('f');os.write('m');os.write('t');os.write(' ');
    // SubChunk1Size
      os.write(16);os.write(0);os.write(0);os.write(0);
    // AudioFormat
      os.write(1);os.write(0);
    // NumChannels
      os.write(2);os.write(0);
    // SampleRate
      os.write(44100&0xff);os.write((41000>>8)&0xff);os.write(0);os.write(0);
    // ByteRate
      os.write(176400&0xff);os.write((176400>>8)&0xff);os.write((176400>>16)&0xff);os.write(0);
    // BlockAlign
      os.write(4);os.write(0);
    // BitsPerSample
      os.write(16);os.write(0);
    // Subchunk2ID
      os.write('d');os.write('a');os.write('t');os.write('a');
    // Subchunk2Size
      os.write((byte)(size&0xff));
      os.write((byte)((size>>8)&0xff));
      os.write((byte)((size>>16)&0xff));
      os.write((byte)((size>>24)&0xff));

    } catch (IOException e) {
      System.out.println("Error writing wave file header");
    }
// header all done: return outputStream so we can write data...
    return(os);
  }

// reverse-FFT on mem[][], then write data to .wav file
  static public void writeData(DataOutputStream os, EXIST exist, double limit) // write limit-second sample
  {
    double time,x,freq;
    int value;
    byte[] buffer = new byte[2];
    for (time=0;time<limit;time+=(1./44100.)){
      rTime+=(1./44100.);
      // treat mem[][] as frequency domain; convert to time domain
      x=exist.mem[0][0]; // COS component only?
      for (int n=1;n<(C.MAXVAL/C.dx);n+=10){
        freq=32.*(double)n;
        x+=exist.mem[n][0]*Math.cos(freq*rTime) + exist.mem[n][1]*Math.sin(freq*rTime);
        //if ((n==51) || (n==81) || (n==121)) // debug test
          //x+=Math.cos(freq*rTime);
      }
      //System.out.println(x);
      x=x/40.; // scaling

      if (x > 1) x=1;
      if (x < -1) x=(-1); // clipping

      value = (int)(32767.*x); // write this value
      if (value < 0) value=65536+value; // 2's comp!
      buffer[0] = (byte) (value & 0xff); // low byte
      buffer[1] = (byte) ((value>>8) & 0xff); // next byte
      try {
        os.write(buffer[0]); // left channel
        os.write(buffer[1]); // left channel
        os.write(buffer[0]); // right channel
        os.write(buffer[1]); // right channel
      } catch (IOException e) {
        System.out.println("Error writing wavfile data");
        return;
      }
    } // small snippet written
  }

}